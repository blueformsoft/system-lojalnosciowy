var serwer = function() {
    this.request = require('superagent')
    this.secretkey = 'E771EC0E544D09E3D7FDBAA99761FB66C8BA30C3CDDAEEDBA62B5BA6E50EDF784AEB03F1541ABBFE3A5E0E2858C3C6D3E537BA0C3A8298971750B33EC1E91CD2'
    this.apiserwer = 'https://firmotron.pl/api/'
    this.GetFooter = function(callback) {
        this.request.get(this.apiserwer + 'footer')
            .then(res => {
                callback(res.text)
            })
            .catch(err => {
                callback('')
            });
    }.bind(this)
}
module.exports = new serwer()