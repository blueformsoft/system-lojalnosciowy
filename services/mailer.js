var Order = require("../models/order")
var MailInfo = require("../models/mailinfo")
var Repository = require("../models/repository")
var moment = require('moment')

var mailer = function() {
    this.nodemailer = require('nodemailer')
    this.config = {
        host: 'mail16.mydevil.net',
        port: 587,
        secure: false,
        auth: {
            user: 'mailer@faktura365.pl',
            pass: 'kanjiGiga$17'
        },
        tls: {
            rejectUnauthorized: false
        }
    }
    this.transporter = this.nodemailer.createTransport(this.config)
    this.activate_mail = function(req, email, res, token, id, firma, program) {
        let url = `https://${req.hostname}/client/${firma}/${program}/activate?id=${id}&token=${token}`
        let mailoptions = {
            from: "SystemLojalnosciowy365.pl <serwis@blueform.pl>",
            to: '<' + email + '>',
            subject: 'Aktywacja konta',
            html: '<p>Aby aktywować swoje konto w serwisie kliknij poniższy link (lub przekopiuj go do przeglądarki)</p><a href="' + url + '">' + url + '</a><p>Wiadomość wygenerowana automatycznie</p>',
        }
        this.transporter.sendMail(mailoptions, (err, info) => {
            if (err) {
                console.log(err)
                res.json({ errors: ["Wystąpił błąd podczas wysyłania wiadomości."] })
            } else
                res.sendStatus(200)
        })
    }
    this.reset_password = function(req, email, res, token, id, login, firma, program) {
        if (!firma) res.locals.info_session.Firma
        if (!program) req.session.program
        let url = `https://${req.hostname}/client/${firma}/${program}/setpassword?id=${id}&token=${token}`
        let mailoptions = {
            from: "SystemLojalnosciowy365.pl <serwis@blueform.pl>",
            to: '<' + email + '>',
            subject: 'Reset hasła',
            html: '<p>Login: ' + login + '</p><p>Aby zresetować swoje hasło w serwisie kliknij poniższy link (lub przekopiuj go do przeglądarki)</p><a href="' + url + '">' + url + '</a><p>Wiadomość wygenerowana automatycznie</p>',
            text: 'Login: ' + login + '. Aby zresetować swoje hasło w serwisie przejdź <a href="' + url + '">tutaj</a>. Wiadomość wygenerowana automatycznie.'
        }
        this.transporter.sendMail(mailoptions, (err, info) => {
            if (err) {
                console.log(err)
                res.json({ errors: ["Wystąpił błąd podczas wysyłania wiadomości."] })
            } else
                res.sendStatus(200)
        })
    }.bind(this)
    this.send_product = function(order, callback) {
        Order.GetProduct(order, function(err, data) {
            if (err) throw err
            data = data[0]
            if (!data.Automat || !data.Opis) return
            let mail = JSON.parse(data.Mail)
            if (!mail.Title) {
                mail.Title = `{Firma} - zamówienie nr {Numer_zamowienia}`
                mail.Body = `Wiadomość jest realizacją zamówienia nr {Numer_zamowienia}.<br><br>
                Twoje zamówienie:<br>
                {Opis}<br><br>
                <b>Szczególy zamówienia</b><br>
                Wybrany produkt: {Nagroda}<br>
                Cena: {Cena}<br>
                <br><br>
                Pozdrawiamy,<br>
                {Firma}`
            }
            mail.Title = resolve(mail.Title, data)
            mail.Body = resolve(mail.Body, data)
            let mailoptions = {
                from: "SystemLojalnosciowy365.pl <serwis@blueform.pl>",
                to: '<' + data.Email + '>',
                subject: mail.Title,
                html: mail.Body
            }
            if (data.Zalacznik) {
                let plik = JSON.parse(data.Plik)
                mailoptions.attachments = [{
                    filename: plik.OriginalName,
                    path: `${__dirname}/../${plik.Path}/${plik.File}`
                }]
            }
            this.transporter.sendMail(mailoptions, (err, info) => {
                if (err) {
                    console.log(err)
                    callback({ errors: ["Wystąpił błąd podczas wysyłania wiadomości."] })
                } else {
                    let rep = {
                        Id: data.Id_produkt,
                        Firma: data.Id_firmy,
                        Wydane: true,
                        Data_wydania: moment().format('YYYY-MM-DD HH:mm'),
                        Id_klienta: data.Id_klienta
                    }
                    Repository.Update(rep, function(err, result) {
                        if (err) throw (err)
                        let order = {
                            Id: data.Id,
                            Firma: data.Id_firmy,
                            Data_realizacji: moment().format('YYYY-MM-DD HH:mm'),
                            Status: 'ZRE'
                        }
                        Order.Update(order, callback)
                    })
                }
            })
        }.bind(this))
    }
    this.new_order = function(order, callback) {
        Order.GetInfo(order, function(err, data) {
            if (err) throw err
            data = data[0]
            let mail = JSON.parse(data.Mail_zamowienie)
            if (!mail.Title) {
                mail.Title = `{Firma} - potwierdzenie zamówienia nr {Numer_zamowienia}`
                mail.Body = `Potwierdzamy dokonanie zamówienia nr {Numer_zamowienia}<br><br>
                <b>Szczególy zamówienia</b><br>
                Wybrany produkt: {Nagroda}<br>
                Cena: {Cena}<br><br>
                Pozdrawiamy,<br>
                {Firma}`
            }
            mail.Title = resolve(mail.Title || '', data)
            mail.Body = resolve(mail.Body || '', data)
            let mailoptions = {
                from: "SystemLojalnosciowy365.pl <serwis@blueform.pl>",
                to: '<' + data.Email + '>',
                subject: mail.Title,
                html: mail.Body
            }
            this.transporter.sendMail(mailoptions, (err, info) => {
                if (err) {
                    console.log(err)
                    callback({ errors: ["Wystąpił błąd podczas wysyłania wiadomości."] })
                } else
                    callback(err, info)
            })

        }.bind(this))
    }.bind(this)
    this.cancel_order = function(email, data, callback) {
        Order.GetInfo(order, function(err, data) {
            if (err) throw err
            data = data[0]
            let mail = JSON.parse(data.Mail_anulowanie)
            if (!mail.Title) {
                mail.Title = `{Firma} - anulowanie zamówienia nr {Numer_zamowienia}`
                mail.Body = `Zamówienia nr {Numer_zamowienia} zostało anulowane.<br><br>
                <b>Szczególy zamówienia</b><br>
                Wybrany produkt: {Nagroda}<br>
                Cena: {Cena}<br>
                <br><br>
                Pozdrawiamy,<br>
                {Firma}`
            }
            mail.Title = resolve(mail.Title || '', data)
            mail.Body = resolve(mail.Body || '', data)
            let mailoptions = {
                from: "SystemLojalnosciowy365.pl <serwis@blueform.pl>",
                to: '<' + data.Email + '>',
                subject: mail.Title,
                html: mail.Body
            }
            this.transporter.sendMail(mailoptions, callback)
        }.bind(this))
    }.bind(this)
    this.realize_order = function(order, callback) {
        Order.GetInfo(order, function(err, data) {
            if (err) throw err
            data = data[0]
            let mail = JSON.parse(data.Mail_realizacja)
            if (!mail.Title) {
                mail.Title = `{Firma} - realizacja zamówienia nr {Numer_zamowienia}`
                mail.Body = `Zamówienia nr {Numer_zamowienia} zostało zrealizowane.<br><br>
                <b>Szczególy zamówienia</b><br>
                Wybrany produkt: {Nagroda}<br>
                Cena: {Cena}<br>
                <br><br>
                Pozdrawiamy,<br>
                {Firma}`
            }
            mail.Title = resolve(mail.Title || '', data)
            mail.Body = resolve(mail.Body || '', data)
            let mailoptions = {
                from: "SystemLojalnosciowy365.pl <serwis@blueform.pl>",
                to: '<' + data.Email + '>',
                subject: mail.Title,
                html: mail.Body
            }
            this.transporter.sendMail(mailoptions, callback)
        }.bind(this))
    }.bind(this)
    this.send_account_state = function(client, callback) {
        MailInfo.GetInfo(client, function(err, data) {
            if (err) throw err
            data = data[0]
            let mail = JSON.parse(data.Mail_zmiana_salda)
            if (!mail.Title) {
                mail.Title = `{Firma} - Zmiana stanu konta`
                mail.Body = `Informujemy, że nastąpiła zmiana stanu Twojego konta w naszym systemie.<br>
                Ostatnia zmiana: <br>
                {Zmiana_stanu_konta} punktów (dokument: {Opis_zmiany})<br>
                Aktualny stan konta:<br>
                {Aktualny_stan_konta}
                <br><br>
                Pozdrawiamy,<br>
                {Firma}`
            }
            mail.Title = resolve(mail.Title || 'Aktualizacja stanu konta', data)
            mail.Body = resolve(mail.Body || 'Aktualizacja stanu konta', data)
            let mailoptions = {
                from: "SystemLojalnosciowy365.pl <serwis@blueform.pl>",
                to: '<' + data.Email + '>',
                subject: mail.Title,
                html: mail.Body
            }
            this.transporter.sendMail(mailoptions, callback)
        }.bind(this))
    }.bind(this)
    this.kontakt = function(req, res) {
        let url = 'https://' + req.hostname
        let html = ''
        html += '<p>Wiadomość od: <strong>' + req.body.email + '</strong>, ' + req.body.imie + ' ' + req.body.nazwisko + '</p>'
        html += '<p>Treść: </p>'
        html += '<p>' + req.body.tresc + '</p>'
        let mailoptions = {
            from: 'Faktura365.pl <mailer@faktura365.pl>',
            to: req.body.to,
            subject: 'Formularz kontaktowy',
            html: html
        }
        if (!validateEmail(req.body.email)) {
            res.send({ error: 'Podano nieprawidłowy adres e-mail' })
            return
        }
        this.transporter.sendMail(mailoptions, (err, info) => {
            if (err) console.log(err)
            res.sendStatus(200)
        })
    }

    function validateEmail(email) {
        if (email.indexOf('@') >= 0 && email.indexOf('.') >= 0)
            return true
        return false
    }

    function resolve(template, data) {
        return template
            .replace(
                /{(\w*)}/g, // or /{(\w*)}/g for "{this} instead of %this%"
                function(m, key) {
                    return data.hasOwnProperty(key) ? data[key] : ""
                }
            )
    }
}
module.exports = new mailer()