var functions = function() {
    this.promiseAllP = function(items, block) {
            var promises = [];
            items.forEach(function(item, index) {
                promises.push(function(item, i) {
                    return new Promise(function(resolve, reject) {
                        return block.apply(this, [item, index, resolve, reject]);
                    });
                }(item, index))
            });
            return Promise.all(promises);
        } //promiseAll

    this.readFiles = function(dirname) {
        return new Promise((resolve, reject) => {
            serwer.fs.readdir(dirname, function(err, filenames) {
                if (err) return reject(err);
                promiseAllP(filenames,
                        (filename, index, resolve, reject) => {
                            serwer.fs.readFile(serwer.path.resolve(dirname, filename), function(err, content) {
                                if (err) return reject(err);
                                return resolve({ filename: filename, contents: content });
                            });
                        })
                    .then(results => {
                        return resolve(results);
                    })
                    .catch(error => {
                        return reject(error);
                    });
            });
        });
    }

    this.template = function(text, data) {
        return text
            .replace(
                /{(\w*)}/g, // or /{(\w*)}/g for "{this} instead of %this%"
                function(m, key) {
                    var new_text = ''
                    if (data.hasOwnProperty(key)) {
                        if (data[key] != null && data[key] != 'undefined')
                            new_text = data[key]
                    }
                    return new_text
                }
            )
    }

    let alg = 'aes-256-ctr'

    this.holidays = function(y) {
        y = y - 1
        var holidays = []
        for (var i = 0; i < 3; i++) {
            holidays.push('1.1.' + y, '6.1.' + y, '1.5.' + y, '3.5.' + y, '15.8.' + y, '1.11.' + y, '11.11.' + y, '25.12.' + y, '26.12.' + y)
            var wielkanoc = easterDate(y)
            var poniedzialek = serwer.moment(wielkanoc, 'D.M.YYYY').add('1', 'd').format('D.M.YYYY')
            var boze_cialo = serwer.moment(wielkanoc, 'D.M.YYYY').add('60', 'd').format('D.M.YYYY')
            holidays.push(wielkanoc, poniedzialek, boze_cialo)
            y++
        }
        return holidays
    }

    this.easterDate = function(rok) {
        var a = rok % 19
        var b = Math.floor(rok / 100)
        var c = rok % 100
        var d = Math.floor(b / 4)
        var e = b % 4
        var f = Math.floor((b + 8) / 25)
        var g = Math.floor((b - f + 1) / 3)
        var h = (19 * a + b - d - g + 15) % 30
        var i = Math.floor(c / 4)
        var k = c % 4
        var l = (32 + 2 * e + 2 * i - h - k) % 7
        var m = Math.floor((a + 11 * h + 22 * l) / 451)
        var p = (h + l - 7 * m + 114) % 31
        var dzien = p + 1
        var miesiac = Math.floor((h + l - 7 * m + 114) / 31)
        var wielkanoc = dzien + '.' + miesiac + '.' + rok
        return wielkanoc
    }

    this.checkDirectory = function(path, cb) {
        serwer.fs.stat(path, function(err, stats) {
            if (err && err.errno === 34) {
                //Create the directory, call the callback.
                serwer.fs.mkdir(path, cb);
            } else {
                //just in case there was a different error:
                cb(err)
            }
        })
    }


    this.calcDistance = function(arg) {
        // Converts from degrees to radians
        Math.radians = function(degrees) {
            return degrees * Math.PI / 180;
        };

        // Converts from radians to degrees.
        Math.degrees = function(radians) {
            return radians * 180 / Math.PI;
        };

        let lat1 = Math.radians(arg.lat1)
        let lng1 = Math.radians(arg.lng1)

        let lat2 = Math.radians(arg.lat2)
        let lng2 = Math.radians(arg.lng2)

        let distance = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lng1 - lng2))

        return 6371 * distance

    }


    this.encrypt = function(text, key, iv_length) {
        if (text == null || text == '' || typeof text == 'undefined')
            return text
        key = serwer.crypto.createHash('md5').update(key, 'utf-8').digest('hex').toUpperCase();
        // var cipher = serwer.crypto.createCipher(alg, pass)
        // var crypted = cipher.update(text,'utf8','hex')
        // crypted += cipher.final('hex');
        // return crypted;
        let length = iv_length || 16
        let iv = new Buffer.alloc(length)
        let cipher = serwer.crypto.createCipheriv('aes-256-ctr', key, iv);
        let encrypted = cipher.update(text, 'utf8', 'hex') + cipher.final('hex');

        return encrypted
    }

    this.decrypt = function(text, key, iv_length) {
        if (text == null || text == '' || typeof text == 'undefined')
            return text
                // var decipher = serwer.crypto.createDecipher(alg, pass)
                // var dec = decipher.update(text,'hex','utf8')
                // dec += decipher.final('utf8');
                // return dec;
        key = serwer.crypto.createHash('md5').update(key, 'utf-8').digest('hex').toUpperCase();
        let length = iv_length || 16
        let iv = new Buffer.alloc(length)
        let decipher = serwer.crypto.createDecipheriv('aes-256-ctr', key, iv)
        let decrypted = decipher.update(text, 'hex', 'utf8') + decipher.final('utf8')

        return decrypted
    }

    this.ftRandom = function(arg) {

        //wylosuj liczbe
        let number = (Math.random() * (arg.max - arg.min)) + arg.min;

        //jeżeli zarządano zaokrąglenia liczby to zrób to
        if (arg.floor && arg.floor == true) {

            number = Math.floor(number);

        }

        //zwróc liczbę
        return number;

    }
    this.codeToken = function(secret, token) {

        let arr = [];
        for (let i = 0, u = 0; i < token.length; i++, u++) {
            if (u == secret.length) u = 0;
            let charCode = token.charCodeAt(i);
            let secretCode = secret.charCodeAt(u);
            arr.push(charCode + secretCode);
        }

        return arr;
    }

    this.decodeToken = function(secret, arr) {

        let token = "";
        for (let i = 0, u = 0; i < arr.length; i++, u++) {
            if (u == secret.length) u = 0;
            let charCode = arr[i];
            let secretCode = secret.charCodeAt(u);
            token += String.fromCharCode(charCode - secretCode);
        }

        return token;
    }

    this.ftGenerateToken = function(length, arr) {

        let token = "";
        let arrd = "a b c d e f g h i j k l m n o p r s t u w x y z A B C D E F G H I J K L M N O P R S T U W X Y Z ";
        arrd += "1 2 3 4 5 6 7 8 9 0"
            //" . / , * & ^ % $ # @ ! ( ) _ - + = { } [ ] : ; ' ? > <";

        arr = arr || arrd;

        arr = arr.split(" ");

        for (var i = 0; i < length; i++) {
            let index = this.ftRandom({ min: 0, max: arr.length - 1, floor: true });
            token += arr[index];
        }

        //zwróc token
        return token;
    }.bind(this)





    this.validatenip = function(nip) {
        var nipWithoutDashes = nip.replace(/-/g, "")
        var reg = /^[0-9]{10}$/
        if (reg.test(nipWithoutDashes) == false) {
            return false
        } else {
            var digits = ("" + nipWithoutDashes).split("")
            var checksum = (6 * parseInt(digits[0]) + 5 * parseInt(digits[1]) + 7 * parseInt(digits[2]) + 2 * parseInt(digits[3]) + 3 * parseInt(digits[4]) + 4 * parseInt(digits[5]) + 5 * parseInt(digits[6]) + 6 * parseInt(digits[7]) + 7 * parseInt(digits[8])) % 11
            return (parseInt(digits[9]) == checksum)
        }
    }

    this.validateregon9 = function(regon) {
        var reg = /^[0-9]{9}$/
        if (!reg.test(regon))
            return false
        else {
            var digits = ("" + regon).split("")
            var checksum = (8 * parseInt(digits[0]) + 9 * parseInt(digits[1]) + 2 * parseInt(digits[2]) + 3 * parseInt(digits[3]) + 4 * parseInt(digits[4]) + 5 * parseInt(digits[5]) + 6 * parseInt(digits[6]) + 7 * parseInt(digits[7])) % 11
            if (checksum == 10)
                checksum = 0
            return (parseInt(digits[8]) == checksum)
        }
    }

    this.validateregon14 = function(regon) {
        var reg = /^[0-9]{14}$/
        if (!reg.test(regon))
            return false
        else {
            var digits = ('' + regon).split('')
            var checksum = (2 * parseInt(digits[0]) + 4 * parseInt(digits[1]) + 8 * parseInt(digits[2]) + 5 * parseInt(digits[3]) + 0 * parseInt(digits[4]) + 9 * parseInt(digits[5]) +
                7 * parseInt(digits[6]) + 3 * parseInt(digits[7]) + 6 * parseInt(digits[8]) + 1 * parseInt(digits[9]) + 2 * parseInt(digits[10]) +
                4 * parseInt(digits[11]) + 8 * parseInt(digits[12])) % 11
            if (checksum == 10)
                checksum = 0
            return (parseInt(digits[13]) == checksum)
        }
    }

    this.translate = function(liczba) {
        var jednosci = ["", " jeden", " dwa", " trzy", " cztery", " pięć", " sześć", " siedem", " osiem", " dziewięć"]
        var nascie = ["", " jedenaście", " dwanaście", " trzynaście", " czternaście", " piętnaście", " szesnaście", " siedemnaście", " osiemnaście", " dziewietnaście"]
        var dziesiatki = ["", " dziesięć", " dwadzieścia", " trzydzieści", " czterdzieści", " pięćdziesiąt", " sześćdziesiąt", " siedemdziesiąt", " osiemdziesiąt", " dziewięćdziesiąt"]
        var setki = ["", " sto", " dwieście", " trzysta", " czterysta", " pięćset", " sześćset", " siedemset", " osiemset", " dziewięćset"]
        var grupy = [
            ["", "", ""],
            [" tysiąc", " tysiące", " tysięcy"],
            [" milion", " miliony", " milionów"],
            [" miliard", " miliardy", " miliardów"],
            [" bilion", " biliony", " bilionów"],
            [" biliard", " biliardy", " biliardów"],
            [" trylion", " tryliony", " trylionów"]
        ]

        if (!isNaN(liczba)) {

            var wynik = ''
            var znak = ''
            if (liczba == 0)
                wynik = "zero"
            if (liczba < 0) {
                znak = "minus"
                liczba = liczba
            }

            var g = 0
            while (liczba > 0) {
                var s = Math.floor((liczba % 1000) / 100)
                var n = 0
                var d = Math.floor((liczba % 100) / 10)
                var j = Math.floor(liczba % 10)
                if (d == 1 && j > 0) {
                    n = j
                    d = 0
                    j = 0
                }

                var k = 2
                if (j == 1 && s + d + n == 0)
                    k = 0
                if (j == 2 || j == 3 || j == 4)
                    k = 1
                if (s + d + n + j > 0)
                    wynik = setki[s] + dziesiatki[d] + nascie[n] + jednosci[j] + grupy[g][k] + wynik

                g++
                liczba = Math.floor(liczba / 1000)
            }
            return wynik.trim()
        }
        return false
    }

    this.passwordValidationFail = function(pass) {
        let passValidationFail = 'Hasło nie spełnia następujących warunków: '
        let passFail = serwer.passwordSchema.validate(pass, { list: true })
        let passFailSet = new Set(passFail)
        if (passFailSet.has('min') || passFailSet.has('max')) passValidationFail += "musi zawierać od 8 do 20 znaków, "
        if (passFailSet.has('uppercase')) passValidationFail += "musi zawierać przynajmniej jedną dużą literę, "
        if (passFailSet.has('lowercase')) passValidationFail += "musi zawierać przynajmniej jedną małą literę, "
        if (passFailSet.has('digits')) passValidationFail += "musi zawierać przynajmniej jedną cyfrę, "
        if (passFailSet.has('spaces')) passValidationFail += "nie może zawierać spacji, "
        return passValidationFail
    }

    this.restrict_level = function(req, res, next) {
        var session = require('../models/session')
        if (req.session && req.session.sid && req.session.key) {
            session.validation(req, res, function() {
                next()
            })
        } else {
            res.sendfile("layouts/logoutTemplate.html")
        }
    }
    this.restrict_remote = function(req, res, next) {
        var session = require('../models/session')
        if (req.body.sid && req.body.key) {
            session.validation_remote(req, res, function() {
                next()
            })
        } else {
            res.json({ error: "Brak danych na temat sesji" })
        }
    }
    this.restrict_program = function(req, res, next) {
        if (req.session.program)
            next()
        else
            res.sendfile("layouts/selectProgramTemplate.html")
    }
    this.restrict_client_level = function(req, res, next) {
        var sessionclient = require('../models/clientsession')
        if (req.session.clientsid && req.session.clientkey) {
            sessionclient.validation(req, res, false, function() {
                next()
            })
        } else {
            res.sendfile("layouts/clientLogin.html")
        }
    }
    this.restrict_main_client_level = function(req, res, next) {
        var sessionclient = require('../models/clientsession')
        if (req.session.clientsid && req.session.clientkey) {
            sessionclient.validation(req, res, true, function() {
                next()
            })
        } else {
            res.sendfile("layouts/clientLogin.html")
        }
    }
}
module.exports = new functions()