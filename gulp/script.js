var gulp = require('gulp')
var concat = require('gulp-concat')
var sourcemaps = require('gulp-sourcemaps')
var uglify = require('gulp-uglify')
var ngAnnotate = require('gulp-ng-annotate')
var pump = require('pump')
var gutil = require('gulp-util')
var babel = require('gulp-babel')

gulp.task('js', function() {
    gulp.src(['ng/module.js', 'ng/**/*.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(ngAnnotate())
        .pipe(babel({
            presets: ["es2015"]
        }))
        // .pipe(uglify().on('error', function(err) {
        //     gutil.log(gutil.colors.red('[Error]'), err.toString());
        //     this.emit('end');
        // }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('assets'))
})
gulp.task('watch:js', ['js'], function() {
    gulp.watch('ng/**/*.js', ['js'])
})
gulp.task('watch:css', ['css'], function() {
    gulp.watch('css/**/*.styl', ['css'])
})

gulp.task('uglify-error-debugging', function(cb) {
    pump([
        gulp.src('app/**/*.js'),
        uglify().on('error', function(err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
            this.emit('end');
        }),
        gulp.dest('./dist/')
    ], cb);
});

gulp.task('concat', function() {
    return gulp.src('app/**/*.js')
        // .pipe(concat('script.js'))
        .pipe(uglify())
        .on('error', function(err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest('./dist/'))
});