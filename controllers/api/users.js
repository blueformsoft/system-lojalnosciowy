var router = require('express').Router()
var User = require('../../models/user')
var bcrypt = require('bcryptjs')
var jwt = require('jwt-simple')
var config = require('../../config')
router.get('/', function(req, res, next) {
    if (!req.headers['x-auth']) {
        return res.send(401)
    }
    var auth = jwt.decode(req.headers['x-auth'], config.secret)
    User.getAll(function(err, data) {
        res.send(data)
    })
})
module.exports = router