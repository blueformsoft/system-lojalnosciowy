var Program = require("../../models/program")
var router = require('express').Router()
var functions = require("../../services/functions")
var moment = require('moment')
var fs = require('fs')
var mkdirp = require('mkdirp')
var multer = require('multer')

router.use('/', functions.restrict_level, function(req, res, next) {
    next()
})

router.get('/', function(req, res, next) {
    Program.GetAll(res.locals.info_session.Firma, function(err, programs) {
        if (err) { return next(err) }
        res.json({ programs, info_session: res.locals.info_session })
    })
})
router.get('/fetch', function(req, res, next) {
    Program.GetAll(res.locals.info_session.Firma, function(err, data) {
        if (err) { return next(err) }
        programs = data[0]
        for(var i = 0; i < programs.length; i++){
            programs[i].statistics = data[1].filter(programfilter, programs[i].Id)
        }
        res.json(programs)
    })
})
var programfilter = function(element) {
    return element.program == this
}
router.post('/select', function(req, res, next) {
    req.session.program = req.body.id
    res.sendStatus(200)
})
router.post('/add', function(req, res, next) {
    let data = req.body.program
    data.Id_sesji = req.session.sid
    data.Firma = res.locals.info_session.Firma
    data.Aktywny = true

    Program.Create(data, function(err, data) {
        if (err) { return next(err) }
        res.send(201)
    })
})
router.post('/update', function(req, res, next) {
    let data = req.body.program
    data.Id_sesji = req.session.sid
    data.Firma = res.locals.info_session.Firma
    Program.Update(data, function(err, data) {
        if (err) { return next(err) }
        res.send(200)
    })
})
router.post('/upload_logo', function(req, res, next) {
    UploadLogo(req, res, function(logo) {
        res.json(logo)
    })
})
router.post('/upload_tlo', function(req, res, next) {
    UploadTlo(req, res, function(tlo) {
        res.json(tlo)
    })
})
router.get('/edit', function(req, res, next) {
    Program.ExecuteManyQueries([Program.Get(req.query.id, res.locals.info_session.Firma), Program.GetClientAmount(req.query.id, res.locals.info_session.Firma)], function(err, data) {
        if (err) { return next(err) }
        let program = data[0][0]
        program.Data_rozpoczecia = moment(program.Data_rozpoczecia).format('YYYY-MM-DD')
        program.Data_zakonczenia = moment(program.Data_zakonczenia).format('YYYY-MM-DD')
        let ilosc_klientow = data[1][0].Ilosc
        res.json({ program, ilosc_klientow })
    })
})
var UploadLogo = function(req, res, callback) {
    let logodata = ''
    let info = res.locals.user_data
    let path = ''
    let storage = multer.diskStorage({
        destination: function(req, file, cb) {
            path = `/data/Firma_${res.locals.info_session.Firma}/Templates`
            let dir = `${__dirname}/../../public/${path}`
            mkdirp(dir, err => cb(err, dir))
        },
        filename: function(req, file, cb) {
            logodata = { OriginalName: file.originalname, File: Date.now() + '' }
            cb(null, logodata.File)
        }
    })
    let upload = multer({ storage, limits: { fileSize: 1000000 } }).single('logo')
    upload(req, res, function(err) {
        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                res.send('Błąd - rozmiar pliku przekracza dozwoloną wartość')
            } else {
                console.log(err)
                res.send('Podczas przesyłania pliku wystąpił błąd')
            }
        } else {
            logodata.Path = path
            callback(logodata)
        }
    })
}
var UploadTlo = function(req, res, callback) {
    let tlodata = ''
    let info = res.locals.user_data
    let path = ''
    let storage = multer.diskStorage({
        destination: function(req, file, cb) {
            path = `/data/Firma_${res.locals.info_session.Firma}/Templates`
            let dir = `${__dirname}/../../public/${path}`
            mkdirp(dir, err => cb(err, dir))
        },
        filename: function(req, file, cb) {
            tlodata = { OriginalName: file.originalname, File: Date.now() + '' }
            cb(null, tlodata.File)
        }
    })
    let upload = multer({ storage, limits: { fileSize: 1000000 } }).single('tlo')
    upload(req, res, function(err) {
        if (err) {
            if (err.code == 'LIMIT_FILE_SIZE') {
                res.send('Błąd - rozmiar pliku przekracza dozwoloną wartość')
            } else {
                console.log(err)
                res.send('Podczas przesyłania pliku wystąpił błąd')
            }
        } else {
            tlodata.Path = path
            callback(tlodata)
        }
    })
}
module.exports = router