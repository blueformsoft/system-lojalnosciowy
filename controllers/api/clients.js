var Client = require("../../models/client")
var Program = require("../../models/program")
var Point = require("../../models/point")
var Invoice = require("../../models/invoice")
var router = require('express').Router()
var functions = require("../../services/functions")
var bcrypt = require('bcryptjs')
var mailer = require("../../services/mailer")
var moment = require('moment')
router.use('/', functions.restrict_level, functions.restrict_program, function(req, res, next) {
    next()
})
router.post('/reset_password', function(req, res, next) {
    let token = bcrypt.hashSync(functions.ftGenerateToken(9), 10)
    let data = {
        Id: req.body.clientId,
        Firma: res.locals.info_session.Firma,
        Haslo_token: token
    }
    Client.Update(data, function(err, result) {
        if (err) { return next(err) }
        mailer.reset_password(req, req.body.clientEmail, res, token, req.body.clientId, req.body.clientEmail)
    })
})
router.post('/activate', function(req, res, next) {
    let data = {
        Id: req.body.id,
        Firma: res.locals.info_session.Firma,
        Aktywny: 1,
        Data_aktywacji: moment().format('YYYY-MM-DD HH:mm:ss')
    }
    Client.Update(data, function(err, result) {
        if (err) { return next(err) }
        res.sendStatus(200)
    })
})
router.get('/', function(req, res, next) {
    let info = res.locals.info_session
    let program = req.session.program
    Client.GetAllForProgram(info.Firma, program, function(err, clients) {
        if (err) { return next(err) }
        res.json({ clients, info_session: res.locals.info_session })
    })
})
router.get('/programs', function(req, res, next) {
    Client.Get(req.query.id, res.locals.info_session.Firma, function(err, client) {
        if (err) { return next(err) }
        res.json(client[0])
    })
})
router.get('/panel_data', function(req, res, next) {
    Program.GetProgramClientData(req.query.client, res.locals.info_session.Firma, res.locals.info_session.program.Id, function(data, polecajacy) {
        res.json({ client: data[0][0], poleceni: data[1], polecajacy })
    })
})
router.get('/program_data', function(req, res, next) {
    Program.GetProgramClientData(req.query.client, res.locals.info_session.Firma, res.locals.info_session.program.Id, function(data, polecajacy) {
        res.json({ clientdata: data[0][0], polecajacy })
    })
})

router.get('/list', function(req, res, next) {
    let program = req.session.program
    Client.GetAllForProgram(res.locals.info_session.Firma, program, function(err, clients) {
        if (err) { return next(err) }
        res.json(clients)
    })
})
router.post('/generate_codes', function(req, res) {
    Client.GenerateCodes(req.body.amount, req.session.program, res.locals.info_session.Firma, req.session.sid, function(err, result) {
        console.log(err)
        if (err) { return res.sendStatus(500) }
        res.sendStatus(200)
    })
})
router.post('/add', function(req, res, next) {
    let data = {
        Aktywny: true,
        Data_utworzenia: moment(Date.now()).format('YYYY-MM-DD'),
        Email: req.body.clientEmail,
        Nazwa: req.body.clientName,
        Id_sesji: req.session.sid,
        Id_programu: req.session.program,
        Firma: res.locals.info_session.Firma,
        Polecajacy: req.body.clientParent
    }
    Client.Validate(0, data.Email, data.Id_programu, data.Firma, function(err, result) {
        if (err) { return next(err) }
        if (result[0].length == 0) {
            req.body.clientGenerujNumer = true
            if (req.body.clientGenerujNumer) {
                var number = 0
                if (result[1][0])
                    number = parseInt(result[1][0].Numer)
                number++
                if (!req.body.clientDlugoscNumeru)
                    req.body.clientDlugoscNumeru = 6
                data.Numer = number.toString().padStart(req.body.clientDlugoscNumeru, '0')
            }
            Client.Insert(data, function(err, result) {
                if (err) { return next(err) }
                res.sendStatus(200)
            })
        } else
            res.json({ errors: ["Użytkownik o takim adresie e-mail już istnieje w tym programie"] })
    })
})
router.post('/update', function(req, res, next) {
    let data = {
        Id: req.body.clientId,
        Email: req.body.clientEmail,
        Nazwa: req.body.clientName,
        Id_sesji: req.session.sid,
        Firma: res.locals.info_session.Firma
    }
    Client.Validate(data.Id, data.Email, req.session.program, data.Firma, function(err, result) {
        if (err) { return next(err) }
        if (result[0].length == 0) {
            Client.Update(data, function(err, result) {
                if (err) { return next(err) }
                res.sendStatus(200)
            })
        } else {
            res.json({ errors: ["Użytkownik o takim adresie e-mail już istnieje w tym programie"] })
        }
    })
})

router.post('/count_points', function(req, res, next) {
    Program.Get(req.body.Id_programu, res.locals.info_session.Firma, function(err, prog) {
        if (err) { return next(err) }
        let wysokosc_premii = parseFloat(prog[0].Wysokosc_premii)
        let kwota = parseFloat(req.body.data_Kwota)
        let wartosc_punktu = parseFloat(prog[0].Wartosc_punktu)
        var punkty = wysokosc_premii * 0.005 * kwota / wartosc_punktu
        res.json({ Punkty: punkty })
            // Point.GetByClientId(req.body.Id_client, function(err, points) {
            //     if (err) { return next(err) }
            // })
    })
})
router.post('/add_points', function(req, res, next) {
    Program.GetProgramClientData(req.body.client, req.body.Firma, req.body.program, function(client, polecajacy, program) {
        res.json({ clientdata: data[0][0], polecajacy })
    })
})
router.post('/update_account', function(req, res, next) {
    var data = {
        Aktywny: 1,
        Data: req.body.data.Data,
        Firma: res.locals.info_session.Firma,
        Id_klienta: req.body.data.Id_klienta,
        Id_programu: req.body.data.Id_programu,
        Id_sesji: req.session.sid,
        Ilosc: req.body.data.Punkty,
        Kwota: req.body.data.Kwota,
        Opis: req.body.data.Opis,
        Id_parent: 0,
        Zatwierdzone: 1,
        Id_fv: req.body.data.Id_fv
    }
    if (req.body.data.Id) {
        data.Id = req.body.data.Id
        Point.UpdateAll(data, req.body.polecajacy, function(err, result) {
            console.log(err)
            if (err) { return next(err) }
            res.send(200)
            mailer.send_account_state(req.body.data.Id_klienta, function() {})
            for (let i = 0; i < req.body.polecajacy.length; i++) {
                mailer.send_account_state(req.body.polecajacy[i][0].Id, function() {})
            }
        })
    } else {
        Point.AddPoints(data.Id_klienta, data.Firma, data.Kwota, data.Opis, data.Id_fv, data.Data, data.Zatwierdzone, data.Id_programu, data.Id_sesji, function(result) {
            res.sendStatus(200)
        })
    }

})
router.get('/edit', function(req, res, next) {
    Client.Get(req.query.id, res.locals.info_session.Firma, function(err, data) {
        if (err) { return next(err) }
        if (data.length > 0)
            res.json({ client: data[0] })
        else
            res.send(404)
    })
})
router.post('/remove_document', function(req, res, next) {
    Point.Remove(req.body.Id, req.body.Desc, res.locals.info_session.Firma, function(err, data) {
        if (err) { return next(err) }
        res.sendStatus(200)
    })
})
router.post('/confirm_document', function(req, res, next) {
    Point.Confirm(req.body.fv, res.locals.info_session.Firma, function(err, data) {
        if (err) { return next(err) }
        res.sendStatus(200)
    })
})
router.post('/importFV', function(req, res, next) {
    Invoice.Import(req.body.fv, res.locals.info_session.Firma, function(err, data) {
        if (err) throw err
        if (data.length == 0)
            res.json({ errors: ["Nie istnieje faktura o podanym numerze."] })
        else if (data[0].Punkty)
            res.json({ errors: ["Przydzielono już punkty na podstawie tej faktury"] })
        else
            res.json(data[0])
    })
})
router.get('/get_account_data', function(req, res, next) {
    Point.GetByClientId(req.query.id, res.locals.info_session.Firma, function(err, data) {
        if (err) { return next(err) }
        res.json({ data: data, recordsTotal: data.length })
    })
})
router.get('/get_polecajacy', function(req, res, next) {
    Client.ExecuteManyQueries([Program.GetParentList(req.session.program, res.locals.info_session.Firma), Client.GetAll(res.locals.info_session.Firma)], function(err, data) {
        if (err) { return next(err) }
        res.json({ parents: data[0], clients: data[1] })
    })
})

module.exports = router