var router = require('express').Router()
var UserClient = require('../../models/userclient')
var Session = require('../../models/clientsession')
var bcrypt = require('bcryptjs')
var jwt = require('jwt-simple')
var config = require('../../config')

router.get('/data', function(req, res) {
    Session.GetData(req, res)
})

router.post('/', function(req, res) {
    UserClient.Login(req, res, { login: req.body.login, haslo: req.body.haslo, firma: req.body.firma, program: req.body.program })
})
router.post('/logout', function(req, res) {
    UserClient.Logout(req, res)
})
module.exports = router