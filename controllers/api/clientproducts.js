var Product = require("../../models/product")
var router = require('express').Router()
var functions = require("../../services/functions")
var moment = require('moment')

router.use('/', functions.restrict_client_level, function(req, res, next) {
    next()
})
router.get('/get', function(req, res) {
    Product.Get(req.query.id, res.locals.info_session.Firma, function(err, data) {
        if (err) { throw (err) }
        if (data.length > 0)
            res.json({ prize: data[0] })
        else
            res.sendStatus(404)
    })
})
router.get('/', function(req, res) {
    Product.GetAll(res.locals.info_session.Firma, function(err, programs) {
        if (err) { throw (err) }
        res.json(programs)
    })
})
module.exports = router