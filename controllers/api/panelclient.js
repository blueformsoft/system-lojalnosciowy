var Client = require("../../models/client")
var Program = require("../../models/program")
var Point = require("../../models/point")
var router = require('express').Router()
var functions = require("../../services/functions")
var bcrypt = require('bcryptjs')
var mailer = require("../../services/mailer")
var moment = require('moment')
router.use('/', functions.restrict_client_level, function(req, res, next) {
    next()
})
router.get('/panel_data', function(req, res, next) {
    Client.ExecuteManyQueries([Program.GetByClientId(res.locals.info_session.Id, res.locals.info_session.Firma)], function(err, data) {
        if (err) { return next(err) }
        res.json({ client: data[0], programs: data[1] })
    })
})
router.get('/get_account_data', function(req, res, next) {
    Point.GetByClientId(res.locals.info_session.Id, res.locals.info_session.Firma, function(err, data) {
        if (err) { return next(err) }
        res.json({ data: data, recordsTotal: data.length })
    })
})
router.get('/program_data', function(req, res, next) {
    Program.GetProgramClientData(res.locals.info_session.Id, res.locals.info_session.Firma, res.locals.info_session.program.Id, function(data, polecajacy) {
        res.json({ client: data[0][0], poleceni: data[1], polecajacy })
    })
})
module.exports = router