var Order = require("../../models/order")
var router = require('express').Router()
var functions = require("../../services/functions")
var mailer = require("../../services/mailer")
var moment = require('moment')
router.use('/', functions.restrict_level, functions.restrict_program, function(req, res, next) {
    next()
})
router.get('/details/:id', function(req, res) {
    Order.Get(req.params.id, res.locals.info_session.Firma, function(err, data) {
        if (err) { throw (err) }
        if (data.length > 0)
            res.json({ order: data[0] })
        else
            res.sendStatus(404)
    })
})
router.post('/add', function(req, res) {
    let data = req.body
    data.Id_klienta = res.locals.info_session.Id
    data.Data_zamowienia = Date.now()
    data.Status = 'ZAM'
    data.Aktywny = 1
    data.Firma = res.locals.info_session.Firma
    Order.Create(data, function(err, data) {
        if (err) { throw (err) }
        res.send(200)
    })
})
router.post('/update', function(req, res) {
    let data = req.body
    data.Firma = res.locals.info_session.Firma
    data.Id_sesji = req.session.sid
    Order.Update(data, function(err, result) {
        if (err) { throw (err) }
        if (data.Status == 'ZRE')
            mailer.realize_order(data.Id, function(err, info) {
                if (err) {
                    console.log(err)
                    res.json({ errors: ["Wystąpił błąd podczas wysyłania wiadomości."] })
                } else
                    res.sendStatus(200)
            })
    })
})
router.post('/remove', function(req, res) {
    let data = req.body
    data.Firma = res.locals.info_session.Firma
    Order.Remove(data, function(err, data) {
        if (err) { throw (err) }
        res.send(200)
    })
})
router.get('/get', function(req, res) {
    Order.GetAll(req.session.program, res.locals.info_session.Firma, function(err, data) {
        if (err) { throw (err) }
        res.send(data)
    })
})
module.exports = router