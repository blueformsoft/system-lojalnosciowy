var Client = require("../../models/client")
var UserClient = require("../../models/userclient")
var Program = require("../../models/program")
var Point = require("../../models/point")
var router = require('express').Router()
var functions = require("../../services/functions")
var bcrypt = require('bcryptjs')
var mailer = require("../../services/mailer")
var moment = require('moment')

router.post('/register', function(req, res, next) {
    let token = bcrypt.hashSync(functions.ftGenerateToken(9), 10)
    UserClient.Register(req.body, token, function(err, data) {
        if(err)
        res.json({ errors: [err] })
        else{
            mailer.activate_mail(req, req.body.Email, res, token, data, req.body.Firma, req.body.Id_programu)
        }
    })
})
router.post('/activate_mail', function(req, res, next){
    mailer.activate_mail(req, req.body.email, res, req.body.token, req.body.id, req.body.Firma, req.body.Program )
})
router.post('/set_password', function(req, res, next) {
    bcrypt.hash(req.body.haslo, 10, (err, hash) => {
        if (err) console.log(err)
        let data = {
            Id: req.body.Id,
            Haslo: hash,
            Haslo_token: req.body.token
        }
        Client.SetPassword(data, function(err, data) {
            if (err) next(err)
            res.sendStatus(200)
        })
    })
})
router.post('/check_number', function(req, res, next) {
    let data = {
        Id: req.body.Id,
        Haslo_token: req.body.token
    }
    Client.CheckNumber(data, function(err, data) {
        if (err) next(err)
        if (data.length > 0 && req.body.number == data[0].Numer)
            res.sendStatus(200)
        else
            res.send(false)
    })
})
router.post('/change_password', functions.restrict_client_level, function(req, res, next) {
    let data = req.body
    data.Id = res.locals.info_session.Id
    data.Firma = res.locals.info_session.Firma
    UserClient.ChangePassword(data, function(err, result) {
        if (err)
            res.json({ errors: ["Hasło nie jest poprawne"] })
        else
            res.sendStatus(200)
    })
})
module.exports = router