var router = require('express').Router()
var Invoice = require("../../models/invoice")
var Point = require("../../models/point")
var functions = require("../../services/functions")

router.post('/add', functions.restrict_remote, function(req, res, next) {
    Invoice.Get(req.body.id_fv, res.locals.info_session.Firma, function(err, result) {
        if (err) {
            res.json({ error: "Nie ma takiej faktury" })
            return
        }
        Point.AddPoints(res.locals.info_session.klient.Id, res.locals.info_session.Firma, result[0].Do_zaplaty, result[0].Nr_faktury, req.body.id_fv, result[0].Data_wystawienia, result[0].Zaplacona, res.locals.info_session.program.Id, req.body.sid, function(result) {
            if (err) throw err
            res.json({ info: `${res.locals.info_session.klient.Nazwa} otrzymał ${result.Ilosc} punktów.` })
        })
    })
})

module.exports = router