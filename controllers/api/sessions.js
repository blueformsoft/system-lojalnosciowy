var router = require('express').Router()
var User = require('../../models/user')
var Session = require('../../models/session')
var Serwer = require('../../services/serwer')

router.get('/footer', function(req, res) {
    Serwer.GetFooter(function(body) {
        res.send({ body })
    })
})
router.get('/data', function(req, res) {
    Session.GetData(req, res)
})
router.post('/go_to_main', function(req, res){
    delete req.session.program
    res.end() 
})
router.post('/', function(req, res) {
    Serwer.request.post(Serwer.apiserwer + 'login').send({
        login: req.body.login,
        password: req.body.haslo,
        secretkey: Serwer.secretkey
    }).end(function(err, response) {
        console.log(err)
        if (response && response.error && (response.code == 'sess_timeout' || response.code == 'license_exp')) {
            return res.sendStatus(403)
        }
        let result = response.body
        if (result.success) {
            var next = req.query.next ? '?next=' + req.query.next : ''
            res.locals.next = next
            console.log('secure1', req.secure)
            req.secure = true
            console.log(req.headers["x-forwarded-proto"])
            console.log('secure2', req.secure)
            console.log('session', req.session)
            req.session.sid = result.sid
            req.session.key = result.key
            // req.session.regenerate(function(err) {
            //     console.log(err)
            // })
            req.session.save()
            console.log('session', req.session)
            return res.send({ success: true, redirect: '/' + next })

        } else {
            return res.send({ success: false, err: result.error })
        }
    })
})
router.post('/logout', function(req, res) {
    User.Logout(req, res)
})
module.exports = router