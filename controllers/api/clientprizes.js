var Prize = require("../../models/prize")
var router = require('express').Router()
var functions = require("../../services/functions")
var moment = require('moment')

router.use('/', functions.restrict_client_level, function(req, res, next) {
    next()
})
router.get('/details/:id', function(req, res) {
    Prize.GetDetails(req.params.id, res.locals.info_session.Firma, function(err, data) {
        if (err) { throw (err) }
        if (data.length > 0) {
            res.json({ prize: data[0][0], repository: data[1][0] })
        } else
            res.sendStatus(404)
    })
})
router.get('/', function(req, res) {
    let program = res.locals.info_session.Id_programu
    Prize.GetAll(program, res.locals.info_session.Firma, function(err, products) {
        if (err) { throw (err) }
        res.json(products)
    })
})
module.exports = router