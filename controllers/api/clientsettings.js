var Client = require("../../models/client")
var Program = require("../../models/program")
var Point = require("../../models/point")
var router = require('express').Router()
var functions = require("../../services/functions")
var bcrypt = require('bcryptjs')
var mailer = require("../../services/mailer")
var moment = require('moment')
router.use('/', functions.restrict_client_level, function(req, res, next) {
    next()
})
router.post('/reset_password', function(req, res, next) {
    let token = bcrypt.hashSync(functions.ftGenerateToken(9), 10)
    let data = {
        Id: req.body.clientId,
        Firma: res.locals.info_session.Firma,
        Haslo_token: token
    }
    Client.Update(data, function(err, result) {
        if (err) { return next(err) }
        mailer.reset_password(req, req.body.clientEmail, res, token, req.body.clientId, req.body.clientEmail)
    })
})

router.post('/update', function(req, res, next) {
    let data = req.body
    data.Id_sesji = req.session.clientsid
    data.Firma = res.locals.info_session.Firma
    Client.Update(data, function(err, result) {
        if (err) { return next(err) }
        res.sendStatus(200)
    })
})

router.get('/edit', function(req, res, next) {
    Client.Get(res.locals.info_session.Id, res.locals.info_session.Firma, function(err, data) {
        if (err) { return next(err) }
        if (data.length > 0)
            res.json({ client: data[0] })
        else
            res.send(404)
    })
})

module.exports = router