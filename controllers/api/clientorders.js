var Order = require("../../models/order")
var Prize = require("../../models/prize")
var router = require('express').Router()
var functions = require("../../services/functions")
var mailer = require("../../services/mailer")
var moment = require('moment')
router.use('/', functions.restrict_client_level, function(req, res, next) {
    next()
})
router.get('/details/:id', function(req, res) {
    Order.Get(req.params.id, res.locals.info_session.Firma, function(err, data) {
        if (err) { throw (err) }
        if (data.length > 0)
            res.json({ order: data[0] })
        else
            res.sendStatus(404)
    })
})
router.post('/add', function(req, res) {
    let data = req.body
    data.Id_klienta = res.locals.info_session.Id
    data.Data_zamowienia = moment(Date.now()).format('YYYY-MM-DD HH:mm')
    data.Status = 'ZAM'
    data.Aktywny = 1
    data.Id_sesji = req.session.clientsid
    data.Firma = res.locals.info_session.Firma
    data.Id_programu = res.locals.info_session.Id_programu
    Order.Validate(data, function(err) {
        if (err)
            res.json({ errors: err })
        else
            Order.Create(data, function(err, data) {
                if (err) { throw (err) }
                mailer.new_order(data.insertId, function(err, info) {
                    if (err) {
                        throw (err)
                        res.json({ errors: ["Wystąpił błąd podczas wysyłania wiadomości."] })
                    } else {
                        res.sendStatus(200)
                        mailer.send_product(data.insertId, function(err, info) {
                            if (err) throw (err)
                        })
                    }
                })
            })
    })
})
router.post('/update', function(req, res) {
    let data = req.body
    data.Firma = res.locals.info_session.Firma
    data.Id_sesji = req.session.clientsid
    Order.Update(data, function(err, result) {
        if (err) { throw (err) }
        if (data.Status == 'ANU')
            mailer.cancel_order(data.Id, function(err, info) {
                if (err) {
                    throw (err)
                    res.json({ errors: ["Wystąpił błąd podczas wysyłania wiadomości."] })
                } else
                    res.sendStatus(200)
            })
    })
})
router.get('/get', function(req, res) {
    Order.GetAllByClient(res.locals.info_session.Id, res.locals.info_session.Firma, function(err, data) {
        if (err) { throw (err) }
        res.send(data)
    })
})
module.exports = router