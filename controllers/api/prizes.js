var Prize = require("../../models/prize")
var Repository = require("../../models/repository")
var router = require('express').Router()
var functions = require("../../services/functions")
var moment = require('moment')
var fs = require('fs')
var mkdirp = require('mkdirp')
var multer = require('multer')

router.use('/', functions.restrict_level, functions.restrict_program, function(req, res, next) {
    next()
})
router.get('/edit', function(req, res) {
    Prize.Get(req.query.id, res.locals.info_session.Firma, function(err, data) {
        if (err) { throw (err) }
        if (data.length > 0)
            res.json({ prize: data[0] })
        else
            res.sendStatus(404)
    })
})
router.get('/', function(req, res) {
    let program = req.session.program
    Prize.GetAll(program, res.locals.info_session.Firma, function(err, products) {
        if (err) { throw (err) }
        res.json(products)
    })
})
router.post('/add', function(req, res) {
    let data = req.body
    data.Id_sesji = req.session.sid
    data.Firma = res.locals.info_session.Firma
    data.Aktywny = true
    data.Program = req.session.program
    Prize.Create(data, function(err, data) {
        if (err) { throw (err) }
        res.send(201)
    })
})
router.post('/upload', function(req, res) {
    UploadObraz(req, res, function(filedata) {
        let data = JSON.parse(req.body.item)
        data.Obraz = JSON.stringify(filedata)
        Prize.Update(data, function(err, data) {
            if (err) { throw (err) }
            res.send(200)
        })
    })
})
router.post('/update', function(req, res) {
    let data = req.body
    Prize.Update(data, function(err, data) {
        if (err) { throw (err) }
        res.send(200)
    })
})
router.get('/get_repository', function(req, res) {
    Repository.GetAllByPrizeId(req.query.id, res.locals.info_session.Firma, function(err, data) {
        if (err) throw (err)
        res.json(data)
    })
})
router.post('/repository/remove', function(req, res) {
    if (req.body.Zalacznik) {
        let plik = JSON.parse(req.body.Plik)
        let oldPath = plik.Path
        plik.Path = oldPath + '/Usuniete'
        var data = {
            Id: req.body.Id,
            Firma: res.locals.info_session.Firma,
            Aktywny: false,
            Id_sesji: req.session.sid,
            Plik: JSON.stringify(plik)
        }
        let oldDir = `${__dirname}/../../${oldPath}`
        let newDir = `${__dirname}/../../${plik.Path}`
        mkdirp(newDir, function(err, dir) {
            fs.rename(`${oldDir}/${plik.File}`, `${newDir}/${plik.File}`, function(err) {
                if (err) throw (err)
                Repository.Update(data, function(err, result) {
                    if (err) throw err
                    res.sendStatus(200)
                })
            })
        })
    } else {
        var data = {
            Id: req.body.Id,
            Firma: res.locals.info_session.Firma,
            Aktywny: false,
            Id_sesji: req.session.sid
        }
        Repository.Update(data, function(err, result) {
            if (err) throw err
            res.sendStatus(200)
        })
    }
})
router.post('/repository/confirm', function(req, res) {
    var data = {
        Id: req.body.Id,
        Firma: res.locals.info_session.Firma,
        Id_sesji: req.session.sid,
        Wydane: true,
        Data_wydania: moment().format('YYYY-MM-DD HH:mm:ss')
    }
    Repository.Update(data, function(err, result) {
        if (err) throw err
        res.sendStatus(200)
    })
})
router.post('/repository/update', function(req, res) {
    let data = {
        Firma: res.locals.info_session.Firma,
        Plik: '',
        Aktywny: true,
        Id_nagrody: req.body.Id_nagrody,
        Id_sesji: req.session.sid,
        Opis: req.body.Opis,
        Wydane: false,
        Zalacznik: false
    }
    if (req.body.Id) {
        data.Id = req.body.Id
        Repository.Update(data, function(err, result) {
            if (err) throw err
            res.sendStatus(200)
        })
    } else {
        Repository.Create(data, function(err, result) {
            if (err) throw err
            res.sendStatus(200)
        })
    }
})
router.post('/repository/upload', function(req, res) {
    UploadFile(req, res, function(filedata) {
        let item = JSON.parse(req.body.item)
        let data = {
            Firma: res.locals.info_session.Firma,
            Plik: JSON.stringify(filedata),
            Aktywny: true,
            Id_nagrody: item.Id_nagrody,
            Id_sesji: req.session.sid,
            Opis: item.Opis,
            Wydane: false,
            Zalacznik: true
        }
        if (item.Id) {
            data.Id = item.Id
            Repository.Update(data, function(err, result) {
                if (err) throw err
                res.sendStatus(200)
            })
        } else {
            Repository.Create(data, function(err, result) {
                if (err) throw err
                res.sendStatus(200)
            })
        }
    })
})
var UploadFile = function(req, res, callback) {
    let filedata = ''
    let info = res.locals.user_data
    let path = ''
    let storage = multer.diskStorage({
        destination: function(req, file, cb) {
            let item = JSON.parse(req.body.item)
            path = `/data/Firma_${res.locals.info_session.Firma}/Program_${req.session.program}/Nagroda_${item.Id_nagrody}`
            let dir = `${__dirname}/../../${path}`
            mkdirp(dir, err => cb(err, dir))
        },
        filename: function(req, file, cb) {
            filedata = { OriginalName: file.originalname, File: Date.now() + '' }
            cb(null, filedata.File)
        }
    })
    let upload = multer({ storage, limits: { fileSize: 10 * 1024 * 1024 } }).single('file')
    upload(req, res, function(err) {
        if (err) {
            console.log(err)
            if (err.code == 'LIMIT_FILE_SIZE') {
                res.send('Błąd - rozmiar pliku przekracza dozwoloną wartość')
            } else {
                console.log(err)
                res.send('Podczas przesyłania pliku wystąpił błąd')
            }
        } else {
            filedata.Path = path
            callback(filedata)
        }
    })
}
var UploadObraz = function(req, res, callback) {
    let filedata = ''
    let info = res.locals.user_data
    let path = ''
    let storage = multer.diskStorage({
        destination: function(req, file, cb) {
            let item = JSON.parse(req.body.item)
            path = `/public/images/Firma_${res.locals.info_session.Firma}/Program_${req.session.program}`
            let dir = `${__dirname}/../../${path}`
            mkdirp(dir, err => cb(err, dir))
        },
        filename: function(req, file, cb) {
            filedata = { OriginalName: file.originalname, File: Date.now() + '' }
            cb(null, filedata.File)
        }
    })
    let upload = multer({ storage, limits: { fileSize: 10 * 1024 * 1024 } }).single('file')
    upload(req, res, function(err) {
        if (err) {
            console.log(err)
            if (err.code == 'LIMIT_FILE_SIZE') {
                res.send('Błąd - rozmiar pliku przekracza dozwoloną wartość')
            } else {
                console.log(err)
                res.send('Podczas przesyłania pliku wystąpił błąd')
            }
        } else {
            filedata.Path = path
            callback(filedata)
        }
    })
}
module.exports = router