var Department = require("../../models/department")
var Program = require("../../models/program")
var Prize = require("../../models/prize")
var router = require('express').Router()
var functions = require("../../services/functions")
var moment = require('moment')
var mailer = require("../../services/mailer")
var bcrypt = require('bcryptjs')
var Client = require("../../models/client")

router.post('/getprizes', function(req, res) {
    Prize.ExecuteManyQueries([`Select * From sl_nagrody WHERE Firma = ${req.body.firma} AND Program = ${req.body.program}`], function(err, data) {
        res.json(data)
    })
})

router.get('/details/:firma/:program', function(req, res) {
    Department.ExecuteManyQueries([Department.Get(req.params.firma), Program.Get(req.params.program, req.params.firma)], function(err, data) {
        if (err) { throw (err) }
        res.json({ Firma: data[0][0], Program: data[1][0] })
    })
})
router.post('/add', function(req, res) {
    let data = req.body
    data.Id_klienta = res.locals.info_session.Id
    data.Data_zamowienia = moment(Date.now()).format('YYYY-MM-DD HH:mm')
    data.Status = 'ZAM'
    data.Aktywny = 1
    data.Id_sesji = req.session.sid
    data.Firma = res.locals.info_session.Firma
    data.Id_programu = res.locals.info_session.Id_programu
    Order.Validate(data, function(err) {
        if (err)
            res.json({ errors: err })
        else
            Order.Create(data, function(err, data) {
                if (err) { throw (err) }
                mailer.new_order(data.insertId, function(err, info) {
                    if (err) {
                        throw (err)
                        res.json({ errors: ["Wystąpił błąd podczas wysyłania wiadomości."] })
                    } else {
                        res.sendStatus(200)
                        mailer.send_product(data.insertId, function(err, info) {
                            if (err) throw (err)
                        })
                    }
                })
            })
    })
})
router.post('/update', function(req, res) {
    let data = req.body
    data.Firma = res.locals.info_session.Firma
    data.Id_sesji = req.session.sid
    Order.Update(data, function(err, result) {
        if (err) { throw (err) }
        if (data.Status == 'ANU')
            mailer.cancel_order(data.Id, function(err, info) {
                if (err) {
                    throw (err)
                    res.json({ errors: ["Wystąpił błąd podczas wysyłania wiadomości."] })
                } else
                    res.sendStatus(200)
            })
    })
})
router.post('/sendmail', function(req, res) {
    mailer.kontakt(req, res)
})
router.get('/get', function(req, res) {
    Order.GetAllByClient(res.locals.info_session.Id, res.locals.info_session.Firma, function(err, data) {
        if (err) { throw (err) }
        res.send(data)
    })
})
router.post('/reset_password', function(req, res, next) {
    Client.GetByLogin(req.body.program, req.body.firma, req.body.login, function(err, result) {
        if (err) throw err
        if (result.length == 0)
            res.json({ errors: ["Brak użytkownika o takim loginie."] })
        else {
            let token = bcrypt.hashSync(functions.ftGenerateToken(9), 10)
            let data = {
                Id: result[0].Id,
                Firma: result[0].Firma,
                Haslo_token: token
            }
            Client.Update(data, function(err, data) {
                if (err) { return next(err) }
                mailer.reset_password(req, req.body.login, res, token, result[0].Id, req.body.login, req.body.firma, req.body.program)
            })
        }
    })

})
module.exports = router