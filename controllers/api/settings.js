var Settings = require("../../models/settings")
var router = require('express').Router()
var functions = require("../../services/functions")

router.get('/', functions.restrict_level, function(req, res, next) {
    let info = JSON.parse(req.session.info)
    Settings.GetAll(info.firma, function(err, settings) {
        if (err) { return next(err) }
        if (settings == null)
            Settings.Insert(info.firma, req.session.sid, function(err, settings) {
                if (err) { return next(err) }
                Settings.GetAll(info.firma, function(err, settings) {
                    if (err) { return next(err) }
                    res.json({ settings })
                })
            })
        res.json({ settings })
    })
})
router.get('/update', functions.restrict_level, function(req, res, next) {
    let info = JSON.parse(req.session.info)
    Settings.Update(req.body.settings, function(err, data) {
        if (err) return next(err)
        res.send(200)
    })
})
module.exports = router