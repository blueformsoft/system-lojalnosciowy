var express = require('express')
var router = require('express').Router()
var functions = require("../services/functions")
var Client = require("../models/client")
router.use(express.static(__dirname + '/../assets'))
router.use(express.static(__dirname + '/../templates'))
router.use(express.static(__dirname + '/../public'))
router.get('/', functions.restrict_level, function(req, res) {
    if (req.session.program)
        res.sendfile("layouts/loginTemplate.html")
    else
        res.sendfile("layouts/selectProgramTemplate.html")
})
router.get('/programs', functions.restrict_level, function(req, res) {
    res.sendfile("layouts/selectProgramTemplate.html")
})
router.get(['/programs/add', '/programs/edit'], functions.restrict_level, function(req, res) {
    res.sendfile("layouts/addEditProgramTemplate.html")
})

router.get('/client/:firma/:program', functions.restrict_main_client_level, function(req, res) {
    res.sendfile("layouts/loginClientTemplate.html")
})
router.get('/client/:firma/:program/activate', function(req, res) {
    if (req.query.id && req.query.token && req.params.program && req.params.firma) {
        Client.Activate(req.query.id, req.query.token, req.params.program, req.params.firma, function(err, doc) {
            console.log(err)
            if (doc[1].length > 0 && doc[1][0].Haslo_token == req.query.token)
                res.sendfile("layouts/clientActivate.html")
            else
                res.sendfile("layouts/clientFailActivate.html")
        })
    } else
        res.sendfile("layouts/clientFailActivate.html")
})
router.get('/client/:firma/:program/regulamin', function(req, res) {
    res.sendfile("layouts/regulamin.html")
})
router.get('/client/:firma/:program/reset_password', function(req, res) {
    res.sendfile("layouts/resetPassword.html")
})
router.get('/client/:firma/:program/kontakt', function(req, res) {
    res.sendfile("layouts/kontakt.html")
})
router.get('/client/:firma/:program/register', function(req, res) {
    res.sendfile("layouts/clientRegister.html")
})
router.get('/client', functions.restrict_client_level, function(req, res) {
    res.sendfile("layouts/loginClientTemplate.html")
})
router.get('/client/:firma/:program/setpassword', function(req, res) {
    let data = {
        Id: req.query.id,
        Haslo_token: req.query.token,
        Firma: req.params.firma
    }
    Client.CheckToken(data, function(err, doc) {
        if (err) throw err
        if (doc[0]) {
            let data = {
                token: req.query.token,
                id: req.query.id,
                strona: 'Resetowanie hasła'
            }
            res.sendfile("layouts/setPassword.html")
        } else {
            res.sendfile("layouts/tokenError.html")
        }
    })
})
module.exports = router