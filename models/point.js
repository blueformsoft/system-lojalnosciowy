var dbelement = require('./dbelement')
var Program = require("./program")
var mailer = require("../services/mailer")

var point = function() {
    dbelement.call(this, 'sl_punkty')
    this.GetByClientId = function(klient, firma, callback) {
        let query = `SELECT sp.*, k.Numer as Parent From sl_punkty sp
        LEFT JOIN sl_punkty pt ON sp.Id_parent = pt.Id
        LEFT JOIN sl_klienci k ON k.Id = pt.Id_klienta 
        Where sp.Firma = ${firma} AND sp.Id_klienta = ${klient} AND sp.Aktywny = 1
        ORDER BY sp.Data DESC`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.UpdateAll = function(data, polecajacy, callback) {
        let query = `UPDATE sl_punkty SET Data = ${this.db.escape(data.Data)}, Ilosc = ${ this.db.escape(data.Ilosc)}, 
        Kwota = ${this.db.escape(data.Kwota)}, Opis = ${this.db.escape(data.Opis)} Where ID = ${data.Id} AND Firma = ${data.Firma}`
        for (var i = 0; i < polecajacy.length; i++) {
            query += `; UPDATE sl_punkty SET Data = ${this.db.escape(data.Data)}, Ilosc = ${ this.db.escape(polecajacy[i][0].Punkty)}, 
            Kwota = ${this.db.escape(data.Kwota)}, Opis = ${this.db.escape(data.Opis)} Where Id_parent = ${data.Id} AND Id_klienta = ${polecajacy[i][0].Id} AND Firma = ${data.Firma}`
        }
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.InsertMultiple = function(data, callback) {
        this.db.query(`INSERT INTO sl_punkty 
        (Aktywny,
            Data,
            Firma,
            Id_klienta,
            Id_programu,
            Id_sesji,
            Ilosc,
            Kwota,
            Opis,
            Id_parent,
            Zatwierdzone) VALUES ?`, [data], callback)
    }.bind(this)
    this.Remove = function(id, desc, firma, callback) {
        let query = `Update sl_punkty Set Aktywny = 0, Opis_del = ${ this.db.escape(desc)} 
        Where (Id = ${ this.db.escape(id)} OR Id_parent = ${ this.db.escape(id)}) AND Firma = ${ this.db.escape(firma)}`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.Confirm = function(id, firma, callback) {
        let query = `Update sl_punkty Set Zatwierdzone = 1 
        Where (Id = ${ this.db.escape(id)} OR Id_parent = ${ this.db.escape(id)}) AND Firma = ${ this.db.escape(firma)}`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.AddPoints = function(klient, firma, kwota, nr_faktury, id_fv, data_fv, zatwierdzone, id_programu, sid, callback) {
        Program.GetProgramClientData(klient, firma, id_programu, function(client, polecajacy, program) {
            var data = {
                Aktywny: 1,
                Data: data_fv,
                Firma: firma,
                Id_klienta: klient,
                Id_programu: id_programu,
                Id_sesji: sid,
                Ilosc: 0,
                Kwota: kwota,
                Opis: nr_faktury,
                Id_parent: 0,
                Zatwierdzone: zatwierdzone
            }
            if (id_fv)
                data.Id_fv = id_fv
            if (!program.MLM) {
                var premia = program.Wysokosc_premii
                if (!program.Punkty && program.Czy_pierwsza_premia)
                    premia = program.Wysokosc_pierwszej_premii
                var punkty = premia * 0.005 * data.Kwota
                data.Ilosc = parseFloat(punkty.toFixed(0))
                var liczba_poziomow = 1
                for (var i = polecajacy.length - 1; i >= 0; i--) {
                    liczba_poziomow++
                    punkty = 0.5 * punkty
                    polecajacy[i][0].Punkty = parseFloat(punkty.toFixed(0))
                    if (liczba_poziomow > program.Liczba_poziomow)
                        break
                }
            } else {
                let premia = program.Procent_dla_sieci * data.Kwota * 0.01
                data.Ilosc = parseFloat(premia * program.Procent_dla_kupujacego * 0.01)
                premia = premia - data.Ilosc
                if (polecajacy.length > 0) {
                    premia = parseFloat((premia / polecajacy.length).toFixed(0))
                    for (var i = 0; i < polecajacy.length; i++)
                        polecajacy[i][0].Punkty = premia
                }
            }
            this.db.beginTransaction(function(err) {
                if (err) {
                    this.db.rollback(function() {
                        throw err;
                    })
                }
                this.Create(data, function(err, result) {
                    if (err) {
                        this.db.rollback(function() {
                            throw err;
                        })
                    }
                    if (polecajacy.length > 0) {
                        var pol = []
                        for (var i = 0; i < polecajacy.length; i++) {
                            pol.push([1, data.Data, data.Firma, polecajacy[i][0].Id,
                                data.Id_programu, sid, polecajacy[i][0].Punkty, data.Kwota,
                                data.Opis, result.insertId, 1
                            ])
                        }
                        this.InsertMultiple(pol, function(err, result) {
                            if (err) {
                                this.db.rollback(function() {
                                    throw err;
                                })
                            }
                            this.db.commit(function(err) {
                                if (err) {
                                    this.db.rollback(function() {
                                        throw err
                                    })
                                }
                                mailer.send_account_state(klient, function(){})
                                for (var i = 0; i < polecajacy.length; i++) {
                                    mailer.send_account_state(polecajacy[i][0].Id, function(){})
                                }
                                callback(data)
                            }.bind(this))
                        }.bind(this))
                    } else {
                        this.db.commit(function(err) {
                            if (err) {
                                this.db.rollback(function() {
                                    throw err
                                })
                            }
                            callback(data)
                        }.bind(this))
                    }
                }.bind(this))
            }.bind(this))
        }.bind(this))
    }.bind(this)
}
module.exports = new point()