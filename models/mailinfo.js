var dbelement = require('./dbelement')

var mailinfo = function() {
    dbelement.call(this, 'sl_punkty')
    this.GetInfo = function(klient, callback) {
        let query = `SELECT k.Email, k.Nazwisko, k.Imie, k.Numer, pr.Mail_zmiana_salda, pr.Nazwa as Program, SUM(case when p.Aktywny = 1 then p.Ilosc else 0 end) Aktualny_stan_konta, 
        (SELECT pt.Ilosc FROM sl_punkty pt WHERE pt.Id_klienta = ${klient} ORDER BY DateUpd DESC LIMIT 1) as Zmiana_stanu_konta, 
        (SELECT Case when pt.Aktywny = 1 then pt.Opis else pt.Opis_del end FROM sl_punkty pt WHERE pt.Id_klienta = ${klient} ORDER BY DateUpd DESC LIMIT 1) as Opis_zmiany
        FROM sl_klienci k
        LEFT JOIN sl_punkty p ON p.Id_klienta = k.Id
        LEFT JOIN sl_programy pr ON pr.Id = k.Id_programu
        LEFT JOIN Oddzialy o ON o.ID = pr.Firma
        Where k.Id = ${klient}
        GROUP BY k.Email, k.Nazwisko, k.Imie, k.Numer, pr.Mail_zmiana_salda, pr.Nazwa`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
}
module.exports = new mailinfo()