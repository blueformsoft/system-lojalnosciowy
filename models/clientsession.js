var db = require('../db')
var config = require('../config')
var functions = require('../services/functions')
var moment = require('moment')
var clientsession = function() {
    this.GetData = function(req, res) {
        if (req.session.clientsid) {
            let query = `SELECT Sum(pt.Ilosc) as Punkty, k.Id, k.Id_programu, CONCAT(k.Imie, ' ', k.Nazwisko) as Nazwa, k.Email, s.Klucz, o.Nazwa as Nazwa_firma, o.Id as Id_firma FROM sl_sesje s 
            INNER JOIN sl_klienci k ON k.Id = s.Id_klienta AND k.Aktywny = 1
            LEFT JOIN sl_punkty pt ON pt.Id_klienta = k.Id AND pt.Aktywny = 1 AND pt.Zatwierdzone = 1
            INNER JOIN Oddzialy o ON o.Id = k.Firma
            WHERE s.Id = ${db.escape(req.session.clientsid)}
            group by k.Id, k.Imie, k.Nazwisko, k.Email, s.Klucz, o.Nazwa, o.Id, k.Id_programu`
            db.query(query, (err, doc) => {
                if (err) throw err
                if (doc[0] && doc[0].Klucz) {
                    res.send({ info_session: doc[0] })
                }
            })
        } else
            res.send(404)
    }
    this.validation = function(req, res, main, cb) {
        let key = JSON.parse(req.session.clientkey)
        let query = 'SELECT * FROM sl_sesje WHERE id = ' + db.escape(req.session.clientsid)

        db.query(query, (err, doc) => {
            if (err) throw err
            if (doc[0] && doc[0].Klucz) {
                let decode = functions.decodeToken(config.secret, key)
                if (doc[0].Klucz == decode) {
                    if (doc[0].Data_wylogowania != null) {
                        req.session.destroy(function(err) {
                            console.log(err)
                            res.sendStatus(404)
                            return
                        })
                    }
                    let active_query = `SELECT *, (SELECT SUM(Ilosc) From sl_punkty Where Aktywny = 1 AND Id_klienta = ${doc[0].Id_klienta}) as Punkty FROM sl_klienci WHERE Id = ${doc[0].Id_klienta} AND Aktywny = 1`
                    active_query += `;SELECT p.* FROM sl_klienci k 
                        INNER JOIN sl_programy p ON k.Id_programu = p.Id 
                        Where k.Id = ${db.escape(doc[0].Id_klienta)}`
                    active_query += `; SELECT 1`
                    db.query(active_query, (err, doc) => {
                        if (err) throw err
                        res.locals.info_session = doc[0][0]
                        res.locals.info_session.program = doc[1][0]
                        if (main && (req.params.firma != res.locals.info_session.Firma || req.params.program != res.locals.info_session.program.Id)) {
                            res.sendfile("layouts/clientLogin.html")
                            return
                        }
                        var query = 'UPDATE sl_sesje SET ? WHERE Id_klienta = ' + doc[0][0].Id + ' AND Data_wylogowania IS NULL AND Id != ' + db.escape(req.session.clientsid)
                        var query_data = {
                            Data_wylogowania: new Date()
                        }
                        db.query(query, query_data, function(err, result) {
                            if (err) throw err
                            if (cb) {
                                cb();
                                return;
                            } else {
                                res.send({ info_session: res.locals.info_session })
                            }
                        })
                    })
                } else {
                    res.sendfile("layouts/clientLogin.html")
                }
            } else {
                res.sendfile("layouts/clientLogin.html")
            }
        })

    }
}
module.exports = new clientsession()