var dbelement = require('./dbelement')
var Programy = require('./program')
var moment = require('moment')
var client = function() {
    dbelement.call(this, 'sl_klienci')
    this.CheckToken = function(data, callback) {
        let query = `SELECT haslo_token FROM sl_klienci WHERE Id = ${this.db.escape(data.Id)} 
        AND haslo_token = ${this.db.escape(data.Haslo_token)} AND Firma = ${this.db.escape(data.Firma)}`
        this.db.query(query, callback)
    }
    this.GetAllForProgram = function(firma, program, callback) {
        let query = `SELECT k.Id, k.Nazwisko, k.Imie, k.Email, k.Numer,
case when k.Haslo is null AND Length(k.Haslo_token) = 0 then 0 when k.Haslo is null then 1 else 2 end as Status,
ifnull(Sum(pkt.Ilosc), 0) as Suma_punktow,
Sum(case when pkt.Id_parent <> 0 then pkt.Ilosc else 0 end) as Suma_punktow_poleconych,
Sum(case when pkt.Id_parent = 0 then pkt.Kwota else 0 end) as Suma_zakupow,
k.Data_aktywacji
From sl_klienci k
LEFT JOIN sl_punkty pkt ON pkt.Id_programu = k.Id_programu AND pkt.Id_klienta = k.Id AND pkt.Aktywny = 1
Where k.Aktywny = 1 AND k.Id_programu = ${this.db.escape(program)} AND k.Firma = ${this.db.escape(firma)}
GROUP BY k.Id, k.Nazwisko, k.Imie, k.Email, k.Haslo, k.Haslo_token, k.Data_aktywacji
ORDER BY k.Data_aktywacji DESC, k.Numer ASC`
        this.db.query(query, callback)
    }.bind(this)
    this.Insert = function(data, callback) {
        if (data.Polecajacy > 0) {
            var query = `Select Polecajacy From sl_klienci k Where Id = ${this.db.escape(data.Polecajacy)}`
            this.db.query(query, function(err, result) {
                if (err) { return next(err) }
                var idpol = JSON.parse(result[0].Polecajacy)
                idpol.push(data.Polecajacy)
                data.Polecajacy = JSON.stringify(idpol)
                this.Create(data, callback)
            }.bind(this))
        } else {
            data.Polecajacy = JSON.stringify([])
            this.Create(data, callback)
        }
    }.bind(this)
    this.SetPassword = function(data, callback) {
        let query = `Update sl_klienci set ? Where Id = ${this.db.escape(data.Id)} AND Haslo_token = ${this.db.escape(data.Haslo_token)}`
        this.db.query(query, data, callback)
    }.bind(this)
    this.CheckNumber = function(data, callback) {
        let query = `Select * FROM sl_klienci Where Id = ${this.db.escape(data.Id)} AND Haslo_token = ${this.db.escape(data.Haslo_token)}`
        this.db.query(query, callback)
    }.bind(this)
    this.Activate = function(client, token, program, firma, callback){
        let query = `UPDATE sl_klienci SET Aktywacja_uzytkownika = NOW() WHERE 
        Id_programu = ${this.db.escape(program)} AND Firma = ${this.db.escape(firma)} 
        AND Haslo_token = ${this.db.escape(token)} AND Id = ${this.db.escape(client)};
        SELECT * FROM sl_klienci k WHERE Id_programu = ${this.db.escape(program)} AND Firma = ${this.db.escape(firma)}
        AND Id = ${this.db.escape(client)}`
        if(!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.Validate = function(client, email, program, firma, callback) {
        let query = `Select k.* From sl_klienci k
        Where k.Id <> ${this.db.escape(client)} AND k.Aktywny = 1 AND k.Id_programu = ${ this.db.escape(program)} AND k.Email=${this.db.escape(email)} AND k.Firma = ${firma};
        Select Max(k.Numer) as Numer From sl_klienci k
        Where k.Aktywny = 1 AND k.Id_programu = ${ this.db.escape(program)} AND k.Firma = ${firma};`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.GenerateCodes = function(amount, program, firma, sid, callback) {
        let query = `Select Max(k.Numer) as Numer From sl_klienci k
        Where k.Aktywny = 1 AND k.Id_programu = ${ this.db.escape(program)} AND k.Firma = ${firma}`
        this.db.query(query, function(err, result) {
            var number = 0
            if (result[0].Numer)
                number = parseInt(result[0].Numer)
            number++
            let data = []
            for(var i = 0; i < amount; i++){
                data.push([true,moment(Date.now()).format('YYYY-MM-DD'), sid, program, firma, number.toString().padStart(6, '0')])
                number++
            }
            this.db.query(`INSERT INTO sl_klienci (Aktywny, Data_utworzenia, Id_sesji, Id_programu, Firma, Numer) VALUES ?`, [data], callback)
        }.bind(this))
    }
    this.UpdateAccount = function(data, callback) {
        Programy.Get(data.Id_programu, data.Firma, function(err, result) {
            if (err) next()
            var data_punkty = data.Kwota * result.Wysokosc_premii
        })
    }.bind(this)
    this.Get = function(id, firma, callback) {
        let query = `Select k.Id, k.Aktywny, k.Data_utworzenia, k.Data_zakonczenia, k.Email,
        k.Firma, k.Id_programu, k.Nazwisko, k.Imie, k.Numer, k.Adres, k.Telefon, k.Polecajacy, CONCAT(kp.Nazwisko, ' ', kp.Imie) as PolecajacyNazwa From sl_klienci k
        LEFT JOIN sl_klienci kp ON (case when JSON_LENGTH(k.Polecajacy) > 0 THEN
        kp.Id = JSON_EXTRACT(k.Polecajacy,CONCAT("$[",JSON_LENGTH(k.Polecajacy)-1,"]"))*1 else 1=0 end)
        Where k.Id = ${id} AND k.Firma = ${firma} order by k.Nazwisko`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.GetByLogin = function(program, firma, login, callback) {
        let query = `Select *
        From sl_klienci k
        Where k.Aktywny = 1 AND k.Firma = ${this.db.escape(firma)} AND k.Id_programu = ${this.db.escape(program)} AND k.Email like ${this.db.escape(login)}`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
}
module.exports = new client()