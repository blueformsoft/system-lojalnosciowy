var dbelement = require('./dbelement')
var Program = require("./program")

var invoice = function() {
    dbelement.call(this, 'fv_Faktury')
    this.Get = function(id, firma, callback) {
        let query = `Select * From fv_Faktury Where ID = ${id} AND ID_oddzialu = ${firma}`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.Import = function(numer, firma, callback) {
        let query = `Select f.*, p.Id as Punkty
        From fv_Faktury f
LEFT JOIN sl_punkty p ON p.Id_fv = f.Id
        Where f.Typ_faktury in ('FS', 'KFS', 'RS', 'KRS') AND f.Nr_faktury like ${this.db.escape(numer)} AND f.ID_oddzialu = ${this.db.escape(firma)}`
        if (!callback) return query
        this.db.query(query, callback)
    }
}
module.exports = new invoice()