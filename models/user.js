var db = require('../db')
var bcrypt = require('bcryptjs')
var cookieparser = require('cookie-parser')
var functions = require('../services/functions')
var config = require('../config')
var moment = require('moment')
var user = function() {
    this.GetAll = function(callback) {
        db.query("Select * From Operatorzy", callback)
    }
    this.Login = function(req, res, data) {
        let query = 'SELECT od.Status, o.moduly, o.haslo, o.id, o.imie, o.nazwisko, o.rodzaj_konta, o.firma, o.data_waznosci, o.email, o.nowosci_data FROM Operatorzy as o'
        query += ' LEFT JOIN Oddzialy AS od ON od.ID = o.firma WHERE ? AND o.Aktywny = 1'
        let query_data = { 'o.login': data.login }
        db.query(query, query_data, (err, doc) => {
            if (err) throw err
            if (doc[0]) {
                if ((doc[0].moduly == null || doc[0].moduly.indexOf('sl') == -1) && doc[0].rodzaj_konta < 3) {
                    let error_data = {}
                    error_data.flash = { type: 'alert-info', messages: [{ location: 'body', msg: 'Brak dostępu do modułu' }] }
                    res.send(error_data)
                    return ''
                }
                if (bcrypt.compareSync(data.haslo, doc[0].haslo)) {
                    let token = functions.ftGenerateToken(16)
                    let key = functions.codeToken(config.secret, token)
                    key = JSON.stringify(key)
                        //wpisanie licencji jeżeli brak, sprawdzanie licencji jeżeli moduły są wpisane

                    let session = 'INSERT INTO Sesje SET ? '
                    let session_data = {
                        IDUzytkownik: doc[0].id,
                        Data_logowania: moment().format('YYYY-MM-DD HH:mm:ss'),
                        Klucz: token,
                        Adres: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
                        Modul: 'sl',
                        Oddzial: doc[0].firma
                    }
                    db.query(session, session_data, (err, result) => {
                        if (err) throw err
                        let data = {
                            userId: doc[0].id,
                            imie: doc[0].imie,
                            nazwisko: doc[0].nazwisko,
                            firma: doc[0].firma,
                            rodzaj: doc[0].rodzaj_konta,
                            email: doc[0].email,
                            parentID: doc[0].firma,
                            nowosci_data: doc[0].nowosci_data
                        }
                        if (doc[0].Status == 2)
                            data.ksiegowy = true
                        this.RemoveCookie(req, res)
                        res.cookie('sid', result.insertId)
                        res.cookie('key', key)
                        res.cookie('sid', result.insertId, { domain: '.bs365.pl' })
                        res.cookie('key', key, { domain: '.bs365.pl' })
                        res.cookie('sid', result.insertId, { domain: 'localhost' })
                        res.cookie('key', key, { domain: 'localhost' })
                            // if (typeof doc[0].firma != 'undefined' && doc[0].firma == null && doc[0].rodzaj_konta == 3) {
                            //   res.redirect('/register2')
                            //   // res.render('register2', {after_login: true})
                            // }
                            // else {
                        let query = 'SELECT * FROM Oddzialy WHERE ?'
                        query = 'SELECT * FROM OddzialOperator op JOIN Oddzialy as o ON o.ID = op.Oddzial WHERE op.Operator = ' + doc[0].id
                        if ((doc[0].moduly == null || doc[0].moduly.indexOf('fv') == -1) && doc[0].rodzaj_konta >= 3) {
                            query = 'INSERT INTO Licencje SET ? '
                            query_data = {
                                firma: doc[0].firma,
                                modul: 'fv',
                                data_waznosci: moment().add(180, 'd').format('YYYY-MM-DD'),
                                limit_uzytkownikow: 0,
                                data_wpisu: new Date()
                            }
                            var moduly = []
                            if (doc[0].moduly != null) {
                                moduly = JSON.parse(doc[0].moduly)
                            }
                            moduly.push('sl')
                            query += '; UPDATE Operatorzy SET Moduly = \'' + JSON.stringify(moduly) + '\' WHERE ID = ' + doc[0].id
                            db.query(query, query_data, function(err, doc) {
                                if (err) throw err
                                res.send(200)

                                //jeżeli folder firmowy nie istnieje - utworzyć

                            })
                        } else {
                            res.send(200)
                        }


                    })
                } else {
                    let data = {}
                    data.flash = { type: 'alert-info', messages: [{ location: 'body', msg: 'Nieprawidłowe dane logowania' }] }
                    res.send(data)
                }
            } else {
                let data = {}
                data.flash = { type: 'alert-info', messages: [{ location: 'body', msg: 'Nieprawidłowe dane logowania' }] }
                res.send(data)
            }
        })
    }.bind(this)

    this.Logout = function(req, res) {
        let query = 'UPDATE Sesje SET ? WHERE Id = '
        query += db.escape(req.session.sid)
        let session_data = {
            Data_wylogowania: moment().format('YYYY-MM-DD HH:mm:ss')
        }
        db.query(query, session_data, (err, doc) => {
            this.RemoveCookie(req, res)
            if (err) throw err
            res.send(200)
        })
    }.bind(this)

    this.RemoveCookie = function(req, res) {
        req.session.destroy(function(err) {
            console.log(err)
        })
    }
}

module.exports = new user()