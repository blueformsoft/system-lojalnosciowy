var db = require('../db')
var settings = function() {
    this.Insert = function(firma, sid, callback) {
        var query = 'INSERT INTO ustawienia (Nazwa, Wartosc, Modul, Firma, Id_sesji) VALUES ?'
        let data = [
            ['RodzajRozliczania', 'procent', 'sl', firma, sid],
            ['WysokoscPremii', '10', 'sl', firma, sid],
            ['kurs', '1,30', 'sl', firma, sid],
            ['MaxPremia', '0', 'sl', firma, sid],
            ['MinPremia', '0', 'sl', firma, sid],
            ['LiczbaPoziomow', '0', 'sl', firma, sid]
        ]

        db.query(query, [data], callback)
    }
    this.Update = function(settings, firma, callback) {
        var query = ''
        for (var val in settings)
            query += `Update ustawienia SET Wartosc = ${settings[val]} WHERE Nazwa = ${val} AND Firma = ${firma}`
        db.query(query, callback)
    }
    this.GetAll = function(firma, callback) {
        var query = `Select * From ustawienia Where Firma = ${firma} AND Modul = 'sl'`
        db.query(query, function(err, data) {
            if (err) { return next(err) }
            if (data.length > 0) {
                var dbSettings = {
                    RodzajRozliczania: data[0].Wartosc,
                    WysokoscPremii: parseFloat(data[1].Wartosc.replace(',', '.')),
                    Kurs: parseFloat(data[2].Wartosc.replace(',', '.')),
                    MaxPremia: parseFloat(data[3].Wartosc.replace(',', '.')),
                    MinPremia: parseFloat(data[4].Wartosc.replace(',', '.')),
                    LiczbaPoziomow: parseFloat(data[5].Wartosc.replace(',', '.'))
                }
                callback(err, dbSettings)
            } else
                callback(err, null)
        })
    }
}
module.exports = new settings()