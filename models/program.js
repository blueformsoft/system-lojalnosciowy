var dbelement = require('./dbelement')
var program = function() {
    dbelement.call(this, 'sl_programy')
    this.GetProgramClientData = function(client, firma, programid, callback) {
        let query = `SET SESSION group_concat_max_len = 1000000; Select k.Data_utworzenia, k.Id, k.Nazwisko, k.Imie, k.Numer, 
        k.Data_utworzenia, k.Data_zakonczenia, k.Data_aktywacji, k.Aktywacja_uzytkownika, k.Adres, k.Telefon, k.Email, k.Polecajacy,
        SUM(case when pt.Zatwierdzone = 0 AND pt.Ilosc < 0 then pt.Ilosc else 0 end) as SumaPunktowUjemnych,
        SUM(case when pt.Zatwierdzone = 1  AND pt.Ilosc < 0 then pt.Ilosc else 0 end) as SumaPunktowUjemnychZatwierdzonych,
        SUM(case when pt.Zatwierdzone = 0 AND pt.Ilosc >= 0 then pt.Ilosc else 0 end) as SumaPunktowDodatnich,
        SUM(case when pt.Zatwierdzone = 1  AND pt.Ilosc >= 0 then pt.Ilosc else 0 end) as SumaPunktowDodatnichZatwierdzonych,
        SUM(case when pt.Id_parent = 0 AND pt.Zatwierdzone = 0 AND pt.Ilosc > 0 then pt.Ilosc else 0 end) as Punkty, 
        SUM(case when pt.Id_parent = 0 AND pt.Zatwierdzone = 1 AND pt.Ilosc > 0 then pt.Ilosc else 0 end) as ZatwierdzonePunkty, 
        SUM(case when pt.Id_parent <> 0 AND pt.Zatwierdzone = 0 then pt.Ilosc else 0 end) as PunktyPoleconych,
        SUM(case when pt.Id_parent <> 0 AND pt.Zatwierdzone = 1 then pt.Ilosc else 0 end) as ZatwierdzonePunktyPoleconych,
        CONCAT('[', GROUP_CONCAT( CONCAT('{"Opis":"',pt.Opis, '", "Ilosc":"',pt.Ilosc,'", "Data":"', DATE_FORMAT(pt.Data, '%Y-%m-%d'),'", "Zatwierdzone":"', pt.Zatwierdzone,'"}')) ,']') as WszystkieDokumenty,
        CONCAT('[', GROUP_CONCAT( Case when pt.Id_parent = 0 then CONCAT('{"Opis":"',pt.Opis, '", "Ilosc":"',pt.Ilosc,'", "Data":"', DATE_FORMAT(pt.Data, '%Y-%m-%d'),'", "Zatwierdzone":"', pt.Zatwierdzone,'"}') else null end) ,']') as Dokumenty
        FROM sl_klienci k
        LEFT JOIN sl_programy p ON p.Id = k.Id_programu
        LEFT JOIN sl_punkty pt ON pt.Id_klienta = ${client} AND pt.Aktywny = 1
        Where k.Aktywny = 1 AND k.Id = ${client} AND k.Firma = ${firma}
        group by k.Id, k.Nazwisko, k.Imie, k.Data_utworzenia, k.Data_zakonczenia, k.Data_aktywacji, k.Aktywacja_uzytkownika, k.Adres, k.Telefon, k.Email, k.Polecajacy;`
        
        query += `SELECT k.Nazwisko, k.Imie, 
        SUM(Case when pt.Zatwierdzone = 0 then pt.Ilosc else 0 end) as PunktyPoleconego,
        SUM(Case when pt.Zatwierdzone = 1 then pt.Ilosc else 0 end) as ZatwierdzonePunktyPoleconego,
        CONCAT('[', GROUP_CONCAT( Case when ptk.Id_parent = 0 then CONCAT('{"Opis":"',pt.Opis, '", "Ilosc":"',pt.Ilosc,'", "Data":"', DATE_FORMAT(pt.Data, '%Y-%m-%d'),'", "Zatwierdzone":"', pt.Zatwierdzone,'"}') else null end) ,']') as DokumentyPoleconego,
        CONCAT('[', GROUP_CONCAT( Case when ptk.Id_parent > 0 then CONCAT('{"Opis":"',pt.Opis, '", "Ilosc":"',pt.Ilosc,'", "Data":"', DATE_FORMAT(pt.Data, '%Y-%m-%d'),'", "Zatwierdzone":"', pt.Zatwierdzone,'"}') else null end) ,']') as DokumentyPoleconychPoleconego
        FROM sl_klienci k
        LEFT JOIN sl_punkty ptk ON ptk.Id_klienta = k.Id
        LEFT JOIN sl_punkty pt ON pt.Id_klienta = ${client} AND (pt.Id_parent = ptk.Id OR (pt.Id_parent > 0 AND pt.Id_parent = ptk.Id_parent)) 
        WHERE k.Firma = ${firma} AND JSON_LENGTH(k.Polecajacy) > 0 AND
        JSON_CONTAINS(k.Polecajacy, '"${client}"', Concat("$[",(JSON_LENGTH(k.Polecajacy)-1),"]")) = 1
        group by k.Nazwisko, k.Imie;
        Select * From sl_programy p Where p.Id = ${programid};`
        
        query += `Select m.miesiac, IFNULL(SUM(pt.Ilosc), 0) as punkty
        From (Select Date_add(Now(),interval - 11 month) as miesiac
        UNION ALL
        Select Date_add(Now(),interval - 10 month)
        UNION ALL
        Select Date_add(Now(),interval - 9 month)
        UNION ALL
        Select Date_add(Now(),interval - 8 month)
        UNION ALL
        Select Date_add(Now(),interval - 7 month)
        UNION ALL
        Select Date_add(Now(),interval - 6 month)
        UNION ALL
        Select Date_add(Now(),interval - 5 month)
        UNION ALL
        Select Date_add(Now(),interval - 4 month)
        UNION ALL
        Select Date_add(Now(),interval - 3 month)
        UNION ALL
        Select Date_add(Now(),interval - 2 month)
        UNION ALL
        Select Date_add(Now(),interval - 1 month)
        UNION ALL
        Select Now()) as m
        LEFT JOIN sl_klienci k ON k.Id = ${client}
        LEFT JOIN sl_programy p ON p.Id = k.Id_programu
        LEFT JOIN sl_punkty pt ON pt.Id_klienta = ${client} AND pt.Aktywny = 1 AND DATE_FORMAT(pt.Data, "%m-%Y") = DATE_FORMAT(m.miesiac, "%m-%Y")
        Where k.Aktywny = 1 AND k.Id = ${client} AND k.Firma = ${firma}
        group by m.miesiac;`
        if (!callback) return query
        this.db.query(query, function(err, result) {
            if (err) { throw (err) }
            let program = result[3][0]
            var polecajacy = []
            result[1][0].Statystyki = result[4]
            if (result[1][0])
                polecajacy = JSON.parse(result[1][0].Polecajacy)
            if (!polecajacy || polecajacy.length == 0)
                callback([result[1], result[2], result[3]], [], program)
            else {
                let query = `SELECT 1`

                for (var i = 0; i < (program.MLM ? polecajacy.length : program.Liczba_poziomow); i++) {
                    query += `;Select k.Id, k.Nazwisko, k.Imie, k.Data_utworzenia, k.Data_zakonczenia, k.Polecajacy,
                    SUM(case when pt.Zatwierdzone = 0 then pt.Ilosc else 0 end) as SumaPunktow,
                    SUM(case when pt.Zatwierdzone = 1 then pt.Ilosc else 0 end) as SumaPunktowZatwierdzonych,
                    SUM(case when pt.Id_parent = 0 AND pt.Zatwierdzone = 0 then pt.Ilosc else 0 end) as Punkty, 
                    SUM(case when pt.Id_parent = 0 AND pt.Zatwierdzone = 1 then pt.Ilosc else 0 end) as ZatwierdzonePunkty, 
                    SUM(case when pt.Id_parent <> 0 AND pt.Zatwierdzone = 0 then pt.Ilosc else 0 end) as PunktyPoleconych,
                    SUM(case when pt.Id_parent <> 0 AND pt.Zatwierdzone = 1 then pt.Ilosc else 0 end) as ZatwierdzonePunktyPoleconych,
                    CONCAT('[', GROUP_CONCAT( Case when pt.Id_parent = 0 then CONCAT('{"Opis":"',pt.Opis, '", "Ilosc":"',pt.Ilosc,'", "Data":"', DATE_FORMAT(pt.Data, '%Y-%m-%d'),'", "Zatwierdzone":"', pt.Zatwierdzone,'"}') else null end) ,']') as Dokumenty
                    From sl_klienci k
                    INNER JOIN sl_programy p ON p.Id = k.Id_programu
                    LEFT JOIN sl_punkty pt ON pt.Id_klienta = ${polecajacy[i]} AND pt.Aktywny = 1
                    Where k.Aktywny = 1 AND k.Id = ${polecajacy[i]} AND k.Firma = ${firma}
                    group by k.Id, k.Nazwisko, k.Imie, k.Data_utworzenia, k.Data_zakonczenia, k.Polecajacy`
                }
                this.db.query(query, function(err, data) {
                    if (err) { return next(err) }
                    data.splice(0, 1)
                    callback([result[1], result[2], result[3]], data, program)
                })
            }
        }.bind(this))
    }.bind(this)
    this.GetParentList = function(program, firma, callback) {
        let query = `
                                    Select k.Nazwisko, k.Imie, k.Email, k.Id From sl_klienci k
                                    WHERE k.Firma = ${ firma }
                                    AND k.Id_programu = ${ program }
                                    AND k.Aktywny = 1 `
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.GetAll = function(firma, callback) {
        let query = `
                                    Select p.Id, p.Logo, p.Tlo, p.Nazwa, p.Opis, p.Data_rozpoczecia, p.Data_zakonczenia,
                                        Count(distinct k.Id) as Ilosc_klientow,
                                        Count(distinct
                                            case when pkt.Id_parent = 0 then pkt.Id
                                            else null end) as Ilosc_zakupow,
                                        IFNULL(Sum(
                                            case when pkt.Id_parent = 0 AND pkt.Ilosc > 0 then pkt.Kwota
                                            else null end), 0) as Kwota_zakupow,
                                            IFNULL(Sum(case when pkt.Ilosc > 0 then pkt.Ilosc else 0 end), 0) as Suma_punktow,
                                            l.Data_waznosci
                                    From sl_programy p
                                    LEFT JOIN sl_klienci k ON k.Id_programu = p.Id AND k.Aktywny = 1
                                    LEFT JOIN sl_punkty pkt ON pkt.Id_klienta = k.Id AND pkt.Id_programu = p.Id AND pkt.Aktywny AND pkt.Ilosc > 0
                                    LEFT JOIN Licencje l ON l.Aktywny = 1 AND l.Firma = ${ firma } AND l.Modul = 'sl'
                                    Where p.Aktywny = 1 AND p.Firma = ${ firma }
                                    Group by p.Id, p.Nazwa, p.Opis, p.Data_rozpoczecia, p.Data_zakonczenia
                                    order by p.Nazwa;
                                    SELECT m.miesiac, pr.Id as program, IFNULL(SUM(p.Ilosc), 0) as punkty FROM (Select Date_add(Now(),interval - 11 month) as miesiac
UNION ALL
Select Date_add(Now(),interval - 10 month)
UNION ALL
Select Date_add(Now(),interval - 9 month)
UNION ALL
Select Date_add(Now(),interval - 8 month)
UNION ALL
Select Date_add(Now(),interval - 7 month)
UNION ALL
Select Date_add(Now(),interval - 6 month)
UNION ALL
Select Date_add(Now(),interval - 5 month)
UNION ALL
Select Date_add(Now(),interval - 4 month)
UNION ALL
Select Date_add(Now(),interval - 3 month)
UNION ALL
Select Date_add(Now(),interval - 2 month)
UNION ALL
Select Date_add(Now(),interval - 1 month)
UNION ALL
Select Now()) as m
LEFT JOIN sl_programy pr ON pr.Firma = ${ firma }
LEFT JOIN sl_punkty p ON p.Ilosc > 0 AND p.Firma = ${ firma } AND p.Id_programu = pr.Id AND DATE_FORMAT(p.Data, "%m-%Y") = DATE_FORMAT(m.miesiac, "%m-%Y")
GROUP BY m.miesiac, pr.Id
ORDER BY m.miesiac`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.GetByClientId = function(client, firma, callback) {
        let query = `
                                    Select p.* From sl_klienci k
                                    INNER JOIN sl_programy p ON k.Id_programu = p.Id
                                    Where k.Id = ${ client }
                                    AND k.Firma = ${ firma }
                                    order by Nazwa `
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.GetClientAmount = function(program, firma, callback) {
        let query = `Select Count(*) as Ilosc From sl_klienci WHERE Aktywny = 1 AND Id_programu = ${program} AND Firma = ${firma}`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
}
module.exports = new program()