var dbelement = require('./dbelement')
var moment = require('moment')

var order = function() {
    dbelement.call(this, 'sl_zamowienia')
    this.Validate = function(data, callback) {
        let query = `Select Sum(p.Ilosc) as Punkty From sl_punkty p 
        Where p.Zatwierdzone = 1 AND p.Aktywny = 1 AND p.Id_klienta = ${data.Id_klienta} AND p.Firma = ${data.Firma};
        Select Count(*) as Ilosc From sl_magazyn m Where m.Id_nagrody = ${data.Id_nagrody} AND m.Wydane = 0 AND m.Aktywny = 1 AND m.Firma = ${data.Firma};
        Select Cena, Magazyn From sl_nagrody n Where n.Firma = ${data.Firma} AND n.Id = ${data.Id_nagrody}`
        this.db.query(query, function(err, result) {
            if (err) throw (err)
            if (result[0][0].Punkty < result[2][0].Cena)
                callback(["Nie masz wystarczającej ilości punktów, aby zamówić ten produkt"])
            else if (result[2][0].Magazyn && result[1][0].Ilosc == 0)
                callback(["Brak produktów w magazynie"])
            else
                callback()
        })
    }.bind(this)
    this.GetAllByClient = function(klient, firma, callback) {
        let query = `Select z.*, n.Cena as Wartosc, n.Nazwa as Nagroda From sl_zamowienia z
        INNER JOIN sl_nagrody n ON n.Id = z.Id_nagrody 
        Where z.Aktywny = 1 AND z.Id_klienta = ${klient} AND z.Firma = ${firma} order by n.Nazwa`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.GetInfo = function(order, callback) {
        let query = `SELECT k.Email, o.Nazwa as Firma, z.Numer_zamowienia, z.Cena, z.Nagroda, z.Wiadomosc as Wiadomosc_klienta, z.Adres, 
        p.Nazwa as Program, n.Opis as Nagroda_opis, n.Opis_skrot as Nagroda_opis_skrot, p.Mail_zamowienie, p.Mail_realizacja, p.Mail_anulowanie
        FROM sl_zamowienia z
        INNER JOIN sl_programy p ON p.Id = z.Id_programu
        INNER JOIN sl_klienci k ON k.Id = z.Id_klienta
        INNER JOIN Oddzialy o ON o.ID = z.Firma
        INNER JOIN sl_nagrody n ON n.Id = z.Id_nagrody
        Where z.Id = ${order}`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.GetProduct = function(order, callback) {
        let query = `SELECT k.Email, k.Id as Id_klienta, o.Nazwa as Firma, o.Id as Id_firmy, z.Id, z.Numer_zamowienia, z.Cena, z.Nagroda, z.Wiadomosc, z.Adres, 
        p.Nazwa as Program, n.Automat, n.Mail, m.Opis, m.Zalacznik, m.Plik, m.Id as Id_produkt 
        FROM sl_zamowienia z
        INNER JOIN sl_programy p ON p.Id = z.Id_programu
        INNER JOIN sl_klienci k ON k.Id = z.Id_klienta
        INNER JOIN Oddzialy o ON o.ID = z.Firma
        INNER JOIN sl_nagrody n ON n.Id = z.Id_nagrody
        LEFT JOIN sl_magazyn m ON m.Aktywny = 1 AND m.Wydane = 0 AND m.Id_nagrody = n.Id 
        Where z.Id = ${order}`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.GetAll = function(program, firma, callback) {
        let query = `Select CONCAT(k.Nazwisko, ' ', k.Imie) as Klient, k.Email, z.*, n.Cena as Wartosc, n.Nazwa as Nagroda From sl_zamowienia z
        INNER JOIN sl_nagrody n ON n.Id = z.Id_nagrody 
        INNER JOIN sl_klienci k ON k.Id = z.Id_klienta
        Where z.Aktywny = 1 AND z.Id_programu = ${program} AND z.Firma = ${firma} order by n.Nazwa`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.Remove = function(data, callback) {
        this.db.beginTransaction(function(err) {
            if (err) {
                this.db.rollback(function() {
                    throw err;
                })
            }
            let query = `Update sl_zamowienia Set Aktywny = 0 Where Id = ${data.Id} AND Firma = ${data.Firma};
        Update sl_punkty Set Aktywny = 0 Where Id_zamowienia = ${data.Id} AND Firma = ${data.Firma}`
            this.db.query(query, function(err, data) {
                if (err) {
                    this.db.rollback(function() {
                        throw err;
                    })
                }
                this.db.commit(function(err) {
                    if (err) {
                        this.db.rollback(function() {
                            throw err
                        })
                    }
                    callback(err, data)
                }.bind(this))
            }.bind(this))
        }.bind(this))

    }.bind(this)
    this.Create = function(data, callback) {
        query = `SELECT Max(Numer) as Numer From sl_zamowienia Where Firma = ${data.Firma} AND Aktywny = 1`

        this.db.beginTransaction(function(err) {
            if (err) {
                this.db.rollback(function() {
                    throw err;
                })
            }
            this.db.query(query, function(err, result) {
                if (err) {
                    this.db.rollback(function() {
                        throw err;
                    })
                }
                data.Numer = 1
                if (result && result.length > 0)
                    data.Numer = result[0].Numer + 1
                    /* Nadanie numeru zamówienia */
                data.Numer_zamowienia = moment().format(`${data.Numer}/M/YYYY`)
                let query = `INSERT INTO sl_zamowienia SET ?`
                this.db.query(query, data, function(err, result) {
                    if (err) {
                        this.db.rollback(function() {
                            throw err;
                        })
                    }
                    query = `INSERT INTO sl_punkty SET ?`
                    let point = {
                        Aktywny: 1,
                        Data: data.Data_zamowienia,
                        Firma: data.Firma,
                        Id_klienta: data.Id_klienta,
                        Id_programu: data.Id_programu,
                        Id_sesji: data.Id_sesji,
                        Ilosc: -data.Cena,
                        Kwota: data.Cena,
                        Opis: `${data.Numer_zamowienia} - ${data.Nagroda}`,
                        Id_parent: 0,
                        Zatwierdzone: 1,
                        Id_zamowienia: result.insertId
                    }
                    this.db.query(query, point, function(err, data) {
                        if (err) {
                            this.db.rollback(function() {
                                throw err;
                            })
                        }
                        this.db.commit(function(err) {
                            if (err) {
                                this.db.rollback(function() {
                                    throw err
                                })
                            }
                            callback(err, result)
                        }.bind(this))
                    }.bind(this))
                }.bind(this))
            }.bind(this))
        }.bind(this))
    }.bind(this)
}
module.exports = new order()