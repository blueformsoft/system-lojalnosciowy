var dbelement = require('./dbelement')

var repository = function() {
    dbelement.call(this, 'sl_magazyn')
    this.GetAllByPrizeId = function(nagroda, firma, callback) {
        let query = `Select s.*, CONCAT(k.Nazwisko, ' ', k.Imie) as Klient From sl_magazyn s
        LEFT JOIN sl_klienci k ON k.Id = s.Id_klienta 
        Where s.Id_nagrody = ${nagroda} AND s.Aktywny = 1 AND s.Firma = ${firma}`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
}
module.exports = new repository()