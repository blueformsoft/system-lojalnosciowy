var db = require('../db')
var bcrypt = require('bcryptjs')
var cookieparser = require('cookie-parser')
var functions = require('../services/functions')
var config = require('../config')
var moment = require('moment')
var passwordValidator = require('password-validator')
var passwordSchema = new passwordValidator()
passwordSchema.is().min(8)
    .is().max(20)
    .has().uppercase()
    .has().lowercase()
    .has().digits()
    .has().not().spaces();

var passwordValidationFail = function(pass) {
    let passValidationFail = []
    let passFail = passwordSchema.validate(pass, { list: true })
    let passFailSet = new Set(passFail)
    if (passFailSet.has('min') || passFailSet.has('max')) passValidationFail.push("musi zawierać od 8 do 20 znaków")
    if (passFailSet.has('uppercase')) passValidationFail.push("musi zawierać przynajmniej jedną dużą literę")
    if (passFailSet.has('lowercase')) passValidationFail.push("musi zawierać przynajmniej jedną małą literę")
    if (passFailSet.has('digits')) passValidationFail.push("musi zawierać przynajmniej jedną cyfrę")
    if (passFailSet.has('spaces')) passValidationFail.push("nie może zawierać spacji")
    return passValidationFail
}

var userclient = function() {
    this.Login = function(req, res, data) {
        let query = `SELECT * FROM sl_klienci WHERE Email=${db.escape(data.login)} AND Firma=${db.escape(data.firma)} AND Id_programu=${db.escape(data.program)} AND Aktywny = 1`
        db.query(query, (err, doc) => {
            if (err) throw err
            if (doc.length > 0 && doc[0]) {
                if (doc[0].Haslo && bcrypt.compareSync(data.haslo, doc[0].Haslo)) {
                    if (!doc[0].Aktywacja_uzytkownika) {
                        data.toActivate = {
                            email: data.login,
                            token: doc[0].Haslo_token,
                            id: doc[0].Id,
                            Firma: data.firma,
                            Program: data.program
                        }
                        data.error = 'Konto nie zostało jeszcze aktywowane.'
                        return res.send(data)
                    }
                    let token = functions.ftGenerateToken(16)
                    let key = functions.codeToken(config.secret, token)
                    key = JSON.stringify(key)

                    let session = 'INSERT INTO sl_sesje SET ? '
                    let session_data = {
                        Id_klienta: doc[0].Id,
                        Data_logowania: moment().format('YYYY-MM-DD HH:mm:ss'),
                        Klucz: token,
                        Adres: req.headers['x-forwarded-for'] || req.connection.remoteAddress
                    }
                    db.query(session, session_data, (err, result) => {
                        if (err) throw err
                        req.session.clientsid = result.insertId
                        req.session.clientkey = key 
                        res.sendStatus(200)
                    })
                } else {
                    let data = {}
                    data.error = 'Nieprawidłowe dane logowania'
                    res.send(data)
                }
            } else {
                let data = {}
                data.error = 'Nieprawidłowe dane logowania'
                res.send(data)
            }
        })
    }
    this.ChangePassword = function(data, callback) {
        let query = `SELECT * FROM sl_klienci WHERE Id=${db.escape(data.Id)} AND Firma=${db.escape(data.Firma)} AND Aktywny = 1`
        db.query(query, (err, doc) => {
            if (err) throw err
            if (doc[0]) {
                if (bcrypt.compareSync(data.Haslo_old, doc[0].Haslo)) {
                    bcrypt.hash(data.Haslo, 10, function(err, hash) {
                        if (err) console.log(err)
                        let klient = {
                            Id: data.Id,
                            Haslo: hash
                        }
                        query = `Update sl_klienci Set ? Where Id = ${data.Id}`
                        db.query(query, klient, callback)
                    })
                } else
                    callback('Hasło nie jest poprawne.')
            }
        })
    }
    this.Register = function(data, token, callback) {
        this.ValidateUser(data.Email, data.Numer, data.iscard == 1, data.Firma, data.Id_programu, function(err, id) {
            if (err) return callback(err, null)
            if (!data.haslo && data.haslo == data.haslo2)
                return callback("Hasła się nie zgadzają.")
            let passFail = passwordValidationFail(data.haslo)
            if (passFail.length == 0) {
                bcrypt.hash(data.haslo, 10, (err, hash) => {
                    data.Haslo = hash
                    data.Haslo_token = token
                    data.Aktywny = 1
                    data.Data_utworzenia = moment().format('YYYY-MM-DD HH:mm:ss')
                    delete data.iscard
                    delete data.haslo
                    delete data.haslo2
                    if (id) {
                        data.Id = id
                        this.CreateUser(data, function(err, data) {
                            callback(null, data)
                        })
                    } else {
                        this.GetCardNumber(data.Firma, data.Id_programu, function(result) {
                            if (result.Id)
                                data.Id = result.Id
                            else {
                                data.Numer = result.Numer
                                data.Data_aktywacji = moment().format("YYYY-MM-DD HH:mm:ss")
                                data.Polecajacy = '[]'
                                data.Id_sesji = 0
                            }
                            data.Data_aktywacji = moment().format("YYYY-MM-DD HH:mm:ss")
                            this.CreateUser(data, function(err, data) {
                                callback(null, data)
                            })
                        }.bind(this))
                    }

                })
            } else {
                callback("Hasło nie spełnia następujących warunków: " + passFail.join(', ') + '.')
            }
            // delete data.
            // this.CreateUser()
            // callback()
        }.bind(this))
    }.bind(this)
    this.GetCardNumber = function(firma, program, callback) {
        let query = `SELECT * FROM sl_klienci 
        WHERE Aktywny = 1 AND Id_programu = ${db.escape(program)} AND Firma = ${db.escape(firma)} AND Data_aktywacji is null
        LIMIT 0,1;
        Select Max(k.Numer) as Numer From sl_klienci k
        Where k.Aktywny = 1 AND k.Id_programu = ${ db.escape(program)} AND k.Firma = ${db.escape(firma)}`
        db.query(query, function(err, result) {
            if (result[0].length > 0 && result[0][0].Id) {
                callback({ Id: result[0][0].Id })
            } else {
                var number = 0
                if (result[0].Numer)
                    number = parseInt(result[0].Numer)
                number++
                callback({ Numer: number.pad(6) })
            }
        })
    }
    this.CreateUser = function(data, callback) {
        let query = `INSERT INTO sl_klienci SET ?`
        if (data.Id) {
            query = `UPDATE sl_klienci SET ? WHERE Id = ${data.Id}`
        }
        db.query(query, data, function(err, doc) {
            if (err) return callback(err, null)
            let id = data.Id
            if (doc.insertId) id = doc.insertId
            callback(null, id)
        })
    }
    this.ValidateUser = function(email, number, cardExist, firma, program, callback) {
        let query = `SELECT * FROM sl_klienci s 
        WHERE s.Aktywny = 1 AND s.Firma = ${db.escape(firma)} AND s.Id_programu = ${db.escape(program)} 
        AND s.Email = ${db.escape(email)}`
        if (cardExist) {
            query = `SELECT * FROM sl_klienci s 
            WHERE s.Aktywny = 1 AND s.Firma = ${db.escape(firma)} AND s.Id_programu = ${db.escape(program)} 
            AND (s.Email = ${db.escape(email)} OR s.Numer = ${db.escape(number)})`
        }
        db.query(query, (err, doc) => {
            if (!cardExist && doc.length > 0) {
                return callback("Istnieje już konto powiązane z tym adresem email.", 0)
            }
            if (cardExist && doc.length > 0) {
                if (!doc[0].Data_aktywacji)
                    return callback("Karta o podanym numerze jest nie została jeszcze aktywowana. Skontaktuj się z dostawcą karty, aby ją aktywować.", null)
                else if (doc[0].Data_aktywacji && doc[0].Email)
                    return callback("Karta należy do innego użytkownika.", null)
                else
                    return callback(null, doc[0].Id)
            }
            if (cardExist) {
                return callback("Numer karty klienta nie jest poprawny.", null)
            }
            return callback(null, null)
        })
    }
    this.RemoveCookie = function(req, res) {
        req.session.destroy(function(err) {
            console.log(err)
        })
    }
    this.Logout = function(req, res) {
        let query = 'UPDATE sl_sesje SET ? WHERE Id = '
        query += db.escape(req.session.sid)
        let session_data = {
            Data_wylogowania: moment().format('YYYY-MM-DD HH:mm:ss')
        }
        db.query(query, session_data, (err, doc) => {
            this.RemoveCookie(req, res)
            if (err) throw err
            res.send(200)
        })
    }
    Number.prototype.pad = function(n) {
        return new Array(n).join('0').slice((n || 2) * -1) + this;
    }
}

module.exports = new userclient()