var dbelement = function(table) {
    this.db = require('../db')
    this.ExecuteManyQueries = function(queries, callback) {
        this.db.query(queries.join(';'), callback)
    }
    this.GetAll = function(firma, callback) {
        let query = `Select * From ${table} Where Aktywny = 1 AND Firma = ${firma} order by Nazwa`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.Get = function(id, firma, callback) {
        let query = `Select * From ${table} Where Id = ${id} AND Firma = ${firma} order by Nazwa`
        if (!callback) return query
        this.db.query(query, callback)
    }.bind(this)
    this.Create = function(data, callback) {
        let query = `INSERT INTO ${table} SET ?`
        if (!callback) return query
        this.db.query(query, data, callback)
    }.bind(this)
    this.Upsert = function(data, callback) {
        let query = `INSERT IGNORE INTO ${table} SET ?`
        if (!callback) return query
        this.db.query(query, data, callback)
    }.bind(this)
    this.Update = function(data, callback) {
        let query = `UPDATE ${table} SET ? Where ID = ${data.Id} AND Firma = ${data.Firma}`
        if (!callback) return query
        this.db.query(query, data, callback)
    }.bind(this)
}
module.exports = dbelement