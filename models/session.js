var db = require('../db')
var config = require('../config')
var functions = require('../services/functions')
var moment = require('moment')
var session = function() {
    this.GetData = function(req, res) {
        if (req.session.sid) {
            let query = 'SELECT o.*, s.Klucz FROM Sesje s INNER JOIN Operatorzy o ON o.ID = s.IdUzytkownik AND o.Aktywny = 1 WHERE s.Id = ' + db.escape(req.session.sid)
            if (req.session.program)
                query += `; SELECT * FROM sl_programy p WHERE p.Id = ${req.session.program}`
            else
                query += '; SELECT 1'
            query += `; SELECT l.Data_waznosci, l.Modul
                FROM Sesje s
                INNER JOIN Licencje l ON l.Firma = s.Oddzial
                WHERE l.Modul <> 'sl' AND s.Id = ${db.escape(req.session.sid)} AND l.Aktywny = 1`
            db.query(query, (err, doc) => {
                if (err) throw err
                if (doc[0][0] && doc[0][0].Klucz) {
                    res.send({ info_session: doc[0][0], program: doc[1][0], licencje: doc[2] })
                }
            })

        } else
            res.send(404)

    }
    this.validation = function(req, res, cb) {
        let key = JSON.parse(req.session.key)

        let query = 'SELECT * FROM Sesje WHERE id = ' + db.escape(req.session.sid)

        db.query(query, (err, doc) => {
            if (err) throw err
            if (doc[0] && doc[0].Klucz) {
                let decode = functions.decodeToken(config.secret, key)
                if (doc[0].Klucz == decode) {
                    if (doc[0].Data_wylogowania != null) {
                        res.sendfile("layouts/logoutTemplate.html")
                        return
                    }
                    let active_query = 'SELECT * FROM Operatorzy WHERE ID = ' + doc[0].IDUzytkownik + ' AND Aktywny = 1'
                    if (req.session.program)
                        active_query += `;SELECT * FROM sl_programy p Where p.Id = ${db.escape(req.session.program)}`
                    active_query += `; SELECT 1`
                    db.query(active_query, (err, doc) => {
                        if (err) throw err
                        if (doc[0][0].Moduly == null || doc[0][0].Moduly.indexOf('sl') == -1) {
                            res.send({ flash: { msg: ['Nie masz uprawnień do modułu.'] } })
                            return
                        }
                        if (doc[0][0]) {
                            if (doc[0][0].Moduly == null || doc[0][0].Moduly.indexOf('sl') == -1) {
                                res.send({ flash: { msg: ['Nie masz uprawnień do modułu.'] } })
                                return
                            }

                        } else {
                            res.send({ flash: { msg: ['Nie masz uprawnień do moduły.'] } })
                            return
                        }
                        res.locals.info_session = doc[0][0]
                        res.locals.info_session.program = doc[1][0]

                        var query = 'UPDATE Sesje SET ? WHERE IDUzytkownik = ' + doc[0][0].ID + ' AND Modul = "sl" AND Data_wylogowania IS NULL AND ID != ' + db.escape(req.session.sid)
                        var query_data = {
                            Data_wylogowania: new Date()
                        }
                        db.query(query, query_data, function(err, result) {
                            if (err) throw err
                            query = 'SELECT l.* FROM Licencje l WHERE l.Firma = ' + doc[0][0].Firma + ' AND l.Modul = \'sl\''
                            db.query(query, function(err, response) {
                                if (err) throw err
                                if (response[0]) {
                                    if (response[0].Aktywny && !moment(new Date(response[0].Data_waznosci)).isBefore(moment())) {
                                        if (cb) {
                                            cb();
                                            return;
                                        } else {
                                            res.send({ info_session: res.locals.info_session })
                                        }
                                    } else {
                                        res.sendfile("layouts/logoutTemplate.html")
                                    }
                                } else {
                                    res.sendfile("layouts/logoutTemplate.html")
                                }
                            })
                        })
                    })
                } else {
                    res.send({ flash: { msg: ['Nie masz uprawnień do moduły.'] } })
                }
            } else {
                res.send({ flash: { msg: ['Nie masz uprawnień do moduły.'] } })
            }
        })

    }
    this.validation_remote = function(req, res, cb) {
        let key = req.body.key

        let query = `SELECT * FROM Sesje WHERE id = ${db.escape(req.body.sid)}`

        db.query(query, (err, doc) => {
            if (err) throw err
            if (doc[0] && doc[0].Klucz) {
                let decode = functions.decodeToken(config.secret, key)
                if (doc[0].Klucz == decode) {
                    if (doc[0].Data_wylogowania != null) {

                        res.json({ error: "Twoja sesja wygasła" })
                        return
                    }
                    let active_query = 'SELECT * FROM Operatorzy WHERE ID = ' + doc[0].IDUzytkownik + ' AND Aktywny = 1'
                    if (req.body.id_programu)
                        active_query += `;SELECT * FROM sl_programy p Where p.Id = ${db.escape(req.body.id_programu)}`
                    else {
                        res.json({ error: "Wybierz program do którego przydzielić punkty" })
                        return
                    }
                    active_query += `; SELECT 1`
                    db.query(active_query, (err, doc) => {
                        if (err) throw err
                        if (doc[0][0].Moduly == null || doc[0][0].Moduly.indexOf('sl') == -1) {
                            res.json({ error: 'Nie masz uprawnień do modułu.' })
                            return
                        }
                        if (doc[0][0]) {
                            if (doc[0][0].Moduly == null || doc[0][0].Moduly.indexOf('sl') == -1) {
                                res.json({ error: 'Nie masz uprawnień do modułu.' })
                                return
                            }

                        } else {
                            res.json({ error: 'Nie masz uprawnień do modułu.' })
                            return
                        }
                        res.locals.info_session = doc[0][0]
                        res.locals.info_session.program = doc[1][0]

                        query = 'SELECT l.* FROM Licencje l WHERE l.Firma = ' + doc[0][0].Firma + ' AND l.Modul = \'sl\''
                        db.query(query, function(err, response) {
                            if (err) throw err
                            if (response[0]) {
                                if (response[0].Aktywny && !moment(new Date(response[0].Data_waznosci)).isBefore(moment())) {
                                    query = `Select * From sl_klienci Where Aktywny = 1 
                                        AND Firma = ${res.locals.info_session.Firma} AND Id_programu = ${res.locals.info_session.program.Id}
                                        AND Numer like '${req.body.nr_klienta}';
                                        Select * From sl_punkty Where Aktywny = 1 AND Id_fv = ${req.body.id_fv}`
                                    db.query(query, function(err, response) {
                                        if (err) throw err
                                        if (response[1].length > 0) {
                                            res.json({ error: 'Dla tej faktury zostały już wystawione punkty.' })
                                            return
                                        }
                                        if (response[0]) {
                                            res.locals.info_session.klient = response[0][0]
                                            if (cb) {
                                                cb();
                                                return;
                                            } else {
                                                res.send({ info_session: res.locals.info_session })
                                            }
                                        } else
                                            res.json({ error: "Nie ma klienta o podanym numerze." })
                                    })

                                } else {
                                    res.json({ error: 'Twoja licencja wygasła.' })
                                }
                            } else {
                                res.json({ error: 'Twoja licencja wygasła.' })
                            }
                        })
                    })
                } else {
                    res.sendfile("layouts/logoutTemplate.html")
                }
            } else {
                res.sendfile("layouts/logoutTemplate.html")
            }
        })

    }
}
module.exports = new session()