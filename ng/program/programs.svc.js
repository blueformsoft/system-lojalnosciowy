angular.module('app')
    .service('ProgramsSvc', function($http) {
        this.edit = function(id) {
            return $http.get('/api/programs/edit?id=' + id)
        }
        this.create = function(program) {
            return $http.post('/api/programs/add', { program: program })
        }
        this.update = function(program) {
            return $http.post('/api/programs/update', { program: program })
        }
        this.uploadLogo = function(data) {
            return $http.post('/api/programs/upload_logo', data, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })
        }
        this.uploadTlo = function(data) {
            return $http.post('/api/programs/upload_tlo', data, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })
        }
        this.fetch = function() {
            return $http.get('/api/programs/fetch')
        }
        this.select = function(id) {
            return $http.post('/api/programs/select', { id })
        }
    })