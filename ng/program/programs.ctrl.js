angular.module('app')
    .controller('ProgramsCtrl', function($scope, ProgramsSvc, $window, $location, $uibModal) {
        $scope.update = function() {
            $scope.data.Data_rozpoczecia = $('#Data_rozpoczecia').val()
            $scope.data.Data_zakonczenia = $('#Data_zakonczenia').val()
            $scope.data.Mail_przywitanie = JSON.stringify({
                Title: $scope.Mail_przywitanie_title,
                Body: $('#program_mail_przywitanie').summernote('code'),
                Do_mnie: $scope.Mail_przywitanie_do_mnie,
                Do_klienta: $scope.Mail_przywitanie_do_klienta
            })
            $scope.data.Mail_cykliczny = JSON.stringify({
                Title: $scope.Mail_cykliczny_title,
                Body: $('#program_mail_cykliczny').summernote('code'),
                Do_mnie: $scope.Mail_cykliczny_do_mnie,
                Do_klienta: $scope.Mail_cykliczny_do_klienta
            })
            $scope.data.Mail_zmiana_salda = JSON.stringify({
                Title: $scope.Mail_zmiana_salda_title,
                Body: $('#program_mail_zmiana_salda').summernote('code'),
                Do_mnie: $scope.Mail_zmiana_salda_do_mnie,
                Do_klienta: $scope.Mail_zmiana_salda_do_klienta
            })
            $scope.data.Mail_zamowienie = JSON.stringify({
                Title: $scope.Mail_zamowienie_title,
                Body: $('#program_mail_zamowienie').summernote('code'),
                Do_mnie: $scope.Mail_zamowienie_do_mnie,
                Do_klienta: $scope.Mail_zamowienie_do_klienta
            })
            $scope.data.Mail_realizacja = JSON.stringify({
                Title: $scope.Mail_realizacja_title,
                Body: $('#program_mail_realizacja').summernote('code'),
                Do_mnie: $scope.Mail_realizacja_do_mnie,
                Do_klienta: $scope.Mail_realizacja_do_klienta
            })
            $scope.data.Mail_anulowanie = JSON.stringify({
                Title: $scope.Mail_anulowanie_title,
                Body: $('#program_mail_anulowanie').summernote('code'),
                Do_mnie: $scope.Mail_anulowanie_do_mnie,
                Do_klienta: $scope.Mail_anulowanie_do_klienta
            })
            $scope.data.Logo = JSON.stringify($scope.data.Logo)
            $scope.data.Tlo = JSON.stringify($scope.data.Tlo)
            $scope.data.Regulamin = $('#program_regulamin').summernote('code')
            if ($scope.data.Nazwa) {
                if ($scope.data.Id)
                    ProgramsSvc.update($scope.data).then(function(program) {
                        $window.location.href = '/programs'
                    })
                else
                    ProgramsSvc.create($scope.data).then(function(program) {
                        $window.location.href = '/programs'
                    })
            }
        }
        $scope.$watch('[data.Wysokosc_premii, data.Liczba_poziomow]', function() {
            if ($scope.data && $scope.data.Wysokosc_premii && $scope.data.Liczba_poziomow != null)
                $scope.DrawChartPL()
        })
        $scope.$watch('[data.Procent_dla_sieci, data.Procent_dla_kupujacego]', function() {
            if ($scope.data && $scope.data.Procent_dla_sieci && $scope.data.Procent_dla_kupujacego)
                $scope.DrawChartMLM()
        })
        $scope.defaultColors = ['#3366CC', '#DC3912', '#FF9900', '#109618', '#990099', '#3B3EAC', '#0099C6', '#DD4477', '#66AA00', '#B82E2E', '#316395', '#994499', '#22AA99', '#AAAA11', '#6633CC', '#E67300', '#8B0707', '#329262', '#5574A6', '#3B3EAC']
        $scope.myChart = null
        $scope.DrawChart = function(program, statistics) {
            let labels = []
            let data = []
            let canvas = $('.pointChart[data-program="' + program + '"]').get(0)
            var ctx = canvas.getContext('2d');
            for (var i = 0; i < statistics.length; i++) {
                labels.push(moment(statistics[i].miesiac).format('MMMM YYYY'))
                data.push(statistics[i].punkty)
            }
            $scope.myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [{
                        data: data,
                        borderWidth: 1,
                        backgroundColor: '#2884c9'
                    }]
                },
                options: {
                    tooltips: {
                        enabled: true,
                        mode: 'single',
                        callbacks: {
                            label: function(tooltipItems, data) {
                                return data.datasets[0].data[tooltipItems.index] + 'punktów';
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    legend: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: 'Wykres ilości przyznawanych punktów w ostatnich 12 miesiącach'
                    }
                }
            });
        }

        $scope.DrawChartPL = function() {
            let canvas = document.getElementById("myChart")
            var ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            let data = []
            let labels = []
            let sumaPremii = 0
            let wysokoscPremii = $scope.data.Wysokosc_premii
            let colors = []
            if ($scope.data.Liczba_poziomow == 0) {
                data.push($scope.data.Wysokosc_premii)
                labels.push('Premia dla kupującego')
                colors.push($scope.defaultColors[0])
            }
            var i = 0
            if ($scope.data.Liczba_poziomow != 0)
                for (i = 0; i <= $scope.data.Liczba_poziomow; i++) {
                    wysokoscPremii = wysokoscPremii / 2
                    sumaPremii += wysokoscPremii
                    data.push(wysokoscPremii)
                    if (i == 0)
                        labels.push('Premia dla kupującego')
                    else
                        labels.push('Premia dla polecającego ' + i + ' stopnia')
                    colors.push($scope.defaultColors[i + 1])
                }
            data.push(100 - sumaPremii)
            labels.push('Kwota pozostająca w firmie')
            colors.push($scope.defaultColors[i + 1])
            if ($scope.myChart) $scope.myChart.destroy()
            $scope.myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: labels,
                    datasets: [{
                        backgroundColor: colors,
                        data: data,
                        borderWidth: 1
                    }]
                },
                options: {
                    tooltips: {
                        enabled: true,
                        mode: 'single',
                        callbacks: {
                            label: function(tooltipItems, data) {
                                return data.datasets[0].data[tooltipItems.index] + '%';
                            }
                        }
                    },
                }
            });
        }

        $scope.DrawChartMLM = function() {
            let canvas = document.getElementById("myChart")
            var ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            let data = []
            let labels = []
            data.push($scope.data.Procent_dla_sieci)
            labels.push('Premia dla sieci polecających')
            data.push($scope.data.Procent_dla_kupujacego)
            labels.push('Premia dla kupującego')
            data.push(100 - $scope.data.Procent_dla_sieci - $scope.data.Procent_dla_kupujacego)
            labels.push('Kwota pozostająca w firmie')
            let colors = ['#3366CC', '#DC3912', '#FF9900']
            if ($scope.myChart) $scope.myChart.destroy()
            $scope.myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: labels,
                    datasets: [{
                        backgroundColor: colors,
                        data: data,
                        borderWidth: 1
                    }]
                },
                options: {
                    tooltips: {
                        enabled: true,
                        mode: 'single',
                        callbacks: {
                            label: function(tooltipItems, data) {
                                return data.datasets[0].data[tooltipItems.index] + '%';
                            }
                        }
                    },
                }
            });
        }

        $scope.uploadLogo = function() {
            if (document.getElementById('program_logo').files.length == 0)
                ShowDialog($uibModal, 'Przesyłanie pliku', 'Nie wybrałeś żadnego pliku', 1, true)
            else {
                var formData = new FormData()
                formData.append("logo", document.getElementById('program_logo').files[0])
                ProgramsSvc.uploadLogo(formData).then(function(data) {
                    $scope.data.Logo = data.data
                })
            }
        }
        $scope.uploadTlo = function() {
            if (document.getElementById('program_tlo').files.length == 0)
                ShowDialog($uibModal, 'Przesyłanie pliku', 'Nie wybrałeś żadnego pliku', 1, true)
            else {
                var formData = new FormData()
                formData.append("tlo", document.getElementById('program_tlo').files[0])
                ProgramsSvc.uploadTlo(formData).then(function(data) {
                    $scope.data.Tlo = data.data
                })
            }
        }
        $scope.goToEdit = function(id) {
            $window.location.href = '/programs/edit?id=' + id
        }
        $scope.goToSelectProgram = function() {
            $window.location.href = '/programs'
        }
        $scope.fetch = function() {
            ProgramsSvc.fetch().then(function(data) {
                $scope.programs = data.data
                for (var i = 0; i < $scope.programs.length; i++) {
                    if (moment($scope.programs[i].Data_waznosci).isBefore(moment($scope.programs[i].Data_zakonczenia))) {
                        $scope.programs[i].Licencja = `Licencja wygasa przed ukończeniem programu (${moment($scope.programs[i].Data_waznosci).format('YYYY-MM-DD')})`
                    }
                    if ($scope.programs[i].Logo)
                        $scope.programs[i].Logo = JSON.parse($scope.programs[i].Logo)
                    $scope.programs[i].Data_rozpoczecia = moment($scope.programs[i].Data_rozpoczecia).format('YYYY.MM.DD')
                    $scope.programs[i].Data_zakonczenia = moment($scope.programs[i].Data_zakonczenia).format('YYYY.MM.DD')
                }
                angular.element(document).ready(function() {
                    for (var i = 0; i < $scope.programs.length; i++) {
                        $scope.DrawChart($scope.programs[i].Id, $scope.programs[i].statistics)
                    }
                })
            })
        }
        $scope.select = function(id) {
            ProgramsSvc.select(id).then(function(data) {
                if (data.data == 'OK')
                    $window.location.href = '/'
            })
        }
        $scope.edit = function(id) {
            ProgramsSvc.edit(id).then(function(data) {
                $scope.data = data.data.program
                $scope.data.Kontakt = $scope.data.Kontakt == 1
                $scope.data.Generuj_numer = $scope.data.Generuj_numer == 1
                $scope.ilosc_klientow = data.data.ilosc_klientow
                if ($scope.data.Tlo) $scope.data.Tlo = JSON.parse($scope.data.Tlo)
                if ($scope.data.Logo) $scope.data.Logo = JSON.parse($scope.data.Logo)
                if (data.data.program.Regulamin) {
                    $('#program_regulamin').summernote('code', $scope.data.Regulamin)
                }
                if (data.data.program.Mail_przywitanie) {
                    let mail_przywitanie = JSON.parse(data.data.program.Mail_przywitanie)
                    $scope.Mail_przywitanie_title = mail_przywitanie.Title
                    $scope.Mail_przywitanie_do_mnie = mail_przywitanie.Do_mnie || false
                    $scope.Mail_przywitanie_do_klienta = mail_przywitanie.Do_klienta || false
                    $('#program_mail_przywitanie').summernote('code', mail_przywitanie.Body)
                }
                if (data.data.program.Mail_cykliczny) {
                    let mail_cykliczny = JSON.parse(data.data.program.Mail_cykliczny)
                    $scope.Mail_cykliczny_title = mail_cykliczny.Title
                    $scope.Mail_cykliczny_do_mnie = mail_cykliczny.Do_mnie || false
                    $scope.Mail_cykliczny_do_klienta = mail_cykliczny.Do_klienta || false
                    $('#program_mail_cykliczny').summernote('code', mail_cykliczny.Body)
                }
                if (data.data.program.Mail_zmiana_salda) {
                    let mail_zmiana_salda = JSON.parse(data.data.program.Mail_zmiana_salda)
                    $scope.Mail_zmiana_salda_title = mail_zmiana_salda.Title
                    $scope.Mail_zmiana_salda_do_mnie = mail_zmiana_salda.Do_mnie
                    $scope.Mail_zmiana_salda_do_klienta = mail_zmiana_salda.Do_klienta
                    $('#program_mail_zmiana_salda').summernote('code', mail_zmiana_salda.Body)
                }
                if (data.data.program.Mail_zamowienie) {
                    let mail_zamowienie = JSON.parse(data.data.program.Mail_zamowienie)
                    $scope.Mail_zamowienie_title = mail_zamowienie.Title
                    $scope.Mail_zamowienie_do_mnie = mail_zamowienie.Do_mnie || false
                    $scope.Mail_zamowienie_do_klienta = mail_zamowienie.Do_klienta || false
                    $('#program_mail_zamowienie').summernote('code', mail_zamowienie.Body)
                }
                if (data.data.program.Mail_realizacja) {
                    let mail_realizacja = JSON.parse(data.data.program.Mail_realizacja)
                    $scope.Mail_realizacja_title = mail_realizacja.Title
                    $scope.Mail_realizacja_do_mnie = mail_realizacja.Do_mnie || false
                    $scope.Mail_realizacja_do_klienta = mail_realizacja.Do_klienta || false
                    $('#program_mail_realizacja').summernote('code', mail_realizacja.Body)
                }
                if (data.data.program.Mail_anulowanie) {
                    let mail_anulowanie = JSON.parse(data.data.program.Mail_anulowanie)
                    $scope.Mail_anulowanie_title = mail_anulowanie.Title
                    $scope.Mail_anulowanie_do_mnie = mail_anulowanie.Do_mnie || false
                    $scope.Mail_anulowanie_do_klienta = mail_anulowanie.Do_klienta || false
                    $('#program_mail_anulowanie').summernote('code', mail_anulowanie.Body)
                }
            })
        }
        if ($location.$$absUrl.indexOf('/programs/edit') != -1 || $location.$$absUrl.indexOf('/programs/add') != -1) {
            SummerNote('#program_mail_przywitanie')
            SummerNote('#program_mail_cykliczny')
            SummerNote('#program_mail_zmiana_salda')
            SummerNote('#program_mail_zamowienie')
            SummerNote('#program_mail_realizacja')
            SummerNote('#program_mail_anulowanie')
            SummerNote('#program_regulamin', 300)
        }
        if ($location.$$absUrl.indexOf('/programs/edit') != -1)
            $scope.edit(getParameterByName('id'))
        else
            $scope.fetch()
    })