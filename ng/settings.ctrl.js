angular.module('app')
    .controller('SettingsCtrl', function($scope, $window, SettingsSvc) {
        $scope.Update = function() {
            SettingsSvc.update($scope.data)
                .success(function(status) {
                    if (status == 200)
                        $window.location.reload()
                })
        }
        SettingsSvc.fetch().success(function(data) {
            $scope.data = data.settings
        })
    })