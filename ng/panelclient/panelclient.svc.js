angular.module('app')
    .service('PanelClientSvc', function($http) {
        this.programdata = function() {
            return $http.get('/api/panelclient/program_data')
        }
        this.updateAccount = function(data) {
            return $http.post('/api/panelclient/update_account', data)
        }
        this.getPolecajacy = function(program) {
            return $http.get('/api/panelclient/get_polecajacy?program=' + program)
        }
    })