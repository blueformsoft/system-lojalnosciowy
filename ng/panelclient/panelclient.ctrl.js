angular.module('app')
    .controller('PanelClientCtrl', function($uibModal, $scope, PanelClientSvc, $window, $location) {

        $scope.convertToRoman = function(num) {
            var roman = { "M": 1000, "CM": 900, "D": 500, "CD": 400, "C": 100, "XC": 90, "L": 50, "XL": 40, "X": 10, "IX": 9, "V": 5, "IV": 4, "I": 1 };
            var str = "";

            for (var i in roman) {
                var q = Math.floor(num / roman[i]);
                num -= q * roman[i];
                str += i.repeat(q);
            }

            return str;
        }
        $scope.DrawChart = function(statistics) {
            let labels = []
            let data = []
            let canvas = $('.pointChart').get(0)
            var ctx = canvas.getContext('2d');
            for (var i = 0; i < statistics.length; i++) {
                labels.push(moment(statistics[i].miesiac).format('MMMM YYYY'))
                data.push(statistics[i].punkty)
            }
            $scope.myChart = null
            $scope.myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: [{
                        data: data,
                        borderWidth: 1,
                        backgroundColor: '#2884c9'
                    }]
                },
                options: {
                    tooltips: {
                        enabled: true,
                        mode: 'single',
                        callbacks: {
                            label: function(tooltipItems, data) {
                                return data.datasets[0].data[tooltipItems.index] + ' punktów';
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    legend: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: 'Wykres ilości przyznawanych punktów w ostatnich 12 miesiącach'
                    }
                }
            });
        }
        $scope.programdata = function() {
            PanelClientSvc.programdata().then(function(data) {
                $scope.client = data.data.client
                $scope.client.Dokumenty = JSON.parse($scope.client.Dokumenty)
                $scope.client.WszystkieDokumenty = JSON.parse($scope.client.WszystkieDokumenty)
                $scope.client.StanKonta = parseFloat($scope.client.SumaPunktowDodatnichZatwierdzonych + $scope.client.SumaPunktowUjemnychZatwierdzonych).toFixed()
                if (data.data.polecajacy.length > 0)
                    $scope.polecajacy = data.data.polecajacy[data.data.polecajacy.length - 1][0]
                $scope.poleceni = data.data.poleceni
                for (var i = 0; i < $scope.poleceni.length; i++) {
                    if ($scope.poleceni[i].DokumentyPoleconego) {
                        $scope.poleceni[i].DokumentyPoleconego = JSON.parse($scope.poleceni[i].DokumentyPoleconego)
                    }
                    if ($scope.poleceni[i].DokumentyPoleconychPoleconego) {
                        $scope.poleceni[i].DokumentyPoleconychPoleconego = JSON.parse($scope.poleceni[i].DokumentyPoleconychPoleconego)
                    }
                }
                angular.element(document.getElementById("panel")).ready(function() {
                    $('.document-table').DataTable({
                        scrollCollapse: true,
                        scrollY: 150,
                        paging: false,
                        responsive: true,
                        searching: false
                    })
                    $('.statistic-table').DataTable({
                        scrollCollapse: true,
                        scrollY: 150,
                        searching: false,
                        ordering: false,
                        lengthChange: false,
                        paging: false,
                        info: false
                    })
                    $scope.DrawChart($scope.client.Statystyki)
                })
            })
        }

        $scope.programClick = function(id) {
            RefreshAccount(id, $scope.info_session.Id, true)
        }

        $scope.programdata()
    })