angular.module('app')
    .controller('ClientsCtrl', function($scope, ClientsSvc, $window, $location) {
        $scope.clients = []
        $scope.selectedClient = null
        $scope.afterSelectClient = function(selected) {
            if (selected) {
                $scope.clientName = selected.originalObject.Nazwa
                $scope.clientEmail = selected.originalObject.Email
            }
        }
        $scope.Code_amount_visible = false
        $scope.model = {}
        $scope.ToggleCodeAmount = function() {
            $scope.Code_amount_visible = !$scope.Code_amount_visible
        }
        $scope.Activate = function(id) {
            ClientsSvc.activate({ id: id }).then(function(result) {
                if (result.data == 'OK') {
                    cardtable.ajax.reload()
                } else
                    ShowError(result.data.error)
            })
        }
        $scope.GenerateCodes = function() {
            ClientsSvc.generatecodes({
                amount: $scope.model.amount
            }).then(function(result) {
                if (result.data == 'OK')
                    location.reload()
                else
                    ShowError(result.data.errors)
            })
        }
        $scope.resetpassword = function() {
            ClientsSvc.resetpassword({
                clientId: $scope.clientId,
                clientEmail: $scope.clientEmail
            }).then(function(result) {
                if (result.data == 'OK')
                    ShowInfo(["Wiadomość została wysłana."])
                else
                    ShowError(result.data.errors)
            })
        }
        $scope.update = function() {
            if ($('#clientParent').val().indexOf('undefined') < 0)
                $scope.clientParent = $('#clientParent').val()
            $scope.clientName = $('#clientName_value').val()
            if ($scope.clientName && $scope.clientEmail && ($scope.clientParent || $scope.clientId || $scope.persons.length == 0)) {
                if ($scope.clientId)
                    ClientsSvc.update({
                        clientId: $scope.clientId,
                        clientName: $scope.clientName,
                        clientEmail: $scope.clientEmail,
                        clientParent: $scope.clientParent
                    }).then(function(result) {
                        if (result.data == 'OK')
                            $window.location.href = '/#/clients'
                        else
                            ShowError(result.data.errors)
                    })
                else {
                    ClientsSvc.create({
                            clientName: $scope.clientName,
                            clientEmail: $scope.clientEmail,
                            clientParent: $scope.clientParent,
                            clientGenerujNumer: $scope.$parent.program.Generuj_numer,
                            clientDlugoscNumeru: $scope.$parent.program.Dlugosc_numeru
                        })
                        .then(function(result) {
                            if (result.data == 'OK')
                                $window.location.href = '/#/clients'
                            else {
                                ShowError(result.data.errors)
                            }
                        })
                }
            } else {
                let errors = []
                if (!$scope.clientEmail)
                    errors.push("Pole email jest wymagane")
                if (!$scope.clientName)
                    errors.push("Pole nazwa jest wymagane")
                if ($scope.clientParent.indexOf('undefined') >= 0)
                    errors.push("Polecający jest wymagany")
                ShowError(errors)
            }
        }

        $scope.edit = function(id) {
            ClientsSvc.edit(id).then(function(data) {
                $scope.clientId = data.data.client.Id
                $scope.clientName = data.data.client.Nazwa
                $scope.clientEmail = data.data.client.Email
                let polecajacy = JSON.parse(data.data.client.Polecajacy)
                if (polecajacy.length > 0) {
                    $scope.clientParent = polecajacy[polecajacy.length - 1]
                    $('#clientParent').val($scope.clientParent)
                    $('.selectpicker').selectpicker('refresh')
                }
                $('#clientName_value').val($scope.clientName)
            })
        }


        if ($location.$$path == '/clients/edit' || $location.$$path == '/clients/add')
            ClientsSvc.getPolecajacy().then(function(data) {
                $scope.persons = data.data.parents
                $scope.clients = data.data.clients
                angular.element(document.getElementById("clientParent")).ready(function() {
                    $('.selectpicker').selectpicker('refresh');
                })
                if ($location.$$path == '/clients/edit')
                    $scope.edit($location.search().id)
            })


    })