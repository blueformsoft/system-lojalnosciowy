angular.module('app')
    .service('ClientsSvc', function($http) {
        this.fetch = function() {
            return $http.get('/api/clients')
        }
        this.edit = function(id) {
            return $http.get('/api/clients/edit?id=' + id)
        }
        this.create = function(client) {
            return $http.post('/api/clients/add', client)
        }
        this.update = function(client) {
            return $http.post('/api/clients/update', client)
        }
        this.activate = function(client) {
            return $http.post('/api/clients/activate', client)
        }
        this.resetpassword = function(client) {
            return $http.post('/api/clients/reset_password', client)
        }
        this.generatecodes = function(amount) {
            return $http.post('/api/clients/generate_codes', amount)
        }
        this.getPolecajacy = function() {
            return $http.get('/api/clients/get_polecajacy')
        }
    })