angular.module('app')
    .controller('DialogCtrl', function($uibModalInstance, $scope, dialog, mode) {
        $scope.header = dialog.header
        $scope.body = dialog.body
        $scope.ShowDesc = dialog.desc
        if (mode == 0)
            $scope.OK = false
        else
            $scope.OK = true
        $scope.YesClick = function() {
            $uibModalInstance.close({value: true, desc: $('#Desc').val()})
        }
        $scope.NoClick = function() {
            $uibModalInstance.close(false)
        }
    })