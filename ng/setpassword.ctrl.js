angular.module('app')
    .controller('SetPasswordCtrl', function($scope, $window, $location, UserClientSvc) {
        $scope.setPassword = function() {
            $scope.haslo = $('#haslo').val()
            $scope.haslo_powt = $('#haslo_powt').val()
            if ($scope.haslo && $scope.haslo_powt && $scope.haslo == $scope.haslo_powt) {
                UserClientSvc.setPassword({
                    Id: $scope.id,
                    haslo: $scope.haslo,
                    token: $scope.token
                }).then(function(data) {
                    if (data.data == 'OK') {
                        ShowInfo(["Hasło zostało pomyślnie utworzone."])
                        $scope.IsSaved = true
                    }
                })
            } else {
                ShowError(["Hasła do siebie nie pasują"])
            }
        }
        $scope.checkNumber = function(){
            $scope.number = $('#number').val()
            if ($scope.number) {
                UserClientSvc.checkNumber({
                    Id: $scope.id,
                    number: $scope.number,
                    token: $scope.token
                }).then(function(data) {
                    if (data.data == 'OK') {
                        ShowInfo(["Numer zweryfikowany poprawnie."])
                        $scope.IsNumberCorrect = true
                    }
                    else{
                        ShowError(["Numer nie został zweryfikowany poprawnie"])
                    }
                })
            } else {
                ShowError(["Hasła do siebie nie pasują"])
            }
        }
        $scope.GoToLogin = function() {
            let parts = $location.$$absUrl.split('#')[0].split('/')
            let firma = parts[4]
            let program = parts[5]
            $window.location.href = UrlData.clientUrl
        }
        $scope.IsNumberCorrect = false
        $scope.IsSaved = false
        $scope.id = getParameterByName('id')
        $scope.token = getParameterByName('token')
    })