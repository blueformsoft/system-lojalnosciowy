angular.module('app')
    .service('ClientSettingsSvc', function($http) {
        this.edit = function() {
            return $http.get('/api/clientsettings/edit')
        }
        this.update = function(client) {
            return $http.post('/api/clientsettings/update', client)
        }
        this.resetpassword = function(client) {
            return $http.post('/api/clients/reset_password', client)
        }
    })