angular.module('app')
    .controller('ChangePasswordCtrl', function($scope, $window, UserClientSvc, $uibModalInstance) {
        $scope.update = function() {
            if ($scope.haslo_old) {
                if ($scope.haslo && $scope.haslo_powt && $scope.haslo == $scope.haslo_powt) {
                    UserClientSvc.changePassword({
                        Haslo_old: $scope.haslo_old,
                        Haslo: $scope.haslo
                    }).then(function(data) {
                        if (data.data == 'OK') {
                            $uibModalInstance.close(true)
                        } else {
                            ShowDialogError(data.data.errors)
                        }
                    })
                } else {
                    ShowDialogError(["Hasła do siebie nie pasują"])
                }
            } else
                ShowDialogError(["Wpisz aktualne hasło"])
        }
        $scope.cancel = function() {
            $uibModalInstance.close(false)
        }
    })