angular.module('app')
    .controller('ClientSettingsCtrl', function($scope, ClientSettingsSvc, $window, $location, $uibModal) {
        $scope.edit = function() {
            ClientSettingsSvc.edit().then(function(data) {
                $scope.user = data.data.client
            })
        }
        $scope.update = function() {
            if ($scope.user.Id)
                ClientSettingsSvc.update({
                    Id: $scope.user.Id,
                    Nazwisko: $scope.user.Nazwisko,
                    Imie: $scope.user.Imie,
                    Telefon: $scope.user.Telefon,
                    Adres: $scope.user.Adres
                }).then(function(result) {
                    if (result.data == 'OK')
                        ShowInfo(["Zmiany zostały zapisane"])
                    else
                        ShowError(result.data.errors)
                })
        }
        $scope.changePassword = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/user/changePasswordDialog.html',
                controller: 'ChangePasswordCtrl',
                controllerAs: '$ctrl',
            })

            modalInstance.result.then(function(data) {
                if (data)
                    ShowInfo(["Hasło zostało zmienione."])
            })
        }
        $scope.edit()
    })