angular.module('app')
    .service('UserSvc', function($http) {
        var svc = this
        svc.getSessionData = function() {
            return $http.get('/api/sessions/data')
        }
        svc.getUser = function() {
            return $http.get('/api/users')
        }
        svc.login = function(username, password) {
            return $http.post('/api/sessions', {
                login: username,
                haslo: password
            })
        }
        svc.logout = function(username, password) {
            return $http.post('/api/sessions/logout')
        }
        svc.goToMain = function() {
            return $http.post('/api/sessions/go_to_main')
        }
    })