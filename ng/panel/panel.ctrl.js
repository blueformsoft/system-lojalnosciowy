angular.module('app')
    .controller('PanelCtrl', function($uibModal, $scope, PanelSvc, $window, $location) {

        $scope.convertToRoman = function(num) {
            var roman = { "M": 1000, "CM": 900, "D": 500, "CD": 400, "C": 100, "XC": 90, "L": 50, "XL": 40, "X": 10, "IX": 9, "V": 5, "IV": 4, "I": 1 };
            var str = "";

            for (var i in roman) {
                var q = Math.floor(num / roman[i]);
                num -= q * roman[i];
                for (var j = 0; j < q; j++)
                    str += i
            }

            return str;
        }

        $scope.paneldata = function(id) {
            PanelSvc.paneldata(id).then(function(data) {
                $scope.client = data.data.client
                $scope.polecajacy = data.data.polecajacy
                angular.element('#program_client_table').ready(function() {
                    RefreshAccount()
                })
            })
        }
        $scope.programClick = function(id) {
            RefreshAccount(id)
        }
        $scope.showAddDocument = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'administration/panel/addDocumentDialog.html',
                controller: 'PanelAddDocumentCtrl',
                controllerAs: '$ctrl',
                resolve: {
                    client: function() {
                        return $scope.client
                    },
                    fv: function() {
                        return null
                    },
                    program: function() {
                        return $scope.$parent.program
                    },
                    polecajacy: function() {
                        return $scope.polecajacy
                    }
                }
            })

            modalInstance.result.then(function() {
                RefreshAccount()
            })
        }

        $scope.showEditDocument = function(fv) {
            if (fv.Id_parent)
                ShowDialog($uibModal, 'Edycja dokumentu', 'Punkty zostały przydzielone na podstawie innego dokumentu. Edycja nie jest możliwa.', 1, false)
            else {
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'administration/panel/addDocumentDialog.html',
                    controller: 'PanelAddDocumentCtrl',
                    controllerAs: '$ctrl',
                    resolve: {
                        client: function() {
                            return $scope.client
                        },
                        fv: function() {
                            return fv
                        },
                        program: function() {
                            return $scope.$parent.program
                        },
                        polecajacy: function() {
                            return $scope.polecajacy
                        }
                    }
                })

                modalInstance.result.then(function() {
                    RefreshAccount()
                })
            }
        }
        $scope.removeDocument = function(el) {
            ShowDialog($uibModal, 'Usuwanie dokumentu', 'Czy na pewno chcesz usunąć ten dokument? Jeśli tak, wpisz powód usunięcia w poniższym polu.', 0, true, function(data) {
                if (data.value) {
                    PanelSvc.remove({Id: el.Id, Desc: data.desc}).then(function(data) {
                        if (data.data == 'OK')
                            RefreshAccount()
                    })
                }
            })
        }
        $scope.confirmDocument = function(fv) {
            if (fv.Id_parent)
                ShowDialog($uibModal, 'Zatwierdzenie dokumentu', 'Punkty zostały przydzielone na podstawie innego dokumentu. Zatwierdzenie nie jest możliwa.', 1)
            else
                PanelSvc.confirm(fv.Id).then(function(data) {
                    if (data.data == 'OK')
                        RefreshAccount()
                })

        }
        $scope.paneldata($location.search().id)
    })