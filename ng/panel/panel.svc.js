angular.module('app')
    .service('PanelSvc', function($http) {
        this.paneldata = function(client) {
            return $http.get('/api/clients/panel_data?client=' + client)
        }
        this.programdata = function(client) {
            return $http.get('/api/clients/program_data?client=' + client)
        }
        this.updateAccount = function(data) {
            return $http.post('/api/clients/update_account', data)
        }
        this.remove = function(fv) {
            return $http.post('/api/clients/remove_document', fv)
        }
        this.confirm = function(fv) {
            return $http.post('/api/clients/confirm_document', { fv })
        }
        this.importFV = function(data) {
            return $http.post('/api/clients/importFV', { fv: data })
        }
    })