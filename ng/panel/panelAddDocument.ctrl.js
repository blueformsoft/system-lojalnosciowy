angular.module('app')
    .controller('PanelAddDocumentCtrl', function($uibModalInstance, $scope, polecajacy, program, client, fv, PanelSvc, $window, $location) {
        $scope.$watch('fv.Kwota', function() {
            if ($scope.programdesc) {
                if (!$scope.programdesc.MLM) {
                    var premia = $scope.programdesc.Wysokosc_premii
                    if (!$scope.programdesc.Punkty && $scope.programdesc.Czy_pierwsza_premia)
                        premia = $scope.programdesc.Wysokosc_pierwszej_premii
                    var punkty = premia * 0.005 * $scope.fv.Kwota
                    $scope.fv.Punkty = parseFloat(punkty.toFixed())
                    var liczba_poziomow = 1
                    for (var i = $scope.polecajacy.length - 1; i >= 0; i--) {
                        liczba_poziomow++
                        punkty = 0.5 * punkty
                        $scope.polecajacy[i][0].Punkty = parseFloat(punkty.toFixed())
                        if (liczba_poziomow > $scope.programdesc.Liczba_poziomow)
                            break
                    }
                } else {
                    let premia = $scope.programdesc.Procent_dla_sieci * $scope.fv.Kwota * 0.01
                    $scope.fv.Punkty = parseFloat(premia * $scope.programdesc.Procent_dla_kupujacego * 0.01)
                    premia = premia - $scope.fv.Punkty
                    if ($scope.polecajacy.length > 0) {
                        premia = parseFloat((premia / $scope.polecajacy.length).toFixed())
                        for (var i = 0; i < $scope.polecajacy.length; i++)
                            $scope.polecajacy[i][0].Punkty = premia
                    }
                }
            }
        })
        $scope.importFV = function() {
            PanelSvc.importFV($scope.faktura).then(function(data) {
                if (data.data.errors) {
                    ShowDialogError(data.data.errors)
                } else {
                    HideDialogError()
                    $scope.fv.Id_fv = data.data.ID
                    $scope.fv.Zatwierdzone = data.data.Zaplacona ? true : false
                    $scope.fv.Kwota = data.data.Do_zaplaty
                    $scope.fv.Data = moment(data.data.Data_wystawienia).format('YYYY-MM-DD')
                    $scope.fv.Opis = data.data.Nr_faktury
                }
            })
        }
        $scope.convertToRoman = function(num) {
            var roman = { "M": 1000, "CM": 900, "D": 500, "CD": 400, "C": 100, "XC": 90, "L": 50, "XL": 40, "X": 10, "IX": 9, "V": 5, "IV": 4, "I": 1 };
            var str = "";

            for (var i in roman) {
                var q = Math.floor(num / roman[i]);
                num -= q * roman[i];
                str += i.repeat(q);
            }

            return str;
        }
        $scope.programdata = function() {
            $scope.programdesc = program
            $scope.programclient = client
            if (fv) {
                $scope.fv = fv
                $scope.fv.Zatwierdzone = ($scope.fv.Zatwierdzone == 1)
                $scope.fv.Data = moment($scope.fv.Data).format('YYYY-MM-DD')
            } else
                $scope.fv = {
                    Id_programu: $scope.programdesc.Id,
                    Id_klienta: $scope.programclient.Id
                }
            $scope.polecajacy = polecajacy
        }
        $scope.cancel = function() {
            $uibModalInstance.close(false)
        }
        $scope.updateAccount = function() {
            $scope.fv.Data = $("#data_data").val()
            PanelSvc.updateAccount({
                data: $scope.fv,
                polecajacy: $scope.polecajacy || []
            }).then(function() {
                $uibModalInstance.close(false)
            })
        }

        $scope.client = client
        $scope.programdata()
    })