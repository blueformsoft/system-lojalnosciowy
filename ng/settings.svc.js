angular.module('app')
    .service('SettingsSvc', function($http) {
        this.fetch = function() {
            return $http.get('/api/settings')
        }
        this.update = function(data) {
            return $http.post('/api/settings/update', { settings: data })
        }
    })