angular.module('app')
    .service('ApplicationClientSvc', function($http) {
        this.details = function(firma, program) {
            return $http.get('/api/clientapp/details/' + firma + '/' + program)
        }
        this.sendMail = function(data) {
            return $http.post('/api/clientapp/sendmail', data)
        }
        this.resetPassword = function(data) {
            return $http.post('/api/clientapp/reset_password', data)
        }
        this.getPrizes = function(data) {
            return $http.post('/api/clientapp/getprizes', data)
        }
    })