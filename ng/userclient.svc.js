angular.module('app')
    .service('UserClientSvc', function($http) {
        var svc = this
        svc.getSessionData = function() {
            return $http.get('/api/clientsessions/data')
        }
        svc.getUser = function() {
            return $http.get('/api/users')
        }
        svc.login = function(username, password, firma, program) {
            return $http.post('/api/clientsessions', {
                login: username,
                haslo: password,
                firma,
                program
            })
        }
        svc.register = function(user) {
            return $http.post('/api/userclient/register', user)
        }
        svc.activateMail = function(data){
            return $http.post('/api/userclient/activate_mail', data)
        }
        svc.setPassword = function(data) {
            return $http.post('/api/userclient/set_password', data)
        }
        svc.checkNumber = function(data) {
            return $http.post('/api/userclient/check_number', data)
        }
        svc.changePassword = function(data) {
            return $http.post('/api/userclient/change_password', data)
        }
        svc.logout = function(username, password) {
            return $http.post('/api/clientsessions/logout')
        }
    })