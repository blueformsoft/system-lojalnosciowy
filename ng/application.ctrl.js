angular.module('app')
    .controller('ApplicationCtrl', function($scope, $window, UserSvc, $sce) {
        $scope.RefreshSessionData = function() {
            if (window.opener)
                $scope.btnLogoutText = "Zamknij"
            UserSvc.getSessionData().then(function(data) {
                $scope.info_session = data.data.info_session
                $scope.program = data.data.program
                $scope.licencje = data.data.licencje
                $scope.licencje.forEach(function(item) {
                    if (item.Modul == 'crm') item.Url = 'https://crm.firmotron.pl'
                    else if (item.Modul == 'fv') item.Url = 'https://fv.firmotron.pl'
                })
            })
        }
        $scope.zaloguj = function() {
            if ($scope.login && $scope.haslo) {
                UserSvc.login($scope.login, $scope.haslo)
                    .then(function(data) {
                        if (data.data.success)
                            $window.location.reload()
                        else
                            $scope.flash = { messages: [{ msg: $sce.trustAsHtml(data.data.err) }], type: 'alert-danger' }
                    })
            }
        }

        $scope.wyloguj = function() {
            if (window.opener){
                window.close()
            }
            else
                UserSvc.logout()
                .then(function() {
                    $window.location.reload()
                })
        }
        $scope.gotoMain = function() {
            UserSvc.goToMain().then(function() {
                window.location = '/'
            })
        }
        $scope.RefreshSessionData()
    })