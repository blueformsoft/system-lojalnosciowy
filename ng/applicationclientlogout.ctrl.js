angular.module('app')
    .controller('ApplicationClientLogoutCtrl', function($scope, $window, $location, UserClientSvc, ApplicationClientSvc, $sce) {
        $scope.zaloguj = function() {
            $scope.login = $('#login').val()
            $scope.haslo = $('#haslo').val()
            if ($scope.login && $scope.haslo) {
                UserClientSvc.login($scope.login, $scope.haslo, $scope.Firma.ID, $scope.Program.Id)
                    .then(function(data) {
                        if (data.data.error) {
                            let errors = []
                            errors.push(data.data.error)
                            if (data.data.error.indexOf('aktywowane') >= 0) {
                                $scope.toActivate = data.data.toActivate
                                errors.push('<a href="#" onclick="ActivateMail()">Wyślij ponownie maila aktywacyjnego.</a>')
                            }
                            HideInfo()
                            ShowError(errors)
                        } else {
                            $scope.info_session = data.data.info_session
                            $scope.gotoMain()
                        }
                    })
            }
        }
        $scope.toActivate = {}
        $scope.ActivateMail = function() {
            if ($scope.toActivate)
                UserClientSvc.activateMail($scope.toActivate).then(function(data) {
                    if (data.data.errors) {
                        ShowError(data.data.errors)
                    } else {
                        HideError()
                        ShowInfo(["Na podany adres email został wysłany link aktywacyjny. Aktywuj konto zanim zaczniesz korzystać z systemu."])
                    }
                })
        }
        $scope.user = {}
        $scope.IsCard = getParameterByName('isCard')
        $scope.gotoRules = function() {
            window.location = `${UrlData.clientUrl}/regulamin`
        }
        $scope.gotoAboutMe = function() {
            if ($scope.Program.O_nas)
                window.open($scope.Program.O_nas, '_blank')
            else
                alert('Brak strony "O nas"')
        }
        $scope.goToRegister = function(IsCard) {
            window.location = `${UrlData.clientUrl}/register?isCard=`+IsCard
        }
        $scope.gotoContact = function() {
            if ($scope.Program.Kontakt) {
                window.location = `${UrlData.clientUrl}/kontakt`
            }
        }
        $scope.resetPassword = function() {
            if (!$scope.login) {
                ShowError(["Wpisz login"])
                return
            }
            var data = {
                login: $scope.login,
                firma: UrlData.firma,
                program: UrlData.program
            }
            ApplicationClientSvc.resetPassword(data).then(function(data) {
                if (data.data.errors) ShowError(data.data.errors)
                else {
                    ShowInfo(["Na podany adres mailowy została wysłana wiadomość pozwalająca na reset hasła."])
                }
            })
        }
        $scope.gotoMain = function() {
            window.location = `${UrlData.clientUrl}`
        }
        $scope.goToResetPassword = function() {
            window.location = `${UrlData.clientUrl}/reset_password`
        }
        $scope.goToLogin = function() {
            window.location = `${UrlData.clientUrl}`
        }
        $scope.sendEmail = function() {
            if ($scope.data.email && $scope.data.imie && $scope.data.nazwisko && $scope.data.tresc && $scope.data.zgoda)
                ApplicationClientSvc.sendMail($scope.data).then(function(data) {
                    ShowInfo(["Wiadomość została wysłana"])
                })
            else if (!$scope.data.zgoda)
                ShowError(["Wymagana jest zgoda na przetwarzanie danych osobowych"])
            else
                ShowError(["Wypełnij wszystkie pola"])
        }
        $scope.Details = function() {

            ApplicationClientSvc.details(UrlData.firma, UrlData.program).then(function(data) {
                $scope.Firma = data.data.Firma
                $scope.Program = data.data.Program
                if ($scope.Program.Tlo) $scope.Program.Tlo = JSON.parse($scope.Program.Tlo)
                if ($scope.Program.Logo) $scope.Program.Logo = JSON.parse($scope.Program.Logo)
                $scope.Program.Regulamin = $sce.trustAsHtml($scope.Program.Regulamin)
                $scope.data = {
                    to: $scope.Program.Kontakt_email
                }
            })
        }
        $scope.getPrizes = function() {
            ApplicationClientSvc.getPrizes({ firma: UrlData.firma, program: UrlData.program }).then(function(s) {
                $scope.prizes = s.data
                for (var i = 0; i < $scope.prizes.length; i++) {
                    $scope.prizes[i].Opis = $sce.trustAsHtml($scope.prizes[i].Opis)
                    if ($scope.prizes[i].Obraz)
                        $scope.prizes[i].Obraz_url = JSON.parse($scope.prizes[i].Obraz).Path.replace('/public', '') + '/' + JSON.parse($scope.prizes[i].Obraz).File
                    else
                        $scope.prizes[i].Obraz_url = ''
                }
                angular.element(document).ready(function(){
                    put_ellipsisses();
                });
            })
        }
        $scope.register = function() {
            if ($scope.user.Email && $scope.user.haslo && $scope.user.haslo2) {
                $scope.user.iscard = $scope.IsCard
                $scope.user.Firma = $scope.Firma.ID
                $scope.user.Id_programu = $scope.Program.Id
                UserClientSvc.register($scope.user)
                    .then(function(data) {
                        console.log(data.data.errors)
                        if (data.data.errors) {
                            ShowError(data.data.errors)
                        } else {
                            HideError()
                            ShowInfo(["Na podany adres email został wysłany link aktywacyjny. Aktywuj konto zanim zaczniesz korzystać z systemu."])
                        }
                    })
            }
            else{
                ShowError(["Wypełnij pola wymagane (oznaczone gwiazdką)"])
            }
        }
        $scope.Details()
        $scope.getPrizes()
    })