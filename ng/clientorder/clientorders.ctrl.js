angular.module('app')
    .controller('ClientOrdersCtrl', function($uibModal, $scope, ClientOrdersSvc, ClientPrizesSvc, $window, $location, $routeParams) {
        $scope.update = function() {
            if ($scope.order.Id)
                ClientOrdersSvc.update($scope.order).then(function(data) {
                    if (data.data == 'OK')
                        $window.location.href = `${UrlData.clientUrl}#/moje_zamowienia`
                    else
                        ShowError(data.data.errors)
                })
            else
                ClientOrdersSvc.create($scope.order).then(function(data) {
                    if (data.data == 'OK')
                        $window.location.href = `${UrlData.clientUrl}#/moje_zamowienia`
                    else
                        ShowError(data.data.errors)
                })
        }
        $scope.cancel = function(el) {
            if (el.Status == 'ANU')
                ShowDialog($uibModal, 'Anulowanie zamówienia', 'Zamówienie zostało anulowane i nie można go już anulować.', 1, false)
            else if (el.Status == 'ZRE')
                ShowDialog($uibModal, 'Anulowanie zamówienia', 'Zamówienie zostało zrealizowane i nie można go już anulować.', 1, false)
            else
                ShowDialog($uibModal, 'Anulowanie zamówienia', 'Czy na pewno chcesz anulować to zamówienie?', 0, false, function(data) {
                    if (data) {
                        let order = {
                            Id: el.Id,
                            Status: 'ANU'
                        }
                        ClientOrdersSvc.update(order).then(function(data) {
                            if (data.data == 'OK')
                                RefreshData()
                        })
                    }
                })
        }
        $scope.edit = function(el) {
            if (el.Status == 'ANU')
                ShowDialog($uibModal, 'Edycja zamówienia', 'Zamówienie zostało anulowane i nie można go już edytować.', 1, false)
            else if (el.Status == 'ZRE')
                ShowDialog($uibModal, 'Edycja zamówienia', 'Zamówienie zostało zrealizowane i nie można go już edytować.', 1, false)
            else
                $window.location.href = `${UrlData.clientUrl}#/nagrody/${el.Id_nagrody}/zamowienie?id=${el.Id}`
        }
        if ($routeParams.id) {
            ClientPrizesSvc.details($routeParams.id).then(function(data) {
                $scope.order = {
                    Nagroda: data.data.prize.Nazwa,
                    Cena: data.data.prize.Cena,
                    Id_nagrody: data.data.prize.Id
                }
            })
            if ($location.search().id) {
                ClientOrdersSvc.details($location.search().id).then(function(data) {
                    $scope.order = data.data.order
                })
            }
        } else
            RefreshData()
    })