angular.module('app')
    .service('ClientOrdersSvc', function($http) {
        this.fetch = function() {
            return $http.get('/api/clientorders')
        }
        this.details = function(id) {
            return $http.get('/api/clientorders/details/' + id)
        }
        this.create = function(order) {
            return $http.post('/api/clientorders/add', order)
        }
        this.update = function(order) {
            return $http.post('/api/clientorders/update', order)
        }
    })