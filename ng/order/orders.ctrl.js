angular.module('app')
    .controller('OrdersCtrl', function($uibModal, $scope, OrdersSvc, PrizesSvc, $window, $location, $routeParams) {
        $scope.update = function() {
            if ($scope.order.Id)
                OrdersSvc.update($scope.order).then(function(data) {
                    if (data.data == 'OK')
                        $window.location.href = '/client/#/moje_zamowienia'
                    else
                        ShowError(data.data.errors)
                })
            else
                OrdersSvc.create($scope.order).then(function(data) {
                    if (data.data == 'OK')
                        $window.location.href = '/client/#/moje_zamowienia'
                    else
                        ShowError(data.data.errors)
                })
        }
        $scope.remove = function(el) {
            ShowDialog($uibModal, 'Usuwanie zamówienia', 'Czy na pewno chcesz usunąć to zamówienie?', 0, false, function(data) {
                if (data) {
                    let order = {
                        Id: el.Id
                    }
                    OrdersSvc.remove(order).then(function(data) {
                        if (data.data == 'OK')
                            RefreshData()
                    })
                }
            })
        }
        $scope.confirm = function(el) {
            ShowDialog($uibModal, 'Realizacja zamówienia', 'Czy na pewno chcesz oznaczyć to zamówienie jako zrealizowane?', 0, false, function(data) {
                if (data) {
                    let order = {
                        Id: el.Id,
                        Status: 'ZRE'
                    }
                    OrdersSvc.update(order).then(function(data) {
                        if (data.data == 'OK')
                            RefreshData()
                    })
                }
            })
        }
        $scope.edit = function(el) {
            if (el.Status == 'ANU')
                ShowDialog($uibModal, 'Edycja zamówienia', 'Zamówienie zostało anulowane i nie można go już edytować.', 1, true)
            else if (el.Status == 'ZRE')
                ShowDialog($uibModal, 'Edycja zamówienia', 'Zamówienie zostało zrealizowane i nie można go już edytować.', 1, true)
            else
                $window.location.href = `/client/#/nagrody/${el.Id_nagrody}/zamowienie?id=${el.Id}`
        }
        if ($routeParams.id) {
            PrizesSvc.details($routeParams.id).then(function(data) {
                $scope.Nagroda = data.data.prize.Nazwa
                $scope.order = {
                    Id_nagrody: data.data.prize.Id
                }
            })
            if ($location.search().id) {
                OrdersSvc.details($location.search().id).then(function(data) {
                    $scope.order = data.data.order
                })
            }
        } else
            RefreshData()
    })