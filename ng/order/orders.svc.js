angular.module('app')
    .service('OrdersSvc', function($http) {
        this.fetch = function() {
            return $http.get('/api/orders')
        }
        this.details = function(id) {
            return $http.get('/api/orders/details/' + id)
        }
        this.create = function(order) {
            return $http.post('/api/orders/add', order)
        }
        this.update = function(order) {
            return $http.post('/api/orders/update', order)
        }
        this.remove = function(order) {
            return $http.post('/api/orders/remove', order)
        }
    })