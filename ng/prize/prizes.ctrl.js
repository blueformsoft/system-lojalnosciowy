angular.module('app')
    .controller('PrizesCtrl', function($scope, PrizesSvc, $window, $location, $sce, $routeParams) {
        $scope.edit = function(id) {
            PrizesSvc.edit(id).then(function(data) {
                $scope.prize = data.data.prize
                $scope.prize.Magazyn = $scope.prize.Magazyn == 1
                $scope.prize.Automat = $scope.prize.Automat == 1
                SummerNote('#prize_opis', 400)
                SummerNote('#prize_mail', 300)
                if (data.data.prize.Obraz)
                    $('#obraz_name').val(JSON.parse(data.data.prize.Obraz).OriginalName)
                $('#prize_opis').summernote('code', $scope.prize.Opis)
                if ($scope.prize.Mail) {
                    let mail = JSON.parse($scope.prize.Mail)
                    $scope.Mail_title = mail.Title
                    $('#prize_mail').summernote('code', mail.Body)
                }
            })
        }
        $scope.goToEdit = function(id) {
            $window.location.href = `/#/prizes/${id}`
        }
        $scope.goToMagazine = function(id) {
            $window.location.href = `/#/prizes/${id}/repository`
        }
        $scope.update = function() {
            if ($scope.prize && $scope.prize.Nazwa) {
                var formData
                if (document.getElementById('prize_obraz').files.length > 0) {
                    var formData = new FormData($('#frmAddEdit'))
                    $scope.prize.Opis = $('#prize_opis').summernote('code')
                    $scope.prize.Mail = JSON.stringify({
                        Title: $scope.Mail_title,
                        Body: $('#prize_mail').summernote('code')
                    })
                    formData.append("item", JSON.stringify($scope.prize))
                    formData.append("file", document.getElementById('prize_obraz').files[0])
                } else {
                    formData = $scope.prize
                }
                if ($scope.prize.Id) {
                    if (formData.Nazwa)
                        PrizesSvc.update(formData).then(function(result) {
                            if (result.data == 'OK')
                                $window.location.href = '/#/prizes'
                            else
                                ShowError(result.data.errors)
                        })
                    else
                        PrizesSvc.upload(formData).then(function(result) {
                            if (result.data == 'OK')
                                $window.location.href = '/#/prizes'
                            else
                                ShowError(result.data.errors)
                        })
                } else {
                    PrizesSvc.create($scope.prize).then(function(result) {
                        if (result.data == 'Created')
                            $window.location.href = '/#/prizes'
                        else
                            ShowError(result.data.errors)
                    })
                }
            } else
                ShowError(["Pole nazwa jest wymagane"])
        }
        $scope.fetch = function() {
            PrizesSvc.fetch().then(function(s) {
                $scope.prizes = s.data
                for (var i = 0; i < $scope.prizes.length; i++) {
                    $scope.prizes[i].Opis = $sce.trustAsHtml($scope.prizes[i].Opis)
                    if ($scope.prizes[i].Obraz)
                        $scope.prizes[i].Obraz_url = JSON.parse($scope.prizes[i].Obraz).Path.replace('/public', '') + '/' + JSON.parse($scope.prizes[i].Obraz).File
                    else
                        $scope.prizes[i].Obraz_url = ''
                }
            })
        }
        if ($routeParams.id) {
            $scope.edit($routeParams.id)
        } else if ($location.$$path == '/prizes/add') {
            SummerNote('#prize_opis', 400)
            SummerNote('#prize_mail', 300)
        } else {
            $scope.fetch()
        }
    })