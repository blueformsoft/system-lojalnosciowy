angular.module('app')
    .service('PrizesSvc', function($http) {
        this.fetch = function() {
            return $http.get('/api/prizes')
        }
        this.edit = function(id) {
            return $http.get('/api/prizes/edit?id=' + id)
        }
        this.create = function(prize) {
            return $http.post('/api/prizes/add', prize)
        }
        this.upload = function(prize) {
            return $http.post('/api/prizes/upload', prize, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })
        }
        this.update = function(prize) {
            return $http.post('/api/prizes/update', prize)
        }
        this.editRepository = function(id) {
            return $http.get('/api/prizes/repository/edit?id=' + id)
        }
        this.confirmRepository = function(data) {
            return $http.post('/api/prizes/repository/confirm', data)
        }
        this.removeRepository = function(data) {
            return $http.post('/api/prizes/repository/remove', data)
        }
        this.uploadRepository = function(data) {
            return $http.post('/api/prizes/repository/upload', data, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })
        }
        this.updateRepository = function(data) {
            return $http.post('/api/prizes/repository/update', data)
        }
    })