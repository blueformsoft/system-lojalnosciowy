angular.module('app')
    .controller('RepositoryAddEditCtrl', function($uibModalInstance, $scope, nagroda, element, PrizesSvc, $window, $location) {

        $scope.cancel = function() {
            $uibModalInstance.close(false)
        }
        $scope.update = function() {
            if ($scope.item.Zalacznik) {
                var formData = new FormData($('#frmAddItem'))
                formData.append("item", angular.toJson($scope.item))
                formData.append("file", document.getElementById('item_Adres').files[0])
                PrizesSvc.uploadRepository(formData).then(function() {
                    $uibModalInstance.close(false)
                })
            } else {
                PrizesSvc.updateRepository($scope.item).then(function(data) {
                    if (data.data == 'OK')
                        $uibModalInstance.close(false)
                })
            }
        }
        $scope.item = {
            Id_nagrody: nagroda.Id
        }
        if (element) {
            $scope.item.Id = element.Id
            $scope.item.Opis = element.Opis
            $scope.item.Zalacznik = element.Zalacznik == 1
        }
    })