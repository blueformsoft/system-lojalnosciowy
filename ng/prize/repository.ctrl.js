angular.module('app')
    .controller('RepositoryCtrl', function($uibModal, $scope, PrizesSvc, $window, $location, $routeParams) {
        $scope.repository = function(id) {
            PrizesSvc.edit(id).then(function(data) {
                $scope.Nagroda = data.data.prize.Nazwa
                RefreshData(id)
            })
        }
        $scope.showAddItem = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'administration/prize/repositoryAddEditDialog.html',
                controller: 'RepositoryAddEditCtrl',
                controllerAs: '$ctrl',
                resolve: {
                    nagroda: function() {
                        return { Nazwa: $scope.Nazwa, Id: $routeParams.id }
                    },
                    element: function() {
                        return null
                    }
                }
            })

            modalInstance.result.then(function() {
                RefreshData($routeParams.id)
            })
        }
        $scope.removeRepository = function(el) {
            if (el.Wydane)
                ShowDialog($uibModal, 'Magazyn - usuwanie', 'Element został już wydany. Jego usunięcie nie jest możliwe.', 1, false)
            else
                ShowDialog($uibModal, 'Magazyn - usuwanie', 'Czy na pewno chcesz usunąć ten element?', 0, false, function(data) {
                    if (data) {
                        PrizesSvc.removeRepository(el).then(function(data) {
                            RefreshData($routeParams.id)
                        })
                    }
                })
        }
        $scope.confirm = function(el) {
            if (el.Wydane)
                ShowDialog($uibModal, 'Magazyn - wydawanie', 'Element został już wcześniej wydany.', 1, false)
            else {
                ShowDialog($uibModal, 'Magazyn - wydawanie', 'Czy na pewno chcesz oznaczyć ten element jako wydany?', 0, false, function(data) {
                    if (data)
                        PrizesSvc.confirmRepository({ Id: el.Id }).then(function(data) {
                            if (data.data == 'OK')
                                RefreshData($routeParams.id)
                        })
                })
            }
        }
        $scope.showEditItem = function(el) {

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'administration/prize/repositoryAddEditDialog.html',
                controller: 'RepositoryAddEditCtrl',
                controllerAs: '$ctrl',
                resolve: {
                    nagroda: function() {
                        return { Nazwa: $scope.Nazwa, Id: $routeParams.id }
                    },
                    element: function() {
                        return el
                    }
                }
            })

            modalInstance.result.then(function() {
                RefreshData($routeParams.id)
            })
        }
        $scope.repository($routeParams.id)
    })