angular.module('app')
    .service('ClientPrizesSvc', function($http) {
        this.fetch = function() {
            return $http.get('/api/clientprizes')
        }
        this.details = function(id) {
            return $http.get('/api/clientprizes/details/' + id)
        }
        this.create = function(prize) {
            return $http.post('/api/clientprizes/add', prize)
        }
        this.update = function(prize) {
            return $http.post('/api/clientprizes/update', prize)
        }

    })