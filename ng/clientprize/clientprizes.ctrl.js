angular.module('app')
    .controller('ClientPrizesCtrl', function($uibModal, $scope, ClientPrizesSvc, $window, $location, $routeParams, $sce) {
        $scope.details = function(id) {
            ClientPrizesSvc.details(id).then(function(data) {
                $scope.prize = data.data.prize
                $scope.prize.Obraz_url = JSON.parse($scope.prize.Obraz).Path.replace('/public', '') + '/' + JSON.parse($scope.prize.Obraz).File
                $scope.prize.Opis_skrot = $sce.trustAsHtml($scope.prize.Opis_skrot)
                $scope.prize.Opis = $sce.trustAsHtml($scope.prize.Opis)
                $scope.repository = {
                    Ilosc: data.data.repository.Ilosc
                }
            })
        }
        $scope.ShowButtons = true
        $scope.goToDetails = function(id) {
            $window.location.href = UrlData.clientUrl + '#/nagrody/' + id
        }
        $scope.goToOrder = function(id) {
            if ($scope.$parent.info_session.Punkty >= $scope.prize.Cena)
                $window.location.href = `${UrlData.clientUrl}#/nagrody/${id}/zamowienie`
            else
                ShowDialog($uibModal, 'Brak punktów', 'Masz za mało punktów, aby zamówić tą nagrodę', 1, false)
        }
        $scope.fetch = function() {
            ClientPrizesSvc.fetch().then(function(s) {
                $scope.prizes = s.data
                for (var i = 0; i < $scope.prizes.length; i++) {
                    $scope.prizes[i].Opis = $sce.trustAsHtml($scope.prizes[i].Opis)
                    if ($scope.prizes[i].Obraz)
                        $scope.prizes[i].Obraz_url = JSON.parse($scope.prizes[i].Obraz).Path.replace('/public', '') + '/' + JSON.parse($scope.prizes[i].Obraz).File
                    else
                        $scope.prizes[i].Obraz_url = ''
                }
            })
        }
        if ($routeParams.id) {
            $scope.$parent.RefreshSessionData()
            $scope.details($routeParams.id)
        } else {
            $scope.fetch()
        }
    })