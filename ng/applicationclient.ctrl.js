angular.module('app')
    .controller('ApplicationClientCtrl', function($scope, $window, $location, UserClientSvc, ApplicationClientSvc) {
        $scope.RefreshSessionData = function() {
            UserClientSvc.getSessionData().then(function(data) {
                $scope.info_session = data.data.info_session
            })
        }
        $scope.wyloguj = function() {
            UserClientSvc.logout()
                .then(function() {
                    $window.location.reload()
                })
        }
        $scope.Details = function() {
            let parts = $location.$$absUrl.split('#')[0].split('/')
            let firma = parts[4]
            let program = parts[5]
            ApplicationClientSvc.details(firma, program).then(function(data) {
                $scope.Firma = data.data.Firma
                $scope.Program = data.data.Program
                if ($scope.Program.Tlo) $scope.Program.Tlo = JSON.parse($scope.Program.Tlo)
                if ($scope.Program.Logo) $scope.Program.Logo = JSON.parse($scope.Program.Logo)
            })
        }
        $scope.goToOrders = function() {
            window.location = `${UrlData.clientUrl}#/moje_zamowienia`
        }
        $scope.goToPrizes = function() {
            window.location = `${UrlData.clientUrl}#/nagrody`
        }
        $scope.goToMain = function() {
            window.location = `${UrlData.clientUrl}#/`
        }
        $scope.goToSettings = function() {
            window.location = `${UrlData.clientUrl}#/settings`
        }
        $scope.Details()
        $scope.RefreshSessionData()
    })