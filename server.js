var express = require('express');
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser')
var app = express();

app.use(cookieParser())

app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
var db = require('./db')
var helmet = require('helmet')
app.use(helmet({
    contentSecurityPolicy: false,
    dnsPrefetchControl: true,
    expectCt: false,
    featurePolicy: false,
    frameguard: true,
    hidePoweredBy: true,
    hsts: true,
    ieNoOpen: true,
    noCache: false,
    noSniff: false,
    permittedCrossDomainPolicies: false,
    referrerPolicy: false,
    xssFilter: true
}))
var session = require('express-session')
var MySQLStore = require('express-mysql-session')(session);
var sessionStore = new MySQLStore({
    createDatabaseTable: true,
    schema: {
        tableName: 'sessions',
        columnNames: {
            session_id: 'session_id',
            expires: 'expires',
            data: 'data'
        }
    }
}, db)
app.set('trust proxy', 1)
app.use(session({
    key: 'sid',
    secret: '1=)!!ÓA>msw5|pdt-?Awlch:<[/7)A[Ź',
    store: sessionStore,
    proxy: true,
    cookie: {
        secure: true,
        httpOnly: true,
        // domain: 'localhost'
        domain: '.firmotron.pl'
    }
}))
app.use(require('./controllers/static'))
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Methods', 'POST')
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
    res.setHeader('Access-Control-Credentials', true)
    next()
})


app.use('/api/sessions', require('./controllers/api/sessions'))
app.use('/api/points', require('./controllers/api/points'))
app.use('/api/clientsessions', require('./controllers/api/clientsessions'))
app.use('/api/users', require('./controllers/api/users'))
app.use('/api/clients', require('./controllers/api/clients'))
app.use('/api/panelclient', require('./controllers/api/panelclient'))
app.use('/api/settings', require('./controllers/api/settings'))
app.use('/api/programs', require('./controllers/api/programs'))
app.use('/api/prizes', require('./controllers/api/prizes'))
app.use('/api/orders', require('./controllers/api/orders'))
app.use('/api/userclient', require('./controllers/api/userclient'))
app.use('/api/clientprizes', require('./controllers/api/clientprizes'))
app.use('/api/clientorders', require('./controllers/api/clientorders'))
app.use('/api/clientsettings', require('./controllers/api/clientsettings'))
app.use('/api/clientapp', require('./controllers/api/clientapp'))


var server = app.listen(5004, function() {
    console.log('Serwer rozpoczął nasłuchiwanie na porcie numer', 5004);
})


