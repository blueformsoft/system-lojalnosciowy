$(function(){
  var numer = '1'
  var template_data = {
    D: moment(data_wyst).format('D'),
    DD: moment(data_wyst).format('DD'),
    M: moment(data_wyst).format('M'),
    MM: moment(data_wyst).format('MM'),
    MMM: moment(data_wyst).format('MMM'),
    MMMM: moment(data_wyst).format('MMMM'),
    YY: moment(data_wyst).format('YY'),
    YYYY: moment(data_wyst).format('YYYY'),
    Q: moment(data_wyst).format('Q')
  }
  if (numer.length == 1) {
    template_data.N = numer
    template_data.NN = '0'+numer
    template_data.NNN = '00'+numer
    template_data.NNNN = '000'+numer
  }
  else if (numer.length == 2) {
    template_data.N = numer
    template_data.NN = numer
    template_data.NNN = '0'+numer
    template_data.NNNN = '00'+numer
  }
  else if (numer.length == 3) {
    template_data.N = numer
    template_data.NN = numer
    template_data.NNN = numer
    template_data.NNNN = '0'+numer
  }
  function counting_preview(obj, obj_prev, param) {
    var val = $('#'+obj).val()
    $('#'+obj).val(val+param)
    $('#'+obj_prev).val(template(obj, template_data))
  }

  $('#edit_counting_dialog').dialog({
    autoOpen: false,
    width: 600,
    buttons: {
      'Potwierdź': function() {
        var data = $('#edit_counting_form').serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value
            return obj
        }, {})
        data.ID = $('#edit_counting_dialog').data('id')
        $.post('/edit_counting', data, function(data) {
          window.location.reload()
        })
      },
      'Anuluj': function() {
        $(this).dialog('close')
      }
    }
  })
  $('.edit_counting').click(function(){
    var data = $(this).data('info')
    $('#edit_counting_form input[name=nazwa]').val(data.Nazwa)
    $('#edit_counting_form input[name=maska]').val(data.Maska)
    $('#edit_counting_form input[name=numer_start]').val(data.Numer_start)
    $('#edit_counting_form input[type=radio][value='+data.Reset+']').prop('checked', true)
    $('#edit_counting_form input[value='+data.Typ_faktury+']').prop('checked', true)
    $('#maska_podglad_e').val(template('maska_e', template_data))
    $('#edit_counting_dialog').data('id', data.ID)
    $('#edit_counting_dialog').dialog('open')
  })
  $('#delete_counting_dialog').dialog({
    autoOpen: false,
    width: 600,
    buttons: {
      'Potwierdź': function(){
        var id = $(this).data('id')
        $.post('/delete_counting', {id: id}, function(){
          window.location.reload()
        })
      },
      'Anuluj': function(){
        $(this).dialog('close')
      }
    }
  })
  $('.delete_counting').click(function(){
    var id = $(this).data('id')
    $('#delete_counting_dialog').data('id', id).dialog('open')
  })
  $('#maska_e').keyup(function() {
    $('#maska_podglad_e').val(template('maska_e', template_data))
  })
  var menu = [{
        name: 'Numer faktury',
        title: 'Dodaj numer faktury',
        subMenu: [{
          name: '1',
          fun: function() {
            counting_preview('maska_e', 'maska_podglad_e', '{N}')
          }
        }, {
          name: '01',
          fun: function() {
            counting_preview('maska_e', 'maska_podglad_e', '{NN}')
          }
        }, {
          name: '001',
          fun: function() {
            counting_preview('maska_e', 'maska_podglad_e', '{NNN}')
          }
        }, {
          name: '0001',
          fun: function() {
            counting_preview('maska_e', 'maska_podglad_e', '{NNNN}')
          }
        }]
    }, {
        name: 'Dzień',
        title: 'Dodaj dzień',
        subMenu: [{
          name: '1',
          fun: function() {
            counting_preview('maska_e', 'maska_podglad_e', '{D}')
          }
        }, {
          name: '01',
          fun: function() {
            counting_preview('maska_e', 'maska_podglad_e', '{DD}')
          }
        }]
    }, {
        name: 'Miesiąc',
        title: 'Dodaj miesiąc',
        subMenu: [{
          name: '1',
          fun: function() {
            counting_preview('maska_e', 'maska_podglad_e', '{M}')
          }
        }, {
          name: '01',
          fun: function() {
            counting_preview('maska_e', 'maska_podglad_e', '{MM}')
          }
        }, {
          name: 'sty',
          fun: function() {
            counting_preview('maska_e', 'maska_podglad_e', '{MMM}')
          }
        }, {
          name: 'styczeń',
          fun: function() {
            counting_preview('maska_e', 'maska_podglad_e', '{MMMM}')
          }
        }]
    }, {
        name: 'Rok',
        title: 'Dodaj rok',
        subMenu: [{
          name: '17',
          fun: function() {
            counting_preview('maska_e', 'maska_podglad_e', '{YY}')
          }
        }, {
          name: '2017',
          fun: function() {
            counting_preview('maska_e', 'maska_podglad_e', '{YYYY}')
          }
        }]
    }]
  $('#maska_e').contextMenu(menu, {triggerOn: 'contextmenu'})
  $('#maska').keyup(function() {
    $('#maska_podglad').val(template('maska', template_data))
  })
  menu = [{
        name: 'Numer faktury',
        title: 'Dodaj numer faktury',
        subMenu: [{
          name: '1',
          fun: function() {
            counting_preview('maska', 'maska_podglad', '{N}')
          }
        }, {
          name: '01',
          fun: function() {
            counting_preview('maska', 'maska_podglad', '{NN}')
          }
        }, {
          name: '001',
          fun: function() {
            counting_preview('maska', 'maska_podglad', '{NNN}')
          }
        }, {
          name: '0001',
          fun: function() {
            counting_preview('maska', 'maska_podglad', '{NNNN}')
          }
        }]
    }, {
        name: 'Dzień',
        title: 'Dodaj dzień',
        subMenu: [{
          name: '1',
          fun: function() {
            counting_preview('maska', 'maska_podglad', '{D}')
          }
        }, {
          name: '01',
          fun: function() {
            counting_preview('maska', 'maska_podglad', '{DD}')
          }
        }]
    }, {
        name: 'Miesiąc',
        title: 'Dodaj miesiąc',
        subMenu: [{
          name: '1',
          fun: function() {
            counting_preview('maska', 'maska_podglad', '{M}')
          }
        }, {
          name: '01',
          fun: function() {
            counting_preview('maska', 'maska_podglad', '{MM}')
          }
        }, {
          name: 'sty',
          fun: function() {
            counting_preview('maska', 'maska_podglad', '{MMM}')
          }
        }, {
          name: 'styczeń',
          fun: function() {
            counting_preview('maska', 'maska_podglad', '{MMMM}')
          }
        }]
    }, {
        name: 'Rok',
        title: 'Dodaj rok',
        subMenu: [{
          name: '17',
          fun: function() {
            counting_preview('maska', 'maska_podglad', '{YY}')
          }
        }, {
          name: '2017',
          fun: function() {
            counting_preview('maska', 'maska_podglad', '{YYYY}')
          }
        }]
    }]

  $('#maska').contextMenu(menu, {triggerOn: 'contextmenu'})
  $('#add_counting_dialog').dialog({
    autoOpen: false,
    width: 600,
    buttons: {
      'Potwierdź': function() {
        var data = $('#add_counting_form').serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value
            return obj
        }, {})
        $.post('/new_counting', data, function(data) {
          window.location.reload()
        })
      },
      'Anuluj': function() {
        $(this).dialog('close')
      }
    }
  })
  $('#add_counting').click(function(){
    $('#add_counting_dialog').dialog('open')
  })
  var data_wyst = new Date()

})
function setDefaultCounting(typ, id) {
  $.post('default_counting', {id: id, typ: typ}, function(data) {
    window.location.reload()
  })
}
function template( templateid, data ){
    return $('#'+templateid).val()
      .replace(
        /{(\w*)}/g, // or /{(\w*)}/g for "{this} instead of %this%"
        function( m, key ){
          return data.hasOwnProperty( key ) ? data[ key ] : ""
        }
      )
}
