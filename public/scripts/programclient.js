function format(d) {
    // `d` is the original data object for the row
    return '<div class="slider"><table class="child-table col-xs-3">' +
        '<tr class="child-row-flag">' +
        '<td class="removefv"><span class="glyphicon glyphicon-remove"></span> &nbsp; usuń</td>' +
        '<td class="editfv"><span class="glyphicon glyphicon-search"></span> &nbsp; edytuj</td>' +
        '<td class="confirmfv"><span class="glyphicon glyphicon-ok"></span> &nbsp; zatwierdź</td>' +
        '</tr>' +
        '</table></div>'
}

function GetIdProgram() {
    return $('li[class="ng-scope active"]').data('id')
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};
$.extend({
    getUrlVars: function() {
        var vars = [],
            hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function(name) {
        return $.getUrlVars()[name];
    }
});
var table = null
$(function() {

    $('#program_client_table tbody').on('click', '.removefv', function() {
        var row = table.row($(this).closest('table').parent().parent().parent().prev())
        RemoveFV(row.data())
    })
    $('#program_client_table tbody').on('click', '.editfv', function() {
        var row = table.row($(this).closest('table').parent().parent().parent().prev())
        EditFV(row.data())
    })
    $('#program_client_table tbody').on('click', '.confirmfv', function() {
        var row = table.row($(this).closest('table').parent().parent().parent().prev())
        ConfirmFV(row.data())
    })
    $('#program_client_table tbody').on('click', 'tr', function() {
        var tr = $(this);
        if (!tr.hasClass('no-padding') && !tr.hasClass('child-row-flag') && tr.children().length > 1) {
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                $('div.slider', row.child()).slideUp(function() {
                    row.child.hide();
                    tr.removeClass('shown');
                });
            } else {
                // Open this row
                var shown_tr = $('.shown')
                var shown_row = table.row(shown_tr)
                    // var shown = table.row($('.shown').next('.slider'))
                $('div.slider', shown_row.child()).slideUp(function() {
                    shown_row.child.hide()
                    shown_tr.removeClass('shown')
                })
                row.child(format(row.data()), 'no-padding').show();
                tr.addClass('shown');
                $('div.slider', row.child()).slideDown();
            }
        }
    })
})
var EditFV = function(el) {
    angular.element($('.container')).scope().showEditDocument(el)
}
var RemoveFV = function(el) {
    angular.element($('.container')).scope().removeDocument(el)
}
var ConfirmFV = function(el) {
    angular.element($('.container')).scope().confirmDocument(el)
}

function RefreshAccount(client, isclient) {
    if (!client) client = $.getUrlVar('id')
    let url = 'api/clients/get_account_data?id=' + client
    if (isclient) url = '../../api/panelclient/get_account_data?id=' + client
    if (table) {
        table.ajax.url(url)
        table.ajax.reload()
        return
    }

    table = $('#program_client_table').DataTable({
        columnDefs: [
            { type: 'natural', targets: '_all' }
        ],
        ajax: {
            url: url,
        },
        rowCallback: function(row, data, index) {
            if (data.Zatwierdzone == 0)
                $(row).find('td:eq(5)').css('background-color', 'red')
            else if (data.Zatwierdzone == 1)
                $(row).find('td:eq(5)').css('background-color', 'green')
            if(data.Parent)
                $(row).find('td:eq(0)').css('background-color', 'var(--light-color)')
            else
                $(row).find('td:eq(0)').css('background-color', 'var(--dark-color)')
        },
        columns: [
            { 
                data: 'Parent',
                "render": function(data) {
                    return data || "WŁASNE"
                }
            },
            {
                data: 'Data',
                "render": function(data) {
                    return moment(data).format("YYYY-MM-DD")
                }
            },
            {
                data: 'Opis'
            },
            {
                data: 'Kwota'
            },
            { data: 'Ilosc' },
            {
                data: 'Zatwierdzone',
                "render": function(data) {
                    if (data == 1)
                        return 'TAK'
                    else
                        return 'NIE'
                }
            }
        ],
        footerCallback: function(row, data, start, end, display) {
            var api = this.api();

            api.columns('.sum', {
                page: 'current'
            }).every(function(col) {
                var sum = this
                    .data()
                    .reduce(function(a, b) {
                        var x = parseFloat(a) || 0;
                        var y = parseFloat(b) || 0;
                        return x + y;
                    }, 0);
                if (col == 3)
                    $(this.footer()).html(sum.toFixed(2) + ' zł');
                else if (col == 4)
                    $(this.footer()).html(sum.toFixed() + ' punktów');
            });
        }
    })
}