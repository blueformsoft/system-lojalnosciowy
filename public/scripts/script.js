var UrlData = null
$(document).ready(function() {
    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    })
    $('body').css('min-height', window.innerHeight + 'px');
    $(document).on('change', ':file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    // We can watch for our custom `fileselect` event like this
    $(document).on('fileselect', ':file', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });
    let parts = window.location.href.split('#')[0].split('/')
    UrlData = {
        firma: parts[4],
        program: parts[5],
        clientUrl: '/client/' + parts[4] + '/' + parts[5]
    }
});
$(window).resize(function() {
    $('body').css('min-height', window.innerHeight + 'px');
});
if ($.fn.dataTable)
    $.extend(true, $.fn.dataTable.defaults, {
        // "ordering": false,
        language: {
            lengthMenu: 'Wyświetl _MENU_ wyników na stronie',
            emptyTable: 'Brak wyników',
            info: '_START_ - _END_ z _TOTAL_ wyników',
            infoEmpty: '0 wyników',
            infoFiltered: '(Przefiltrowane z _MAX_ wyników)',
            loadingRecords: 'Ładowanie...',
            processing: 'Przetwarzanie danych...',
            search: 'Wyszukaj',
            zeroRecords: 'Brak wyników',
            paginate: {
                first: 'Pierwsza',
                last: 'Ostatnia',
                next: 'Następna',
                previous: 'Poprzednia'
            },
            aria: {
                sortAscending: ': sortuj rosnąco',
                sortDescending: ': sortuj malejąco'
            }
        }
    })
var ShowDialogError = function(messages) {
    var html = '<div class="alert alert-danger"><ul>'
    if (messages.length == 1)
        html = '<div class="alert alert-danger">' + messages[0] + '</div>'
    else {
        for (var i = 0; i < messages.length; i++) {
            html += '<li>' + messages[i] + '</li>'
        }
        html += '</ul></div>'
    }
    $('.dialogerrors').html(html)
}
var ActivateMail = function(){
    angular.element($('.container')).scope().ActivateMail()
}
var HideDialogError = function() {
    $('.dialogerrors').html('')
}
var ShowError = function(messages) {
    var html = '<div class="alert alert-danger"><ul>'
    if (messages.length == 1)
        html = '<div class="alert alert-danger">' + messages[0] + '</div>'
    else {
        for (var i = 0; i < messages.length; i++) {
            html += '<li>' + messages[i] + '</li>'
        }
        html += '</ul>'
        html += '</div>'
    }
    $('.errors').html(html)
}
var HideError = function() {
    $('.errors').html('')
}
var HideInfo = function(){
    $('.infos').html('')
}
var ShowInfo = function(messages) {
    HideError()
    var html = '<div class="alert alert-info"><ul>'
    if (messages.length == 1)
        html = '<div class="alert alert-info">' + messages[0] + '</div>'
    else {
        for (var i = 0; i < messages.length; i++) {
            html += '<li>' + messages[i] + '</li>'
        }
        html += '</ul></div>'
    }
    $('.infos').html(html)
}
var SummerNote = function(selector, customHeight) {
    let Height = 100
    if (customHeight) Height = customHeight
    $(selector).summernote({
        height: Height,
        lang: 'pl-PL',
        dialogsInBody: true,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'hr']],
            ['view', ['fullscreen']],
            ['help', ['help']]
        ]
    })
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var ShowDialog = function(modal, headerText, bodyText, mode, desc, callback) {
    var modalInstance = modal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: '/dialog.html',
        controller: 'DialogCtrl',
        controllerAs: '$ctrl',
        resolve: {
            dialog: function() {
                return { header: headerText, body: bodyText, desc: desc }
            },
            mode: function() {
                return mode
            }
        }
    })
    if (callback)
        modalInstance.result.then(callback)
}