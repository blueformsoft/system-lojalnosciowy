function format(d) {
    // `d` is the original data object for the row
    return '<div class="slider"><table class="child-table col-xs-3">' +
        '<tr class="child-row-flag">' +
        '<td class="cancel"><span class="glyphicon glyphicon-remove"></span> &nbsp; anuluj</td>' +
        '<td class="edit"><span class="glyphicon glyphicon-search"></span> &nbsp; edytuj</td>' +
        '</tr>' +
        '</table></div>'
}

function Cancel(el) {
    angular.element($('.container')).scope().cancel(el)
}

function Edit(el) {
    angular.element($('.container')).scope().edit(el)
}
var table = null
$(function() {

    $('#order_table tbody').on('click', '.cancel', function() {
        var row = table.row($(this).closest('table').parent().parent().parent().prev())
        Cancel(row.data())
    })
    $('#order_table tbody').on('click', '.edit', function() {
        var row = table.row($(this).closest('table').parent().parent().parent().prev())
        Edit(row.data())
    })
    $('#order_table tbody').on('click', 'tr', function() {
        var tr = $(this);
        if (!tr.hasClass('no-padding') && !tr.hasClass('child-row-flag') && tr.children().length > 1) {
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                $('div.slider', row.child()).slideUp(function() {
                    row.child.hide();
                    tr.removeClass('shown');
                });
            } else {
                // Open this row
                var shown_tr = $('.shown')
                var shown_row = table.row(shown_tr)
                    // var shown = table.row($('.shown').next('.slider'))
                $('div.slider', shown_row.child()).slideUp(function() {
                    shown_row.child.hide()
                    shown_tr.removeClass('shown')
                })
                row.child(format(row.data()), 'no-padding').show();
                tr.addClass('shown');
                $('div.slider', row.child()).slideDown();
            }
        }
    })
})

var RefreshData = function() {
    if (table) {
        table.ajax.url('/api/clientorders/get')
        table.ajax.reload()
        return
    }
    table = $('#order_table').DataTable({
        columnDefs: [
            { type: 'natural', targets: '_all' }
        ],
        ajax: {
            url: '/api/clientorders/get',
            dataSrc: ''
        },
        rowCallback: function(row, data, index) {
            if (data.Status == 'ZAM')
                $(row).find('td:eq(3)').css('background-color', 'orange')
            else if (data.Status == 'ZRE')
                $(row).find('td:eq(3)').css('background-color', 'green')
            else
                $(row).find('td:eq(3)').css('background-color', 'red')
        },
        columns: [
            { data: 'Nagroda' },
            { data: 'Wiadomosc' },
            { data: 'Wartosc' },
            {
                data: 'Status',
                "render": function(data) {
                    if (data == 'ZAM')
                        return 'OCZEKUJE NA REALIZACJĘ'
                    else if (data == 'ZRE')
                        return 'ZREALIZOWANE'
                    else
                        return 'ANULOWANE'
                }
            }
        ]
    })
}