function format(d) {
    // `d` is the original data object for the row
    return '<div class="slider"><table class="child-table col-xs-3">' +
        '<tr class="child-row-flag">' +
        '<td><a href="#/programs/edit?id=' + d.Id + '"><span class="glyphicon glyphicon-search"></span> &nbsp; edycja </a></td>'
        //wysyłanie na maila
        // '<td><a href="new_invoice?client='+d.ID+'"><img src="./public/images/faktura.png"/> &nbsp; wystaw fakturę</a></td>'+


    // '<td onclick="remove_service(this)" data-info="'+d.ID+'"><img src="./public/images/kosz.png"/> &nbsp; drukuj </td>'+
    '</tr>' +
    '</table></div>';
}
$(function() {
    var table = null
    table = $('#program_table').DataTable({
        columnDefs: [
            { type: 'natural', targets: '_all' }
        ],
        ajax: {
            url: 'api/programs/list',
            dataSrc: ''
        },
        columns: [
            { data: 'Nazwa' },
            { data: 'Opis' }
        ]
    })


    $('#program_table tbody').on('click', 'tr', function() {
        var tr = $(this);
        if (!tr.hasClass('no-padding') && !tr.hasClass('child-row-flag') && tr.children().length > 1) {
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                $('div.slider', row.child()).slideUp(function() {
                    row.child.hide();
                    tr.removeClass('shown');
                });
            } else {
                // Open this row
                var shown_tr = $('.shown')
                var shown_row = table.row(shown_tr)
                    // var shown = table.row($('.shown').next('.slider'))
                $('div.slider', shown_row.child()).slideUp(function() {
                    shown_row.child.hide()
                    shown_tr.removeClass('shown')
                })
                row.child(format(row.data()), 'no-padding').show();
                tr.addClass('shown');
                $('div.slider', row.child()).slideDown();
            }
        }
    })
})