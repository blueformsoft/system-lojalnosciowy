window.addEventListener("load", function() {
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#252e39"
            },
            "button": {
                "background": "#2884c9",
                "text": "#fff"
            }
        },
        "content": {
            "message": "Ten serwis wykorzystuje pliki cookies. Korzystanie z witryny oznacza zgodę na ich zapis lub odczyt wg ustawień przeglądarki.",
            "dismiss": "OK",
            "link": "Dowiedz się więcej",
            "href": "http://cookiealert.sruu.pl/o-ciasteczkach"
        }
    })
});