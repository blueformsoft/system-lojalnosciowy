//redo ellipsis when window resizes
var re_ellipsis_timeout;
$(function(){
    $(window).on('resize', function() {
        clearTimeout(re_ellipsis_timeout);
        put_ellipsisses();
    });
})


//the main function
function put_ellipsisses(){
    $(".js_ellipsis").each(function(){
        //remember initial text to be able to regrow when space increases
        var object_data=$(this).data();
        if(typeof object_data.oldtext != "undefined"){
            $(this).text(object_data.oldtext);
        }else{
            object_data.oldtext = jQuery(this).text();
            $(this).data(object_data);
        }

        //truncate and ellipsis
        var clientHeight = this.clientHeight;
        var maxturns=100; var countturns=0;
        while (this.scrollHeight > clientHeight && countturns < maxturns) {
            countturns++;
            $(this).text(function (index, text) {
                return text.replace(/\W*\s(\S)*$/, '...');
            });
        }
    });
}