function format(d) {
    // `d` is the original data object for the row
    let slider = '<div class="slider"><table class="child-table col-xs-3">' +
        '<tr class="child-row-flag">' +
        '<td data-info="' + d.Id + '"><a href="#/clients/panel?id=' + d.Id + '"><span class="glyphicon glyphicon-fire"></span> &nbsp; panel klienta </a></td>'
    if (!d.Data_aktywacji)
        slider += '<td><a onclick="Activate(' + d.Id + ')"><span class="glyphicon glyphicon-check"></span> &nbsp; aktywuj </a></td>'
    slider += '</tr></table></div>';
        return slider
}
var Activate = function(id) {
    angular.element($('.container')).scope().Activate(id)
}
var cardtable = null
$(function() {
    cardtable = $('#client_table').DataTable({
        order: [],
        columnDefs: [
            { type: 'natural', targets: '_all' }
        ],
        ajax: {
            url: 'api/clients/list',
            dataSrc: ''
        },
        rowCallback: function(row, data, index) {
            if (!data.Data_aktywacji)
                $(row).find('td:eq(1)').addClass('unactive-code')
            else
                $(row).find('td:eq(1)').addClass('active-code')
        },
        columns: [
            { data: 'Numer' },
            {
                data: 'Data_aktywacji',
                "render": function(data) {
                    if (!data)
                        return 'Nieaktywny'
                    else
                        return moment(data).format('DD.MM.YYYY HH:mm')
                }
            }
        ]
    })


    $('#client_table tbody').on('click', 'tr', function() {
        var tr = $(this);
        if (!tr.hasClass('no-padding') && !tr.hasClass('child-row-flag') && tr.children().length > 1) {
            var row = cardtable.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                $('div.slider', row.child()).slideUp(function() {
                    row.child.hide();
                    tr.removeClass('shown');
                });
            } else {
                // Open this row
                var shown_tr = $('.shown')
                var shown_row = cardtable.row(shown_tr)
                    // var shown = table.row($('.shown').next('.slider'))
                $('div.slider', shown_row.child()).slideUp(function() {
                    shown_row.child.hide()
                    shown_tr.removeClass('shown')
                })
                row.child(format(row.data()), 'no-padding').show();
                tr.addClass('shown');
                $('div.slider', row.child()).slideDown();
            }
        }
    })
})