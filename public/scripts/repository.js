function format(d) {
    // `d` is the original data object for the row
    return '<div class="slider"><table class="child-table col-xs-3">' +
        '<tr class="child-row-flag">' +
        '<td class="confirm"><span class="glyphicon glyphicon-ok"></span> &nbsp; oznacz jako wydane</td>' +
        '<td class="remove"><span class="glyphicon glyphicon-remove"></span> &nbsp; usuń</td>' +
        '</tr>' +
        '</table></div>'
}

function Confirm(el) {
    angular.element($('.container')).scope().confirm(el)
}

function Remove(el) {
    angular.element($('.container')).scope().removeRepository(el)
}
var table = null
$(function() {

    $('#repository_table tbody').on('click', '.confirm', function() {
        var row = table.row($(this).closest('table').parent().parent().parent().prev())
        Confirm(row.data())
    })

    $('#repository_table tbody').on('click', '.remove', function() {
        var row = table.row($(this).closest('table').parent().parent().parent().prev())
        Remove(row.data())
    })

    $('#repository_table tbody').on('click', 'tr', function() {
        var tr = $(this);
        if (!tr.hasClass('no-padding') && !tr.hasClass('child-row-flag') && tr.children().length > 1) {
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                $('div.slider', row.child()).slideUp(function() {
                    row.child.hide();
                    tr.removeClass('shown');
                });
            } else {
                // Open this row
                var shown_tr = $('.shown')
                var shown_row = table.row(shown_tr)
                    // var shown = table.row($('.shown').next('.slider'))
                $('div.slider', shown_row.child()).slideUp(function() {
                    shown_row.child.hide()
                    shown_tr.removeClass('shown')
                })
                row.child(format(row.data()), 'no-padding').show();
                tr.addClass('shown');
                $('div.slider', row.child()).slideDown();
            }
        }
    })
})

var RefreshData = function(prize) {
    if (table) {
        table.ajax.url('../api/prizes/get_repository?id=' + prize)
        table.ajax.reload()
        return
    }
    table = $('#repository_table').DataTable({
        columnDefs: [
            { type: 'natural', targets: '_all' }
        ],
        ajax: {
            url: '../api/prizes/get_repository?id=' + prize,
            dataSrc: ''
        },
        rowCallback: function(row, data, index) {
            if (data.Wydane)
                $(row).find('td:eq(2)').css('background-color', 'green')
            else
                $(row).find('td:eq(2)').css('background-color', 'red')
        },
        columns: [
            { data: 'Opis' },
            { data: 'Plik' },
            {
                data: 'Wydane',
                render: function(data, type, row) {
                    if (data)
                        return 'TAK (' + moment(row.Data_wydania).format('YYYY-MM-DD') + ' - ' + row.Klient + ')'
                    else
                        return 'NIE'
                }
            }
        ]
    })
}